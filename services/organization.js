const { Op } = require('sequelize');
const { models } = require('../database/index');
const { checkIsAdmin } = require('../helpers/isCheck');

module.exports.checkOrganization = async (organizationName, userId) => {
  try {
    const organization = await models.organization.findOne({
      where: {
        name: organizationName,
      },
    });

    if (!organization) {
      return {
        statusCode: 200,
        status: 'success',
        isAvailable: true,
        info: null,
        isMember: false,
      };
    }

    /* Check membership */
    const checkMembership = await models.organizationuser.findOne({
      where: {
        organizationId: organization.organizationId,
        uid: Number(userId),
      },
    });

    return {
      statusCode: 200,
      status: 'success',
      isAvailable: false,
      info: organization,
      isMember: Boolean(checkMembership),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};
module.exports.checkUser = async (organizationId, email) => {
  try {
    console.log(models);
    const orgUser = await models.organizationuser.findOne({
      where: {
        organizationId,
      },
      include: [
        {
          model: models.user,
          where: {
            email,
          },
        },
      ],
    });
    console.log('orgUser', orgUser);
    if (orgUser) {
      return { statusCode: 200, status: 'success', exist: true };
    }
    return { statusCode: 200, status: 'success', exist: false };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};
module.exports.checkOrganizationByPartialSearch = async (organizationText) => {
  try {
    const checkOrganiztion = await models.organization.findOne({
      where: {
        name: {
          [Op.substring]: organizationText,
        },
      },
    });
    return {
      statusCode: 200,
      status: 'success',
      lists: checkOrganiztion,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.createOrganization = async (organization, userId) => {
  try {
    /* For safety purpose */
    const duplicateCheck = await models.organization.findOne({
      where: {
        name: organization,
      },
    });

    if (duplicateCheck) {
      return {
        statusCode: 409,
        status: 'failed',
        response: 'Already exist',
      };
    }

    const createOrganization = await models.organization.create(
      {
        name: organization,
        organizationusers: [
          {
            uid: userId,
            role: 'admin',
            requestAccepted: 'true',
            approvedBy: userId,
          },
        ],
      },
      {
        include: [models.organizationuser],
      }
    );
    return {
      statusCode: 200,
      status: 'success',
      response: createOrganization,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.invitaRequestToOrganization = async (organizationId, userId, inviterId, usertype, department) => {
  try {
    const organization = await models.organization.findByPk(organizationId);

    if (!organization) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'Not found',
      };
    }

    const createdOrganizationUser = await organization.createOrganizationuser({
      uid: userId,
      role: usertype,
      requestAccepted: 'false',
      approvedBy: inviterId,
      department,
    });

    return {
      statusCode: 200,
      status: 'success',
      response: createdOrganizationUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.joinRequestOrganization = async (organizationId, userId) => {
  try {
    const organization = await models.organization.findByPk(organizationId);

    if (!organization) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'Not found',
      };
    }

    const createdOrganizationUser = await organization.createOrganizationuser({
      uid: userId,
      role: 'user',
      requestAccepted: 'requested',
      approvedBy: null,
    });

    return {
      statusCode: 200,
      status: 'success',
      response: createdOrganizationUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.requestOrganizationApprove = async (userId, organizationId, adminId) => {
  try {
    const isAdmin = await checkIsAdmin(adminId, organizationId);

    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        response: 'Unauthorized',
      };
    }

    const organizationUser = await models.organizationuser.findOne({
      where: {
        organizationId,
        uid: userId,
      },
    });

    if (!organizationUser) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'Not found',
      };
    }

    organizationUser.requestAccepted = 'true';
    organizationUser.approvedBy = userId;
    const savedOrganizationUser = await organizationUser.save();

    return {
      statusCode: 200,
      status: 'success',
      response: savedOrganizationUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.getOrgList = async () => {
  try {
    const getList = await models.organization.findAll();
    return {
      statusCode: 200,
      status: success,
      response: getList,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.getOrgUsersListByOrgId = async (organizationId, limit, offset) => {
  try {
    const organizationUsers = await models.organizationuser.findAndCountAll({
      where: {
        organizationId,
      },
      include: {
        model: models.user,
      },
      limit: Number(limit),
      offset: Number(offset),
    });

    return {
      statusCode: 200,
      status: 'success',
      result: organizationUsers.rows,
      totalRecord: organizationUsers.count,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.orgDetailsByID = async (organizationId) => {
  try {
    const organization = await models.organization.findByPk(organizationId, {
      include: {
        model: models.user,
      },
    });

    return organization;
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.orgDetailsWithRepoInfoByID = async (organizationId, userId) => {
  try {
    const organization = await models.organization.findByPk(organizationId, {
      include: {
        model: models.user,
      },
    });

    if (!organization) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'Not found',
      };
    }

    const isAdmin = checkIsAdmin(userId, organizationId);
    let whereClause = { organizationId };
    if (!isAdmin) {
      whereClause = {
        organizationId,
        [Op.or]: [
          { isPublic: 1 },
          { [Op.and]: [{ isPublic: 0 }, { '$repositoryusers.organizationuser.user.uid$': userId }] },
        ],
      };
    }

    const repositories = await models.repository.findAll({
      where: whereClause,
      include: {
        model: models.repositoryuser,
        include: {
          model: models.organizationuser,
          include: {
            model: models.user,
          },
        },
      },
    });

    return {
      statusCode: 200,
      status: 'success',
      orgInfo: organization,
      orgRepositoryInfo: repositories,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.getBySubdomain = async (subdomain) => {
  try {
    const organization = await models.organization.findOne({
      where: {
        subdomain,
      },
    });

    if (!organization) {
      return {
        statusCode: 404,
        status: 'not found',
        organization: null,
      };
    }

    return {
      statusCode: 200,
      status: 'success',
      organization,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      organization: null,
    };
  }
};

module.exports.removeUserFromOrganization = async (userId, organizationId, adminId) => {
  try {
    console.log('remove', userId, organizationId, adminId);
    const isAdmin = await checkIsAdmin(adminId, organizationId);

    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        response: 'Unauthorized',
      };
    }

    const organizationuser = await models.organizationuser.findOne({
      where: {
        uid: userId,
        organizationId,
      },
    });

    if (!organizationuser) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'User not found',
      };
    }

    const deletedUser = await organizationuser.destroy();
    return {
      statusCode: 200,
      status: 'success',
      message: deletedUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Unexpected error',
    };
  }
};
