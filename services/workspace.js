const { Op } = require("sequelize");
const { models } = require('../database/index');
const { deleteFolder } = require('../services/folder')

const { createFolderByCopy } = require('./folder');

/* Adding user to WorkSpace */
module.exports.addWorkspaceToRepository = async (
  adminId,
  repositoryId,
  workspaceName
) => {
  try {
    const createResponse = await models.repositoryworkspace.create({
      uid: adminId,
      repositoryId,
      workspaceName,
    });

    return {
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

getCopyWorkspaceName = async (repositoryId, sourceWorkspaceName) => {
  let count = 0;
  const existedWorkspace = await models.repositoryworkspace.findAll({
    where: {
      repositoryId,
    },
  });
  const existedWorkspaceNames = existedWorkspace.map(
    (workspace) => workspace.workspaceName,
  );
  let copyWorkspaceName;
  do {
    if (count === 0) {
      copyWorkspaceName = `${sourceWorkspaceName} copy`;
    } else {
      copyWorkspaceName = `${sourceWorkspaceName} copy (${count})`;
    }
    count++;
  } while (existedWorkspaceNames.includes(copyWorkspaceName));

  return copyWorkspaceName;
};

module.exports.addWorkspaceToRepositoryByCopy = async (
  uid,
  repositoryId,
  workspaceId
) => {
  try {
    const sourceWorkspace = await models.repositoryworkspace.findOne({
      where: { workspaceId },
    });
    const copyWorkspaceName = await getCopyWorkspaceName(
      repositoryId,
      sourceWorkspace.workspaceName,
    );

    delete sourceWorkspace.workspaceId;
    const destWorkspace = await models.repositoryworkspace.create({
      uid,
      repositoryId,
      ...sourceWorkspace,
      workspaceName: copyWorkspaceName,
    });
    const sourceFolders = await models.repositoryworkspacefolder.findAll({
      where: { workspaceId },
    });
    sourceFolders.forEach(folder => {
      createFolderByCopy(
        uid,
        folder.workspaceFolderId,
        true,
        true,
        destWorkspace.workspaceId
      );
    });

    return {
      status: 'success',
      response: destWorkspace,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

/* Get Workspace infor */
module.exports.getWorkSpaceInfo = async (workspaceId, filters) => {
  try {
    const folderWhere = {};
    const folderBoardWhere = {};

    if(filters) {
      folderBoardWhere['workspaceFolderBoardName'] = {
        [Op.like]: `%${filters}%`
      }
    }
    const getResponse = await models.repositoryworkspace.findOne({
      where: {
        workspaceId,
      },
      order: [['workspaceId', 'ASC']],
      include: [
        {
          model: models.filegroup,
          include: {
            model: models.filegrouprecord,
            separate: true,
            order: [['recordId', 'ASC']],
          },
        },
        {
          model: models.repositoryworkspacefolder,
          separate: true,
          order: [
            ['order', 'ASC'],
            ['workspaceFolderId', 'ASC'],
          ],
          include: [
            {
              model: models.filegroup,
              include: {
                model: models.filegrouprecord,
                separate: true,
                order: [['recordId', 'ASC']],
              },
            },
            {
              model: models.repositoryworkspacefolderboard,
              where: folderBoardWhere,
              separate: true,
              order: [
                ['order', 'ASC'],
                ['workspaceFolderBoardId', 'ASC'],
              ],
              include: [
                {
                  model: models.filegroup,
                  separate: true,
                  order: [
                    ['order', 'ASC'],
                    ['fileGroupId', 'ASC'],
                  ],
                  include: {
                    model: models.filegrouprecord,
                    separate: true,
                    order: [['recordId', 'ASC']],
                    include: {
                      model: models.subgroup,
                      as: 'subgroup',
                      include: {
                        model: models.filegroup,
                        as: 'filegroup',
                        include: {
                          model: models.filegrouprecord,
                          separate: true,
                          order: [['recordId', 'ASC']],
                        },
                      },
                    },
                  },
                },
                {
                  model: models.applicationform,
                  as: 'applicationform',
                  order: [
                    ['applicationFormId', 'ASC']
                  ],
                  include: {
                    model: models.applicationformentry,
                    as: 'applicationformentry',
                    separate: true,
                    order: [
                      ['applicationFormEntryId', 'ASC']
                    ],
                  },
                },
              ],
            },
          ],
        },
      ],
    });

    return {
      statusCode: 200,
      status: 'success',
      response: getResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.deleteWorkspace = async workspaceId => {
  try {

    const folders = await models.repositoryworkspacefolder.findAll({
      where: {
        workspaceId,
      },
    });

    const folderIds = folders.map((folder) => folder.workspaceFolderId);
    
    if(folderIds.length > 0) {
      await deleteFolder(folderIds)  
    }

    await models.repositoryworkspace.destroy({
      where: { workspaceId },
    });

    return {
      statusCode: 204,
      status: 'success',
      message: 'Workspace deleted',
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Failed to delete folder',
    };
  }
};

module.exports.renameWorkspace = async (workspaceId, workspaceName) => {

  try {
    const workspace = await models.repositoryworkspace.findOne({
      where: { workspaceId }
    })

    if (!workspace) {
      return {
        statusCode: 400,
        status: 'failed',
        message: 'workspace not found',
      };
    } else {

      workspace.workspaceName = workspaceName;
      workspace.save();
      return {
        statusCode: 200,
        status: 'success',
        message: "Workspace name updated"
      }

    }

  }catch(error){
    console.log("update workspace name", error)
    return {
      statusCode: 400,
      status: 'failed',
      message: 'error while trying to update workspace name',
    };
  }

}