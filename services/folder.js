const { models } = require('../database/index');
const { duplicateBoards, clearBoard } = require('./repository');

/* Adding Folder to WorkSpace */
module.exports.addWorkspaceFolderName = async (adminId, workspaceId, workspaceFolderName) => {
  try {
    const createResponse = await models.repositoryworkspacefolder.create({
      uid: adminId,
      workspaceId,
      workspaceFolderName,
    });

    return {
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

// Updating the folder name
module.exports.updateFolderName = async (folderId, folderName) => {
  try {
    const folder = await models.repositoryworkspacefolder.findByPk(folderId);
    const folderRecord = await models.filegrouprecord.findOne({ where: { type: 'folder', linkId: folderId } });

    if (folder && folderRecord) {
      folder.workspaceFolderName = folderName;
      folderRecord.title = folderName;
      await folder.save();
      await folderRecord.save();
      return {
        statusCode: 200,
        status: 'success',
        updated: true,
      };
    }
    return {
      statusCode: 404,
      status: 'not found',
      updated: false,
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      status: 'failed',
      updated: false,
    };
  }
};

module.exports.deleteFolder = async (workspaceFolderId) => {
  try {
    const boards = await models.repositoryworkspacefolderboard.findAll({
      where: {
        workspaceFolderId,
      },
    });
    const boardIds = boards.map((board) => board.workspaceFolderBoardId);

    await clearBoard(boardIds);
    await models.repositoryworkspacefolder.destroy({
      where: { workspaceFolderId },
    });
    await models.filegrouprecord.destroy({ where: { type: 'folder', linkId: workspaceFolderId } });
    return {
      statusCode: 204,
      status: 'success',
      message: 'Folder deleted',
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Failed to delete folder',
    };
  }
};

module.exports.updateFolderListOrder = async (folderOrders) => {
  try {
    const data = folderOrders.map((folder) => ({
      workspaceFolderId: folder.id,
      order: folder.order,
      uid: folder.uid,
    }));
    await models.repositoryworkspacefolder.bulkCreate(data, {
      returning: true,
      updateOnDuplicate: ['order'],
    });

    return {
      statusCode: 200,
      status: 'success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.createFolderByCopy = async (
  uid,
  folderId,
  isWithData,
  isCopyWorkspace = false,
  destinationWorkspaceId = -1
) => {
  try {
    const toModifyAsNewRecord = await models.repositoryworkspacefolder.findOne({
      where: {
        workspaceFolderId: folderId,
      },
    });

    let newFolderName;
    let newWorkspaceId;
    if (isCopyWorkspace && destinationWorkspaceId !== -1) {
      newFolderName = toModifyAsNewRecord.workspaceFolderName;
      newWorkspaceId = destinationWorkspaceId;
    } else {
      newFolderName = `${toModifyAsNewRecord.workspaceFolderName}_copy`;
      newWorkspaceId = toModifyAsNewRecord.workspaceId;
    }

    /* Create folder with modified name */
    if (toModifyAsNewRecord) {
      const createResponse = await models.repositoryworkspacefolder.create({
        uid,
        workspaceId: newWorkspaceId,
        workspaceFolderName: newFolderName,
      });

      /* For creating board */
      if (createResponse) {
        const boardList = await models.repositoryworkspacefolderboard.findAll({
          where: {
            workspaceFolderId: folderId,
          },
        });
        if (boardList) {
          await duplicateBoards(uid, boardList, isWithData, createResponse.workspaceFolderId, isCopyWorkspace);
        }
      }
    }
    return { code: 200, status: 'success', response: 'done' };
  } catch (error) {
    console.error(error);
    return {
      code: 400,
      status: 'failed',
      response: 'Something went wrong',
      error,
    };
  }
};
