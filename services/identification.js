const { Op, where } = require('sequelize');
const { models } = require('../database/index');
const { checkIsAdmin } = require('../helpers/isCheck');

module.exports.getAdminUser = async () => {
    try {
      const getList = await models.user.findAll({
        where: {
            [Op.or]: [{type: 'admin'}, {type: 'super'}]
      }}
      );
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
  };

module.exports.updateUserType = async (type, uid) => {
      try {
        const createResponse = await models.user.update(
          { type: type },
          { where : { uid : uid}})
        
      return {
          status: 'success',
          response: createResponse
        };
      } catch (error) {
        console.error(error);
        return {
          status: 'failed',
          response: 'Something went wrong!',
        };
      }
    };

module.exports.createIdentification = async (    
  uid, uName, adminId, status, type) => {
    try {
      const createResponse = await models.identification.create({
        uid, uName, adminId, status, type
      })
          
    return {
      status: 'success',
      response: createResponse
    };
    } catch (error) {
      console.error(error);
      return {
        status: 'failed',
        response: 'Something went wrong!',
      };
    }
  };

module.exports.getIdentification = async (uid) => {
  try {
    const getList = await models.identification.findAll({
      where: { [Op.and]: [{adminId: uid}, {status: 'request'}] }
    }
    );
    return {
      statusCode: 200,
      status: 'success',
      response: getList,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.getUserIdentification = async (uid) => {
  try {
    const getList = await models.identification.findAll({
      where: { uid: uid }
    }
    );
    return {
      statusCode: 200,
      status: 'success',
      response: getList,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.deleteIdentification = async (identificationId) => {
  try {
    const getList = await models.identification.destroy({
      where: { identificationId: identificationId }
    }
    );
    return {
      statusCode: 200,
      status: 'success',
      response: getList,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.updateIdentification = async (identificationId) => {
  try {
    const createResponse = await models.identification.update(
      { status: 'deny' },
      { where : { identificationId : identificationId }})
    
  return {
      status: 'success',
      response: createResponse
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
    };
  }
};

