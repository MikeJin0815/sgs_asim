const sha256 = require('sha256');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Op, where } = require('sequelize');
const { models } = require('../database/index');
const { validateEmail, sendPhoneOtpSMS, sendCodeToEmail, commonEmailSend } = require('../helpers/utils');
const messages = require('../helpers/messages');
const { checkIsAdmin } = require('../helpers/isCheck');

const { invitaRequestToOrganization } = require('./organization');

module.exports.registrationOpen = async (req) => {
  try {
    const { email } = req.body;
    await models.user.checkIfExist(email);
    const user = await models.user.findOne({
      where: { email },
    });

    if (!user) {
      /* super user 생성 */
      const superemail = ['mike.jin@sgs.com', 'cendrick.ahn@sgs.com', 'hyemi.ji@sgs.com', 'chaeeun.kim@sgs.com', 'hiel.kim@sgs.com', 'dongha.lee@sgs.com', 'sungkyu.min@sgs.com', 'jeongho.yoon@sgs.com']
      if (superemail.includes(email)) {
        const response = await models.user.createSuperUser(req.body);
      if (response.user) {
        /* No blocking email */
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'Registration',
            },
          },
        });

        commonEmailSend(response.user, emailTemplate);
      }
      }
      else if (email.split('@')[1] === 'sgs.com') {
        const response = await models.user.createCompanyUser(req.body);
      if (response.user) {
        /* No blocking email */
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'Registration',
            },
          },
        });

        commonEmailSend(response.user, emailTemplate);
      }
      }
      else {
        const response = await models.user.createUser(req.body);
      if (response.user) {
        /* No blocking email */
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'Registration',
            },
          },
        });

        commonEmailSend(response.user, emailTemplate);
      }
      }
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.getInformation = async (req) => {
  try {
    const token = req.headers['x-access-token'];
    const decoded = jwt.decode(token);
    const userInfo = await models.user.findOne({
      where: {
        uid: decoded.id,
      },
      attributes: [
        'uid',
        'first_name',
        'last_name',
        'full_name',
        'username',
        'email',
        'type',
        'phone',
        'updated_at',
        'profileImage',
      ],
      include: [
        {
          model: models.organization,
        },
      ],
    });
    const returnData = {
      email: userInfo.email,
      phone: userInfo.dataValues.phone,
      first_name: userInfo.dataValues.first_name,
      last_name: userInfo.dataValues.last_name,
      full_name: userInfo.full_name,
      uid: userInfo.dataValues.uid,
      username: userInfo.username,
      type: userInfo.type,
      updated_at: userInfo.updated_at,
      profileImage: userInfo.profileImage,
      organizations: userInfo.organizations,
    };
    return returnData;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.registrationCompleteByUser = async (req) => {
  try {
    const {
 email, phone, password, email_verification_code, secret 
} = req.body;
    const checkEmailAndEmailVerifCode = await models.user.checkIfExistEmailVerificationCode(
      email,
      email_verification_code
    );

    if (checkEmailAndEmailVerifCode) {
      const user = await models.user.findOne({
        where: {
          email,
          email_verification_code,
        },
      });
      user.phone = phone;
      user.password = await bcrypt.hash(password, 12);
      user.request = 'accepted';
      user.approvedBy = user.id;

      /* Check invitation is valid or not */
      const invitation = await models.organizationuser.findOne({
        where: {
          organizationId: Number(secret),
          uid: user.uid,
        },
      });

      if (invitation) {
        invitation.requestAccepted = 'true';
        await invitation.save();
        const userUpdate = await user.save();
        delete userUpdate.password;
        user.URL = process.env.CLIENT_URL;
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'userAcceptanceMail',
            },
          },
        });

        commonEmailSend(user, emailTemplate);
        return userUpdate;
      }

      throw new Error(err);
    } else {
      throw new Error('Unable to update user profile. Please check your e-mail verification code!');
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.registrationByAdmin = async (req) => {
  try {
    const { email, first_name, last_name, usertype, organizationId, department } = req.body;
    const user = await models.user.findOne({
      where: { email },
    });
    const isAdmin = await checkIsAdmin(req.authorizationId, organizationId);
    const organization = await models.organization.findByPk(organizationId);

    if (!organization) {
      throw new Error('Organization Not found');
    }
    if (!isAdmin) {
      throw new Error('Unprivilized user!');
    }
    if (!user) {
      const email_verification_code = String(Math.floor(100000 + Math.random() * 900000)) + Date.now();
      const userData = await models.user.createUserByAdmin({
        first_name,
        last_name,
        email,
        user_created_by: req.authorizationId,
        email_verification_code,
      });
      const response = await invitaRequestToOrganization(
        organizationId,
        userData.uid,
        req.authorizationId,
        usertype,
        department
      );
      if (response.status == 'success') {
        console.log('User invitation if block');
        userData.URL = `${process.env.CLIENT_URL}/invitation-register?email=${userData.email}&secret=${response.response.organizationId}&code=${email_verification_code}&username=${userData.username}`;
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'UserInvitation',
            },
          },
        });
        commonEmailSend(userData, emailTemplate);
        return 'Invitation sent';
      }

      await models.user.destroy({
        where: {
          uid: userData.uid,
        },
      });
      throw new Error('Failed to create user');
    } else {
      const inviteResult = await invitaRequestToOrganization(organizationId, user.uid, req.authorizationId, usertype);
      console.log('inviteResult', inviteResult);

      if (inviteResult.status == 'failed') {
        throw new Error('Failed to create user');
      } else {
        console.log('inviteResult success');
        console.log('process.env.CLIENT_URL', process.env.CLIENT_URL);
        console.log('user.email', user.email);
        console.log('organizationId', organizationId);
        console.log('user.email_verification_code', user.email_verification_code);
        // console.log("user", user)
        console.log('user.username', user.username);

        user.URL = `${process.env.CLIENT_URL}/invitation-register?email=${user.email}&secret=${organizationId}&code=${user.email_verification_code}&username=${user.username}`;
        console.log('User invitation else block');
        const emailTemplate = await models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'UserInvitation',
            },
          },
        });

        commonEmailSend(user, emailTemplate);
      }
      return 'Invitation sent';
    }
  } catch (err) {
    console.log('error invite');
    throw new Error(err);
  }
};

module.exports.registrationBySocialLogin = async (
  email,
  first_name,
  last_name,
  user_created_by,
  platform,
  socialUid,
  profileImage
) => {
  try {
    const user = await models.user.findOne({
      where: {
        email,
      },
    });

    if (!user) {
      const verificationCode = Math.floor(100000 + Math.random() * 900000);
      const user = await models.user.createUserBySocialLogin({
        first_name,
        last_name,
        email,
        user_created_by,
        verificationCode,
        platform,
        socialUid,
        profileImage,
      });
      const emailTemplate = await models.emailtemplate.findOne({
        where: {
          type: {
            [Op.like]: 'SocialLogin',
          },
        },
      });
      commonEmailSend(user, emailTemplate);
      return 1;
    }

    user.profileImage = profileImage;
    user.socialUid = socialUid;
    await user.save();
    return 0;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.login = async (req) => {
  try {
    const user = await models.user.scope('withPassword').findOne({
      where: {
        [Op.or]: [{ email: req.body.email }, { username: req.body.email }],
      },
    });
    if (!user) {
      throw new Error(messages.user.USER_NOT_FOUND);
    }
    const isValid = await bcrypt.compare(req.body.password, user.password);
    if (!isValid) {
      throw new Error(messages.user.INVALID_PASSWORD);
    }
    const token = jwt.sign({ id: user.uid }, process.env.SECRET_KEY || 'tokenSigningSecretKey');

    let returnData = {};

    /*
     * Const userInfo = await models.user.findOne({
     *     where: { uid: user.uid },
     *     attributes : ["uid", "request", "approvedBy"],
     * });
     */

    returnData = {
      token,
    };
    return returnData;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.socialLogin = async (socialId, email) => {
  try {
    const user = await models.user.findOne({
      where: {
        email,
        // SocialUid : socialId
      },
    });
    if (!user) {
      throw new Error(messages.user.USER_NOT_FOUND);
    }
    const token = jwt.sign({ id: user.uid }, process.env.SECRET_KEY || 'tokenSigningSecretKey');

    let returnData = {};

    returnData = {
      token,
      uid: user.uid,
      request: user.request,
      approvedBy: user.approvedBy,
    };
    return returnData;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.forgetPassword = async (userData) => {
  if (!userData.email || !validateEmail(userData ? userData.email : '')) {
    throw new Error('Vaild email is required');
  }
  try {
    const user = await models.user.findOne({
      where: { email: userData.email },
    });
    if (!user) {
      throw new Error(messages.user.USER_NOT_FOUND);
    } else {
      const verificationCode = Math.floor(100000 + Math.random() * 900000);
      user.forget_password_code = verificationCode;
      await user.save();
      const receiverArr = [
        {
          name: `${user.first_name} ${user.last_name}`,
          mobile: user ? user.phone : null,
          message: 'Forgot Password OTP',
        },
      ];
      sendPhoneOtpSMS('forgot password', verificationCode, receiverArr);
      // Sending email forgot password
      let code = userData.email;
      code = sha256(code);
      userData.emailURL = `${process.env.CLIENT_URL}/reset-password?token=${code}`;
      const emailTemplate = await models.emailtemplate.findOne({
        where: {
          type: {
            [Op.like]: 'ForgetPassword',
          },
        },
      });
      commonEmailSend(userData, emailTemplate);
    }

    return user;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.forgetUserNameFunc = async (userData) => {
  if (!userData.email || !validateEmail(userData ? userData.email : '')) {
    throw new Error('Vaild email is required');
  }
  try {
    const user = await models.user.findOne({
      where: {
        email: userData.email,
        platform: {
          [Op.eq]: null,
        },
      },
    });
    if (!user) {
      throw new Error(messages.user.USER_NOT_FOUND);
    } else {
      const emailTemplate = await models.emailtemplate.findOne({
        where: {
          type: {
            [Op.like]: 'ForgetUserName',
          },
        },
      });

      commonEmailSend(user, emailTemplate);
      return {
        status: 200,
        mesage: 'If your email is correct, we will send your username to your email account.',
      };
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.verifiyUserCode = async ({ code, email }) => {
  try {
    const user = await models.user.findOne({ where: { email } });

    if (!user) {
      throw new Error(messages.user.USER_NOT_FOUND);
    }
    if (user.forget_password_code === code) {
      user.forget_password_code = `${code}:true`;
      user.save();
      return {};
    }
    throw new Error(messages.user.INVALIDE_VERIFICATION_CODE);
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.resetPassword = async ({ email, newPassword, confirmPassword }) => {
  if (newPassword !== confirmPassword) {
    throw new Error(messages.user.PASSWORD_NOT_MATCHING);
  }

  try {
    const foundUser = await models.user.findOne({ where: { email } });

    if (!foundUser) {
      throw new Error(messages.user.USER_NOT_FOUND);
    }
    foundUser.password = await bcrypt.hash(newPassword, 12);
    await foundUser.save();

    const user = foundUser.toJSON();
    delete user.password;

    return user;
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.sendPhoneOtp = async ({ phone }) => {
  try {
    const otpCode = Math.floor(100000 + Math.random() * 900000);
    const now = new Date();
    const validUpto = new Date(now.getTime() + 5 * 60000);
    const receiverArr = [
      {
        mobile: phone,
        message: 'One time OTP message',
      },
    ];
    const otpData = await models.phoneOtp.findOne({
      where: {
        phone,
      },
    });
    if (!otpData) {
      const sendOtp = await sendPhoneOtpSMS('one time otp', otpCode, receiverArr);
      if (sendOtp && sendOtp.status == 0) {
        await models.phoneOtp.create({
          phone,
          otp: otpCode,
          expiresAt: validUpto,
        });
      } else {
        throw new Error('Error while sending otp!');
      }
    } else {
      const sendOtp = await sendPhoneOtpSMS('one time otp', otpCode, receiverArr);
      if (sendOtp && sendOtp.status == 0) {
        await models.phoneOtp.update(
          {
            otp: otpCode,
            expiresAt: validUpto,
          },
          {
            where: { phone },
          }
        );
      } else {
        throw new Error(`Error while sending otp, ${sendOtp.status}`);
      }
    }
    return { otp: otpCode };
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.verifyPhoneOtp = async ({ phone, otp }) => {
  try {
    const OtpData = await models.phoneOtp.findOne({ where: { phone } });
    if (!OtpData) {
      throw new Error(messages.user.PHONE_NOT_FOUND);
    }
    const now = new Date();
    if (OtpData.expiresAt < now) {
      throw new Error(messages.user.EXPIRED_OTP_CODE);
    }
    if (OtpData.otp == otp) {
      await OtpData.destroy();
      return {};
    }
    throw new Error(messages.user.INVALID_OTP_CODE);
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.checkEmailService = async (email) => {
  const foundUser = await models.user.findOne({
    where: { email },
  });

  if (foundUser) {
    return { status: 400, proceed: false, message: 'Already taken' };
  }
  return { status: 200, proceed: true };
};

module.exports.checkUserNameService = async (username) => {
  const foundUser = await models.user.findOne({
    where: { username },
  });

  if (foundUser) {
    return { status: 400, proceed: false, message: 'Already taken' };
  }
  return { status: 200, proceed: true };
};
