const { Op, where } = require('sequelize');
const { models } = require('../database/index');
const { checkIsAdmin } = require('../helpers/isCheck');

module.exports.createApplication = async (
  company,
  manager,
  phone,
  address,
  number,
  category,
  managementNum,
  express,
  size,
  sizeUnit,
  count,
  density,
  date,
  range,
  sample,
  extra,
  agree,
  fileName,
  fileLocation,
  ) => {
    try {
      const createResponse = await models.application.create({
        company,
        manager,
        phone,
        address,
        number,
        category,
        managementNum,
        express,
        size,
        sizeUnit,
        count,
        density,
        date,
        range,
        sample,
        extra,
        agree,
        fileName,
        fileLocation,
      })
      
      return {
        status: 'success',
        response: createResponse
      };
    } catch (error) {
      console.error(error);
      return {
        status: 'failed',
        response: 'Something went wrong!',
      };
    }
  };

  module.exports.getApplicationList = async (manager) => {
    try {
      const getList = await models.application.findAll({
        where: {
          manager: manager
      }}
      );
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
  };

  module.exports.getAllApplicationList = async () => {
    try {
      const getList = await models.application.findAll();
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
  };

  module.exports.removeApplication = async (applicationId) => {
    try {
      const application = await models.application.findByPk(applicationId);
  
      if (!application) {
        return {
          statusCode: 404,
          status: 'not found',
          message: 'User not found',
        };
      }
  
      const deletedApplication = await application.destroy();
      return {
        code: 200,
        status: 'success',
        response: deletedApplication,
      };
    } catch (error) {
      console.error(error);
      return {
        code: 400,
        status: 'failed',
        response: 'Something went wrong!',
        error,
      };
    }
  };

  module.exports.updateApplication = async (
    applicationId,
    category,
    express,
    size,
    sizeUnit,
    count,
    density,
    date,
    range,
    sample,
    extra) => {
      try {
        const createResponse = await models.application.update(
          {
            category: category,
            express: express,
            size: size,
            sizeUnit: sizeUnit,
            count: count,
            density: density,
            date: date,
            range: range,
            sample: sample,
            extra: extra
          },
          {
           where : { applicationId : applicationId}
          })
        
      return {
          status: 'success',
          response: createResponse
        };
      } catch (error) {
        console.error(error);
        return {
          status: 'failed',
          response: 'Something went wrong!',
        };
      }
    };
