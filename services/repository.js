const Sequelize = require('sequelize');
const axios = require('axios');
const sequelize = require('../database/index');
const { models } = require('../database/index');
const { checkIsAdmin, isUserAvailableInOrganization, isUserAlreadyInRepository } = require('../helpers/isCheck');

const { Op } = Sequelize;

/* Creating  repository with Image */
module.exports.createRepository = async (uid, organizationId, repositoryName, repositoryPhoto, isPublic) => {
  try {
    const createResponse = await models.repository.create({
      uid,
      organizationId,
      repositoryName,
      repositoryPhoto,
      isPublic: JSON.parse(isPublic),
    });

    return {
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
    };
  }
};

/* Adding user to repository */
module.exports.addUserToRepository = async (adminId, repositoryId, userId, organizationId) => {
  try {
    const isAdmin = await checkIsAdmin(adminId, organizationId);
    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        response: 'Unauthorized',
      };
    }

    const isUserAvailableInOrg = await isUserAvailableInOrganization(userId, organizationId);
    if (!isUserAvailableInOrg) {
      return {
        statusCode: 403,
        status: 'failed',
        response: 'Forbidden',
      };
    }

    const isAlreadyInRepository = await isUserAlreadyInRepository(userId, repositoryId);
    if (isAlreadyInRepository) {
      return {
        statusCode: 409,
        status: 'failed',
        response: 'Conflict',
      };
    }

    const createResponse = await models.repositoryuser.create({
      organizationUserId: userId,
      repositoryId,
      assignedBy: adminId,
    });

    return {
      statusCode: 200,
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Something went wrong!',
    };
  }
};

/* Getting workspace list of repository */
module.exports.getRepositoryInfo = async (repositoryId) => {
  try {
    const getResponse = await models.repository.findOne({
      where: {
        repositoryId,
      },
      include: {
        model: models.repositoryworkspace,
      },
    });

    return {
      status: 'success',
      response: getResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.addWorkspaceBoard = async (
  uid,
  workspaceFolderId,
  workspaceFolderBoardName,
  workspaceFolderBoardType
) => {
  try {
    const createResponse = await models.repositoryworkspacefolderboard.create({
      uid,
      workspaceFolderId,
      workspaceFolderBoardName,
      workspaceFolderBoardType,
    });

    return {
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

// Updating the board name
module.exports.updateBoardName = async (boardId, boardName) => {
  try {
    const board = await models.repositoryworkspacefolderboard.findByPk(boardId);
    const boardRecord = await models.filegrouprecord.findOne({ where: { type: 'board', linkId: boardId } });

    if (board && boardRecord) {
      board.workspaceFolderBoardName = boardName;
      boardRecord.title = boardName;
      await board.save();
      await boardRecord.save();
      return {
        statusCode: 200,
        status: 'success',
        updated: true,
      };
    }
    return {
      statusCode: 404,
      status: 'not found',
      updated: false,
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: 500,
      status: 'failed',
      updated: false,
    };
  }
};

// Updating the repository name
module.exports.updateRepositoryName = async (repositoryId, repositoryName) => {
  const getResult = await models.repository.findOne({
    where: {
      repositoryId,
    },
  });

  if (getResult) {
    getResult.repositoryName = repositoryName;
    await getResult.save();
    return {
      status: 'success',
      updated: true,
    };
  }
  return {
    status: 'failed',
    updated: false,
  };
};

module.exports.updateRepository = async (repositoryId, repoDetails, adminId) => {
  const repository = await models.repository.findOne({
    where: {
      repositoryId,
    },
  });

  if (repository) {
    const isAdmin = await checkIsAdmin(adminId, repository.organizationId);
    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        response: 'Unauthorized',
      };
    }
    await repository.update(repoDetails, {
      where: {
        repositoryId,
      },
    });
    return {
      status: 'success',
      updated: true,
    };
  }
  return {
    status: 'failed',
    updated: false,
  };
};

module.exports.updateBoardListOrder = async (boardOrders) => {
  try {
    const data = boardOrders.map((board) => ({
      workspaceFolderBoardId: board.id,
      workspaceFolderId: board.workspaceFolderId,
      order: board.order,
      uid: board.uid,
    }));
    await models.repositoryworkspacefolderboard.bulkCreate(data, {
      returning: true,
      updateOnDuplicate: ['order', 'workspaceFolderId'],
    });

    return {
      statusCode: 200,
      status: 'success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

// Updating repository photo
module.exports.updateRepositoryPhoto = async (repositoryId, repositoryPhoto, adminId) => {
  const repository = await models.repository.findOne({
    repositoryPhoto,
    where: {
      repositoryId,
    },
  });

  if (repository) {
    const isAdmin = await checkIsAdmin(adminId, repository.organizationId);
    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        message: 'Unauthorized',
      };
    }

    repository.repositoryPhoto = repositoryPhoto;
    await repository.save();
    return {
      status: 'success',
      updated: true,
    };
  }
  return {
    status: 'failed',
    updated: false,
  };
};

// Deleting repository
module.exports.deleteRepository = async (repositoryId, adminId) => {
  try {
    const repository = await models.repository.findByPk(repositoryId);
    const isAdmin = await checkIsAdmin(adminId, repository.organizationId);
    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        message: 'Unauthorized',
      };
    }
    /* Getting repository work spaces */
    const repWorkSpaces = await models.repositoryworkspace.findAll({
      where: {
        repositoryId,
      },
    });
    console.log(repWorkSpaces);

    repWorkSpaces.map(async (single) => {
      const { workspaceId } = single;
      await models.repositoryworkspace.destroy({ where: { workspaceId } });

      const repWorkSpaceFolder = await models.repositoryworkspacefolder.findAll({
        where: {
          workspaceId,
        },
      });
      repWorkSpaceFolder.map(async (singleFolder) => {
        const { workspaceFolderId } = singleFolder;
        await models.repositoryworkspacefolder.destroy({
          where: { workspaceFolderId },
        });

        const repWorkSpaceFolderBoard = await models.repositoryworkspacefolderboard.findAll({
          where: {
            workspaceFolderId,
          },
        });
        const boardIds = [];

        for (let i = 0; i < repWorkSpaceFolderBoard.length; i++) {
          boardIds.push(repWorkSpaceFolderBoard[i].workspaceFolderBoardId);
        }
        console.log(boardIds);
        await clearBoard(boardIds);
      });
    });

    await models.repository.destroy({ where: { repositoryId } });
    await models.repositoryuser.destroy({ where: { repositoryId } });

    return { message: 'repository deleted' };
  } catch (error) {
    console.error(error);
    return { message: 'failed' };
  }
};

module.exports.clearBoard = async (boardIds) => {
  await clearBoard(boardIds);
  return {
    message: 'Board deleted',
  };
};

module.exports.removeUser = async (repositoryUserId, adminId) => {
  try {
    const repositoryUser = await models.repositoryuser.findByPk(repositoryUserId, {
      include: {
        model: models.organizationuser,
      },
    });

    if (!repositoryUser) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'User not found',
      };
    }

    const isAdmin = await checkIsAdmin(adminId, repositoryUser.organizationuser.organizationId);
    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        message: 'Unauthorized',
      };
    }

    const deletedUser = await repositoryUser.destroy();
    return {
      code: 200,
      status: 'success',
      response: deletedUser,
    };
  } catch (error) {
    console.error(error);
    return {
      code: 400,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

/* Adding user to repository */
module.exports.createEmailTemplate = async (type, body, uid, subject) => {
  try {
    const createResponse = await models.emailtemplate.create({
      type,
      body,
      uid,
      subject,
    });

    return {
      status: 'success',
      response: createResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

// Deleting Email Template
module.exports.deleteEmailTemplate = async (type) => {
  const deleteResults = await models.emailtemplate.destroy({
    where: {
      type: {
        [Op.like]: type,
      },
    },
  });
  if (deleteResults) {
    return {
      message: 'email template deleted',
    };
  }
  return 404;
};

/* Getting email template by type */
module.exports.getEmailTemplate = async (type) => {
  try {
    const getResponse = await models.emailtemplate.findOne({
      where: {
        type: {
          [Op.like]: type,
        },
      },
    });

    return {
      status: 'success',
      response: getResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

/* Getting email template by Email Subject */
module.exports.getEmailTemplateBySubject = async (subject) => {
  try {
    const getResponse = await models.emailtemplate.findOne({
      where: {
        subject: {
          [Op.like]: subject,
        },
      },
    });

    return {
      status: 'success',
      response: getResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

/* Update email template by ID */
module.exports.updateEmailTemplate = async (tid, body, uid, subject) => {
  try {
    const getResponse = await models.emailtemplate.update(
      {
        body,
        uid,
        subject,
      },
      {
        where: {
          templateId: tid,
        },
      }
    );

    return {
      status: 'success',
      response: getResponse,
    };
  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.getBoardDetails = async (boardId) => {
  try {
    const getResponse = await models.repositoryworkspacefolderboard.findOne({
      where: {
        workspaceFolderBoardId: boardId,
      },
    });
    if (getResponse) {
      return { code: 200, status: 'success', response: getResponse };
    }
    return { code: 404, status: 'success', response: getResponse };
  } catch (error) {
    console.error(error);
    return { code: 400, status: 'failed', response: 'Something went wrong' };
  }
};

module.exports.getRepositoryUser = async (repositoryId, userId) => {
  try {
    console.log('repositoryUser', repositoryId, userId);
    const repositoryUser = await models.repositoryuser.findOne({
      where: { repositoryId },
      include: { model: models.organizationuser, where: { uid: userId } },
    });

    return { code: 200, status: 'success', response: repositoryUser };
  } catch (error) {
    console.error(error);
    return { code: 400, status: 'failed', response: 'Something went wrong' };
  }
};

module.exports.updateRepositoryUser = async (repositoryUserId, payload) => {
  try {
    const repositoryUser = await models.repositoryuser.findByPk(repositoryUserId);

    if (!repositoryUser) {
      return { code: 404, status: 'not found', response: 'Repository user not found' };
    }

    if (payload.accessLevel) {
      repositoryUser.accessLevel = payload.accessLevel;
    }

    const updatedRepositoryUser = await repositoryUser.save();
    return { code: 200, status: 'success', response: updatedRepositoryUser };
  } catch (error) {
    console.error(error);
    return { code: 400, status: 'failed', response: 'Something went wrong' };
  }
};

module.exports.getRepositoryUsers = async (repositoryId) => {
  try {
    const getList = await models.repositoryuser.findAll({
      where: {
        repositoryId,
      },
      include: {
        model: models.organizationuser,
        include: {
          model: models.user,
        },
      },
    });
    const usersList = [];
    getList.forEach((repoUser) => {
      usersList.push(repoUser.organizationuser);
    });

    return { code: 200, status: 'success', response: usersList };
  } catch (error) {
    console.error(error);
    return { code: 400, status: 'failed', response: 'Something went wrong' };
  }
};

const duplicateCell = async (recordId, cell, newColumns) => {
  const cellColumn = newColumns.find((column) => column.rootreference === Number(cell.colId));
  if (cellColumn?.colId) {
    console.log(
      'duplicateCell',
      cell.fileName,
      cell.fileLocation,
      cell.statusId,
      cell.text,
      cell.people,
      cell.date,
      cell.daterange_start,
      cell.daterange_end
    );
    const newCell = await models.filegrouprecordfile.findOne({ where: { recordId, colId: cellColumn.colId } });

    newCell.fileName = cell.fileName;
    newCell.fileLocation = cell.fileLocation;
    newCell.statusId = cell.statusId;
    newCell.text = cell.text;
    newCell.people = cell.people;
    newCell.date = cell.date;
    newCell.daterange_start = cell.daterange_start;
    newCell.daterange_end = cell.daterange_end;
    await newCell.save();

    return newCell;
  }
  return null;
};

const duplicateRecord = async (groupId, record, newColumns) => {
  const newRecord = await models.filegrouprecord.create({
    fileGroupId: groupId,
    title: record.title,
  });

  if (record.subgroup) {
    console.log('hasSubGroup', record.subGroup);
    const newGroup = await duplicateGroup(record.subgroup.filegroup, null);
    const createdSubGroup = await models.subgroup.create({
      recordId: newRecord.recordId,
      fileGroupId: newGroup.fileGroupId,
    });
    console.log('setSubGroup');
    await newRecord.setSubgroup(createdSubGroup);
  }

  const recordCells = await models.filegrouprecordfile.findAll({
    where: { recordId: record.recordId },
  });
  const newCells = [];
  recordCells.forEach((cell) => {
    newCells.push(duplicateCell(newRecord.recordId, cell, newColumns));
  });
  await Promise.all(newCells);

  return newRecord;
};

const duplicateColumn = async (groupId, column) => {
  const newColumn = await models.filegroupcolumn.create({
    fileGroupId: groupId,
    headerName: column.headerName,
    headerType: column.headerType,
    rootreference: column.colId,
    active: column.active,
  });
  // Add duplicate status
  return newColumn;
};

const duplicateGroup = async (group, newBoardId) => {
  const newGroup = await models.filegroup.create({
    boardId: newBoardId,
    fileSystemGroupName: group.fileSystemGroupName,
  });
  const columns = await models.filegroupcolumn.findAll({
    where: {
      fileGroupId: group.fileGroupId,
    },
    order: [
      ['colorder', 'ASC'],
      ['colId', 'ASC'],
    ],
  });
  const columnsPromise = [];
  columns.forEach((column) => {
    columnsPromise.push(duplicateColumn(newGroup.fileGroupId, column));
  });
  const newColumns = await Promise.all(columnsPromise);

  const records = await models.filegrouprecord.findAll({
    where: { fileGroupId: group.fileGroupId },
    order: [['order', 'ASC']],
    include: {
      model: models.subgroup,
      as: 'subgroup',
      include: {
        model: models.filegroup,
        as: 'filegroup',
      },
    },
  });
  const recordsPromise = [];
  records.forEach((record) => {
    recordsPromise.push(duplicateRecord(newGroup.fileGroupId, record, newColumns));
  });
  const newRecords = await Promise.all(recordsPromise);
  return newGroup;
};

const duplicateBoard = async (board, workspaceFolderId, isCopyWorkspace) => {
  const newBoardName = isCopyWorkspace ? board.workspaceFolderBoardName : `${board.workspaceFolderBoardName}_copy`;
  const newBoard = await models.repositoryworkspacefolderboard.create({
    workspaceFolderId,
    workspaceFolderBoardName: newBoardName,
    workspaceFolderBoardType: board.workspaceFolderBoardType,
  });
  if (board.workspaceFolderBoardType === 'Project') {
    const groups = await models.filegroup.findAll({
      where: { boardId: board.workspaceFolderBoardId },
      order: [
        ['order', 'ASC'],
        ['fileGroupId', 'ASC'],
      ],
    });
    const groupsPromise = [];
    groups.forEach((group) => {
      groupsPromise.push(duplicateGroup(group, newBoard.workspaceFolderBoardId));
    });
    const newGroups = await Promise.all(groupsPromise);
  }
  return newBoard;
};

module.exports.duplicateBoards = async (uid, boardList, isWithData, workspaceFolderId, isCopyWorkspace = false) => {
  await sequelize.transaction(async (t1) => {
    const boardsPromise = [];
    boardList.forEach((board) => boardsPromise.push(duplicateBoard(board, workspaceFolderId, isCopyWorkspace)));
    const newBoards = await Promise.all(boardsPromise);
    return 'complete';
  });

  // // TO REFACTOR

  // for (let i = 0; i < boardList.length; i++) {
  //   const newBoardName = isCopyWorkspace
  //     ? boardList[i].workspaceFolderBoardName
  //     : `${boardList[i].workspaceFolderBoardName}_copy`;
  //   /* Creating duplicate board */
  //   const boardCreationResponse = await models.repositoryworkspacefolderboard.create({
  //     uid,
  //     workspaceFolderId,
  //     workspaceFolderBoardName: newBoardName,
  //     workspaceFolderBoardType: boardList[i].workspaceFolderBoardType,
  //   });

  //   /* Replicating board data */

  //   if (boardList[i].workspaceFolderBoardType === 'Project') {
  //     const getFileGroups = await models.filegroup.findAll({
  //       where: {
  //         boardId: boardList[i].workspaceFolderBoardId,
  //       },
  //     });

  //     if (getFileGroups) {
  //       for (let j = 0; j < getFileGroups.length; j++) {
  //         const dupFileGroupResponse = await models.filegroup.create({
  //           uid,
  //           boardId: boardCreationResponse.workspaceFolderBoardId,
  //           fileSystemGroupName: `${getFileGroups[j].fileSystemGroupName}`,
  //         });
  //         /* Read column def */
  //         const colDefArray = await models.filegroupcolumn.findAll({
  //           where: {
  //             fileGroupId: getFileGroups[j].fileGroupId,
  //           },
  //         });
  //         const rootreference = [];
  //         if (colDefArray) {
  //           for (var k = 0; k < colDefArray.length; k++) {
  //             await models.filegroupcolumn.create({
  //               uid,
  //               fileGroupId: dupFileGroupResponse.fileGroupId,
  //               headerName: colDefArray[k].headerName,
  //               headerType: colDefArray[k].headerType,
  //               rootreference: colDefArray[k].colId,
  //               active: colDefArray[k].active,
  //             });
  //             rootreference.push(colDefArray[k].colId);
  //           }
  //         }

  //         /* For creating column definition */
  //         const getFileGroupReocrds = await models.filegrouprecord.findAll({
  //           where: {
  //             fileGroupId: getFileGroups[j].fileGroupId,
  //           },
  //         });

  //         if (getFileGroupReocrds && isWithData) {
  //           for (var k = 0; k < getFileGroupReocrds.length; k++) {
  //             const title = `${getFileGroupReocrds[k].title}`;
  //             const newRecord = await models.filegrouprecord.create({
  //               uid,
  //               fileGroupId: dupFileGroupResponse.fileGroupId,
  //               title,
  //             });
  //             /* Copy related data */

  //             const newrecordId = newRecord.recordId;
  //             const oldrecordId = getFileGroupReocrds[k].recordId;
  //             await finishCopy(newrecordId, rootreference, oldrecordId, dupFileGroupResponse.fileGroupId);
  //           }
  //         }
  //       }
  //     }
  //   }
  // }

  return 'complete';
};

module.exports.createBoardByCopy = async (uid, folderId, boardId, isWithData) => {
  try {
    /* For creating board */

    /* We are not use findOne method, because inside (duplicateBoards) we need to pass array value */
    const boardList = await models.repositoryworkspacefolderboard.findAll({
      where: {
        workspaceFolderBoardId: boardId,
      },
    });
    if (boardList) {
      await module.exports.duplicateBoards(uid, boardList, isWithData, folderId);
    }

    return { code: 200, status: 'success', response: 'done' };
  } catch (error) {
    console.error(error);
    return {
      code: 400,
      status: 'failed',
      response: 'Something went wrong',
      error,
    };
  }
};

async function finishCopy(newrecordId, rootreference, oldrecordId, fileGroupId) {
  for (let i = 0; i < rootreference.length; i++) {
    var oldColId = rootreference[i];
    const grabDataRow = await models.filegrouprecordfile.findAll({
      where: {
        recordId: oldrecordId,
        colId: oldColId,
      },
    });
    if (grabDataRow) {
      grabDataRow.map(async (single) => {
        const retriveColarray = await models.filegroupcolumn.findAll({
          where: {
            fileGroupId,
            rootreference: oldColId,
          },
        });

        for (let j = 0; j < retriveColarray.length; j++) {
          await models.filegrouprecordfile.create({
            recordId: newrecordId,
            colId: retriveColarray[j].colId,
            fileName: single.fileName,
            fileLocation: single.fileLocation,
            status: single.status,
            text: single.text,
            people: single.people,
            date: single.date,
            daterange_start: single.daterange_start,
            daterange_end: single.daterange_end,
            uid: single.uid,
          });
        }
      });
    }
  }
}

async function clearBoard(boardIds) {
  try {
    boardIds.map(async (single) => {
      const workspaceFolderBoardId = single;
      await models.repositoryworkspacefolderboard.destroy({
        where: { workspaceFolderBoardId },
      });
      await models.filegrouprecord.destroy({ where: { type: 'board', linkId: workspaceFolderBoardId } });

      const fileGroups = await models.filegroup.findAll({
        where: {
          boardId: workspaceFolderBoardId,
        },
      });

      fileGroups.map(async (singleFileGroup) => {
        const { fileGroupId } = singleFileGroup;

        await models.filegroup.destroy({ where: { fileGroupId } });
        await models.filegroupcolumn.destroy({ where: { fileGroupId } });

        const fileGroupRecords = await models.filegrouprecord.findAll({
          where: {
            fileGroupId,
          },
        });

        fileGroupRecords.map(async (singleGroupRecord) => {
          const { recordId } = singleGroupRecord;
          console.log('dasfghjkd', recordId);
          await models.filegrouprecord.destroy({ where: { recordId } });
          await models.filegrouprecordfile.destroy({ where: { recordId } });
        });
      });
    });
    return 1;
  } catch (err) {
    console.error(err);
    return err;
  }
}
