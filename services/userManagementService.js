const { models } = require('../database/index');
const { checkIsAdmin } = require('../helpers/isCheck');

module.exports.getoTotalUsers = () => models.user.count();

module.exports.getUsers = async (limit, offset) => {
  const getResult = await models.user.findAll({
    include: {
      model: models.organization,
    },
    limit,
    offset,
    order: [['uid', 'DESC']],
  });
  return getResult;
};

module.exports.updateUser = async (id, adminId) => {
  const getResult = await models.organization.findOne({
    where: {
      organizationId: id,
    },
  });

  if (getResult) {
    getResult.requestAccepted = 'true';
    getResult.approvedBy = adminId;
    await getResult.save();
    return {
      status: 'success',
      updated: true,
    };
  }
  return {
    status: 'failed',
    updated: false,
  };
};

module.exports.updateUser = async (id, body, adminId) => {
  try {
    const organizationUser = await models.organizationuser.findByPk(id);

    if (!organizationUser) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'Not found',
      };
    }

    const isAdmin = await checkIsAdmin(adminId, organizationUser.organizationId);

    if (!isAdmin) {
      return {
        statusCode: 401,
        status: 'failed',
        response: 'Unauthorized',
      };
    }
    const { role, department } = body;
    if (role) {
      organizationUser.role = role;
    }
    if (department) {
      organizationUser.department = department;
    }

    const updatedUser = await organizationUser.save();
    return {
      statusCode: 200,
      status: 'success',
      response: updatedUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};

module.exports.updateProfile = async (userId, body) => {
  try {
    const user = await models.user.findByPk(userId, {
      attributes: [
        'uid',
        'first_name',
        'last_name',
        'full_name',
        'username',
        'email',
        'type',
        'phone',
        'updated_at',
        'profileImage',
        //'department',
      ],
      include: [
        {
          model: models.organization,
        },
      ],
    });

    if (!user) {
      return {
        statusCode: 404,
        status: 'failed',
        response: 'User not found',
      };
    }

    const { last_name, first_name } = body;

    if (last_name) {
      user.last_name = last_name;
    }

    if (first_name) {
      user.first_name = first_name;
    }

    const updatedUser = await user.save();
    return {
      statusCode: 200,
      status: 'success',
      response: updatedUser,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};
