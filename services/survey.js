const { Op, where } = require('sequelize');
const { models } = require('../database/index');
const { checkIsAdmin } = require('../helpers/isCheck');
const question = require('../models/question');

module.exports.createSurvey = async (name, writer, start_date, end_date, questionArray, typeArray, rType) => {
    try {
      const createResponse = await models.survey.create({
        name, 
        writer, 
        start_date, 
        end_date,
        rType
      }).then(result => {
        let surveyId = result.dataValues.surveyId
        let name, type = ''
        let array = new Array
        array = questionArray
        for(var i = 0; i<array.length; i++) {
          name = questionArray[i]
          type = typeArray[i]
          models.question.create({
            name, 
            type, 
            surveyId
          })
        }
      })
      .catch(err => {
        console.log("fail");
      })
      
      return {
        status: 'success',
        response: createResponse
      };
    } catch (error) {
      console.error(error);
      return {
        status: 'failed',
        response: 'Something went wrong!',
      };
    }
  };

module.exports.getDoSurveyList = async () => {
    try {
      const today = new Date()
      const getList = await models.survey.findAll({
        where: {
          start_date: { [Op.lte]: today }, 
          end_date: { [Op.gte]: today },
      }}
      );
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
  };

module.exports.getSurveyList = async () => {
    try {
      const getList = await models.survey.findAll();
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
  };

module.exports.getQeustionList = async (surveyId) => {
    try {
      const getList = await models.question.findAll({where: { surveyId: surveyId }});
      return {
        statusCode: 200,
        status: 'success',
        response: getList,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        status: 'failed',
        response: 'Unexpected error',
      };
    }
};

module.exports.responseSurvey = async (respondent, answerArray) => {
  try {
    let array = new Array
    array = answerArray
    for(var i = 0; i<array.length; i++) {
      if(answerArray[i] == null) continue

      let questionId = i
      let answer = answerArray[i]

      const createResponse = await models.questionAnswer.create({
        answer,
        respondent,
        questionId
      })

      if(i == array.length-1) {return {
        status: 'success',
        response: createResponse
      };}
    }

  } catch (error) {
    console.error(error);
    return {
      status: 'failed',
      response: 'Something went wrong!',
    };
  }
};

module.exports.removeSurvey = async (surveyId) => {
  try {
    const survey = await models.survey.findByPk(surveyId);

    if (!survey) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'User not found',
      };
    }

    const deletedSurvey = await survey.destroy();
    return {
      code: 200,
      status: 'success',
      response: deletedSurvey,
    };
  } catch (error) {
    console.error(error);
    return {
      code: 400,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.getQeustionAnswerList = async (questionId) => {
  try {
    const getList = await models.questionAnswer.findAll({where: { questionId: questionId }});
    return {
      statusCode: 200,
      status: 'success',
      response: getList,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      response: 'Unexpected error',
    };
  }
};