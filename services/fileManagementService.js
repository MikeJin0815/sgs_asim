const { nanoid } = require('nanoid');
const { Op } = require('sequelize');
const { models } = require('../database/index');
const { validationCheck } = require('../helpers/isCheck');

module.exports.groupCreate = async (userId, boardId, groupName) => {
  try {
    const createFileGroup = await models.filegroup.create({
      uid: userId,
      boardId,
      fileSystemGroupName: groupName,
    });

    return {
      statusCode: 200,
      status: 'success',
      message: createFileGroup,
    };
  } catch (error) {
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error File Group',
    };
  }
};

module.exports.updateGroupListOrder = async (groupOrders) => {
  try {
    const data = groupOrders.map((group) => ({
      fileGroupId: group.id,
      order: group.order,
    }));
    await models.filegroup.bulkCreate(data, { updateOnDuplicate: ['order'] });

    return {
      statusCode: 200,
      status: 'success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      response: 'Something went wrong!',
      error,
    };
  }
};

module.exports.groupUpdate = async (groupId, body) => {
  try {
    const filegroup = await models.filegroup.findByPk(groupId);

    if (!filegroup) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'File group not found',
      };
    }

    const { groupName } = body;

    if (groupName) {
      filegroup.fileSystemGroupName = groupName;
    }
    const updatedFilegroup = await filegroup.save();

    return {
      statusCode: 200,
      status: 'success',
      message: updatedFilegroup,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error renaming group',
    };
  }
};

module.exports.groupDelete = async (groupId) => {
  try {
    const filegroup = await models.filegroup.findByPk(groupId);

    if (!filegroup) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'File group not found',
      };
    }

    const deletedFilegroup = await filegroup.destroy();

    return {
      statusCode: 200,
      status: 'success',
      message: deletedFilegroup,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error renaming group',
    };
  }
};

module.exports.groupColumnCreate = async (userId, fileGroupId, headerName, headerType, colorder) => {
  try {
    const createColumn = await models.filegroupcolumn.create({
      uid: userId,
      fileGroupId,
      headerName,
      headerType,
      colorder,
    });
    /* Get all the records from the group to create an empty fileGroupRecord */
    const recordsFromGroup = await models.filegrouprecord.findAll({
      where: {
        fileGroupId,
      },
    });
    /*
     * Create array of promises to create all the records simultaneously for better optimization
     * but still wait for them to be all created before sending success request to avoid possible problem front-side if the user try
     * to update a recordFile before it has been created
     */
    const promises = [];
    recordsFromGroup.forEach((record) => {
      promises.push(
        models.filegrouprecordfile.create({
          recordId: record.recordId,
          colId: createColumn.colId,
        })
      );
    });
    await Promise.all(promises);

    return {
      statusCode: 200,
      status: 'success',
      message: createColumn,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error Creating record',
    };
  }
};

module.exports.groupRecordCreate = async (userId, fileGroupId, title) => {
  try {
    const createRecord = await models.filegrouprecord.create({
      uid: userId,
      fileGroupId,
      title,
    });

    return {
      statusCode: 200,
      status: 'success',
      message: createRecord,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error creating record',
    };
  }
};

module.exports.groupRecordDelete = async (recordId) => {
  try {
    const record = await models.filegrouprecord.findByPk(recordId);

    if (!record) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'Record not found',
      };
    }

    const deletedRecord = await record.destroy();

    return {
      statusCode: 200,
      status: 'success',
      message: deletedRecord,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error deleting record',
    };
  }
};

module.exports.groupRecordUpdate = async (recordId, body) => {
  try {
    const record = await models.filegrouprecord.findByPk(recordId);

    if (!record) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'Record not found',
      };
    }

    const { title } = body;

    if (title) {
      record.title = title;
    }
    const updatedRecord = await record.save();

    return {
      statusCode: 200,
      status: 'success',
      message: updatedRecord,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error renaming record',
    };
  }
};

module.exports.groupRecordFileCreate = async (payload) => {
  try {
    /* Before insertion need to to check whether colId is valid or not */
    const { recordId, colId } = payload;
    const valid = await validationCheck(recordId, colId);
    if (valid == false) {
      return {
        statusCode: 400,
        status: 'failed',
        message: 'Error Creating record. recordId or colId is invalid.',
      };
    }

    /* Creating payload obj */
    const getObject = Object.keys(payload);
    const creatObject = {};
    await getObject.map((single) => {
      console.log(single, payload[single]);
      if (single === 'people') {
        const tofilter = payload[single].split(',');
        const filtered = [];
        tofilter.map((single) => {
          const filteredStr = single.replace(/[^0-9]/g, '');
          if (filteredStr) {
            filtered.push(filteredStr);
          }
        });
        if (filtered.length > 0) {
          creatObject[single] = filtered.toString();
        }
      } else if (single === 'status') {
        if (payload[single]) {
          creatObject[single] = payload[single];
        }
      } else {
        creatObject[single] = payload[single];
      }
    });
    const createRecord = await models.filegrouprecordfile.create(creatObject);
    return {
      statusCode: 200,
      status: 'success',
      message: createRecord,
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while creating new record.',
    };
  }
};

module.exports.groupRecordFileDelete = async (recordFileId) => {
  try {
    const file = await models.filegrouprecordfile.findByPk(recordFileId);

    if (!file) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'File not found',
      };
    }

    const deletedFile = await file.destroy();
    return {
      statusCode: 200,
      status: 'success',
      message: deletedFile,
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while deleting file.',
    };
  }
};

module.exports.groupRecordFileEdit = async (recordFileId, payload) => {
  try {
    const record = await models.filegrouprecordfile.findOne({
      where: {
        recordFileId,
      },
      attributes: [
        'recordFileId',
        'recordId',
        'fileName',
        'fileLocation',
        'statusId',
        'text',
        'people',
        'date',
        'daterange_start',
        'daterange_end',
      ],
    });

    if (!record) {
      return { statusCode: 404, status: 'failed', message: 'Record not found' };
    }

    /* Loop through payload keys and update record field if content is different */
    const getObject = Object.keys(payload);
    getObject.map((key) => {
      if (record[key] !== payload[key]) {
        record[key] = payload[key];
      }
    });

    await record.save();
    return { statusCode: 200, status: 'success', message: 'Record Updated.' };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while updating record.',
    };
  }
};

module.exports.groupListByboardId = async (boardId) => {
  try {
    const getRecord = await models.filegroup.findAll({
      where: {
        boardId,
      },
      order: [['order', 'ASC']],
    });
    return {
      statusCode: 200,
      status: 'success',
      message: getRecord,
    };
  } catch (error) {
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error retriving record',
    };
  }
};

module.exports.getDownloadFilesByGroups = async (fileGroupIds, type = 'docx') => {
  try {
    const records = await models.filegrouprecord.findAll({
      where: {
        fileGroupId: fileGroupIds,
      },
      include: {
        model: models.subgroup,
        as: 'subgroup',
        include: {
          model: models.filegroup,
          as: 'filegroup',
          include: {
            model: models.filegrouprecord,
          },
        },
      },
    });

    const recordsFileIds = [];
    records.forEach((record) => {
      recordsFileIds.push(record.recordId);
      if (record.subgroup) {
        record.subgroup.filegroup.filegrouprecords.forEach((subgroupRecord) => {
          recordsFileIds.push(subgroupRecord.recordId);
        });
      }
    });
    const recordsFile = await models.filegrouprecordfile.findAll({
      where: {
        recordId: recordsFileIds,
      },
    });

    if (!recordsFile || recordsFile.length <= 0) {
      return [];
    }

    const supportFileExtension = `.${type}`;
    const files = recordsFile.filter((file) => file.fileLocation && file.fileLocation.endsWith(supportFileExtension));

    return files.map((file) => ({
      value: file.fileLocation,
      name: file.fileName,
    }));
  } catch (error) {
    console.log(error);
    return [];
  }
};

module.exports.getDownloadFilesByFolderBoard = async (workspaceId, folderId, boardId, type = 'docx') => {
  try {
    let folderBoards = [];
    const fileGroupsIds = [];

    if (folderId && boardId) {
      folderBoards = await models.repositoryworkspacefolderboard.findAll({
        where: {
          workspaceFolderId: folderId,
          workspaceFolderBoardId: boardId,
        },
        include: [
          {
            model: models.filegroup,
            separate: true,
            order: [
              ['order', 'ASC'],
              ['fileGroupId', 'ASC'],
            ],
          },
        ],
      });
    } else if (!folderId && !boardId) {
      const folders = await models.repositoryworkspacefolder.findAll({
        where: {
          workspaceId,
        },
      });
      const folderIds = folders.map((folder) => folder.workspaceFolderId);

      const mainGroups = await models.filegroup.findAll({
        where: { [Op.or]: [{ workspaceId }, { workspaceFolderId: folderIds }] },
      });
      mainGroups.forEach((group) => {
        fileGroupsIds.push(group.fileGroupId);
      });

      folderBoards = await models.repositoryworkspacefolderboard.findAll({
        where: {
          workspaceFolderId: folders.map((f) => f.workspaceFolderId),
        },
        include: [
          {
            model: models.filegroup,
            separate: true,
            order: [
              ['order', 'ASC'],
              ['fileGroupId', 'ASC'],
            ],
          },
        ],
      });
    } else {
      const folderGroup = await models.filegroup.findOne({
        where: { workspaceFolderId: folderId },
      });
      if (folderGroup) {
        fileGroupsIds.push(folderGroup.fileGroupId);
      }
      folderBoards = await models.repositoryworkspacefolderboard.findAll({
        where: {
          workspaceFolderId: folderId,
        },
        include: [
          {
            model: models.filegroup,
            separate: true,
            order: [
              ['order', 'ASC'],
              ['fileGroupId', 'ASC'],
            ],
          },
        ],
      });
    }

    folderBoards.forEach((item) => {
      const fileGroups = item.dataValues.filegroups;
      fileGroups.forEach((fileGroup) => {
        fileGroupsIds.push(fileGroup.fileGroupId);
      });
    });

    return module.exports.getDownloadFilesByGroups(fileGroupsIds, type);
  } catch (error) {
    console.log(error);
    return [];
  }
};

module.exports.groupColumnSequence = async (fileGroupId) => {
  try {
    const getCols = await models.filegroupcolumn.findAll({
      where: {
        fileGroupId,
        active: true,
      },
      order: [
        ['colorder', 'ASC'],
        ['colId', 'ASC'],
      ],
      include: models.status,
    });
    /* Getting First record of **fileGroupId** */
    const records = await models.filegrouprecord.findAll({
      where: {
        fileGroupId,
      },
      order: [['order', 'ASC']],
      include: { model: models.subgroup, as: 'subgroup' },
    });
    const row = [];
    if (records.length > 0) {
      for (let i = 0; i < records.length; i++) {
        const { recordId } = records[i];
        const temp = [];
        temp.push({ titleInfo: records[i] });

        /* Getting records of file if there is multiple or single by colId */
        for (let j = 0; j < getCols.length; j++) {
          const recordsFile = await models.filegrouprecordfile.findAll({
            where: {
              recordId,
              colId: getCols[j].colId,
            },
            include: [models.status],
            // Attributes: ['recordFileId', 'recordId','fileName','fileLocation', 'status', 'text', 'people', 'date', 'daterange_start', 'daterange_end']
          });
          temp.push({
            colDef: {
              colName: getCols[j].headerName,
              colType: getCols[j].headerType,
              colId: getCols[j].colId,
            },
            colInfo: recordsFile,
          });
        }
        row.push(temp);
      }
    }

    return {
      statusCode: 200,
      status: 'success',
      message: {
        column: getCols,
        data: row,
        dataRowLength: row.length,
      },
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error retrieving record',
      error,
    };
  }
};

module.exports.groupColumnEdit = async (colId, body) => {
  try {
    const getColName = await models.filegroupcolumn.findOne({
      where: {
        colId,
      },
    });
    if (getColName) {
      if (body.headerName) {
        getColName.headerName = body.headerName;
      }
      if (body.width) {
        getColName.width = body.width;
      }
      getColName.save();
      return { statusCode: 200, status: 'success', message: 'Column Updated.' };
    }
    return {
      statusCode: 404,
      status: 'failed',
      message: 'Column name not found',
    };
  } catch (error) {
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while updating the column name',
    };
  }
};

module.exports.groupColumnStatus = async (colId, status) => {
  try {
    const getColName = await models.filegroupcolumn.findOne({
      where: {
        colId,
      },
    });
    if (getColName) {
      getColName.active = status;
      getColName.save();
      return {
        statusCode: 200,
        status: 'success',
        message: 'Column status Updated.',
      };
    }
    return { statusCode: 404, status: 'failed', message: 'Column  not found' };
  } catch (error) {
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while updating the column status',
    };
  }
};

module.exports.groupColumnDelete = async (colId) => {
  try {
    const column = await models.filegroupcolumn.findByPk(colId);
    if (!column) {
      return {
        statusCode: 404,
        status: 'failed',
        message: 'Column name not found',
      };
    }

    const deletedColumn = await column.destroy();
    return {
      statusCode: 200,
      status: 'success',
      message: deletedColumn,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while updating the column status',
    };
  }
};

module.exports.createStatus = async (colId, title, color) => {
  try {
    const createdStatus = await models.status.create({
      colId,
      title,
      color,
    });
    return {
      statusCode: 200,
      status: 'success',
      message: createdStatus,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error when creating status',
    };
  }
};

module.exports.editStatus = async (statusId, title, color) => {
  try {
    const status = await models.status.findByPk(statusId);
    if (title) {
      status.title = title;
    }
    if (color) {
      status.color = color;
    }
    const updatedStatus = await status.save();
    return {
      statusCode: 200,
      status: 'success',
      message: updatedStatus,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error when editing status',
    };
  }
};

module.exports.deleteStatus = async (statusId) => {
  try {
    const status = await models.status.findByPk(statusId);
    if (!status) {
      return {
        statusCode: 404,
        status: 'failed',
        message: 'Status not found',
      };
    }

    const recordsUsingStatus = await models.filegrouprecordfile.findAll({
      where: {
        statusId,
      },
    });
    if (recordsUsingStatus?.length) {
      return {
        statusCode: 403,
        status: 'failed',
        message: "Can't delete a status being used",
      };
    }

    await status.destroy();
    return {
      statusCode: 200,
      status: 'success',
      message: 'Status deleted',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error when deleting status',
    };
  }
};

module.exports.subGroupCreate = async (recordId) => {
  try {
    const record = await models.filegrouprecord.findByPk(recordId);
    if (!record) {
      return {
        statusCode: 404,
        status: 'not found',
        message: 'Record not found',
      };
    }
    const newGroup = await models.filegroup.create({
      fileSystemGroupName: 'Subgroup',
    });

    const createdSubGroup = await models.subgroup.create({
      recordId,
      fileGroupId: newGroup.fileGroupId,
    });
    await record.setSubgroup(createdSubGroup);

    return {
      statusCode: 200,
      status: 'success',
      message: record,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error File Group',
    };
  }
};

module.exports.getApplicationForm = async (applicationFormId) => {
  try {
    const applicationForm = await models.applicationform.findOne({
      where: {
        applicationFormId,
      },
      include: {
        model: models.applicationformentry,
        as: 'applicationformentry',
      },
    });
    if (applicationForm) {
      // applicationForm.formContent = JSON.parse(applicationForm.formContent);

      return {
        statusCode: 200,
        status: 'success',
        message: applicationForm,
      };
    }
    return {
      statusCode: 404,
      status: 'failed',
      message: 'Application form not found',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while retreiving the Application form',
    };
  }
};

module.exports.applicationFormCreate = async (boardId, formContent) => {
  try {
    const createdForm = await models.applicationform.create({
      boardId,
      formContent,
    });
    return {
      statusCode: 200,
      status: 'success',
      message: createdForm,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error when creating application form',
    };
  }
};
module.exports.applicationFormSave = async (applicationFormId, payload) => {
  try {
    const appForm = await models.applicationform.findOne({
      where: {
        applicationFormId,
      },
      attributes: ['applicationFormId', 'formContent', 'isActive', 'updated_at'],
    });

    // console.log(appForm)
    if (!appForm) {
      return {
        statusCode: 404,
        status: 'failed',
        message: 'Application form not found',
      };
    }

    /* Loop through payload keys and update appForm field if content is different */
    const getObject = Object.keys(payload);
    getObject.map((key) => {
      if (appForm[key] !== payload[key]) {
        appForm[key] = payload[key];
      }
    });
    appForm.updated_at = new Date();

    await appForm.save();
    return {
      statusCode: 200,
      status: 'success',
      message: 'Application Form Updated.',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while saving Application Form.',
    };
  }
};

module.exports.applicationFormGenerateLink = async (applicationFormId) => {
  try {
    const appForm = await models.applicationform.findOne({
      where: {
        applicationFormId,
      },
      attributes: ['applicationFormId', 'publictoken'],
    });

    // console.log(appForm)
    if (!appForm) {
      return {
        statusCode: 404,
        status: 'failed',
        message: 'Application form not found',
      };
    }

    const token = `${applicationFormId}-${nanoid()}`;

    /* Loop through payload keys and update appForm field if content is different */

    appForm.publictoken = token;

    const saved = await appForm.save();
    // console.log(saved)

    return { statusCode: 200, status: 'success', message: saved };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while generating publicToken for Application Form.',
    };
  }
};

module.exports.getApplicationFormByToken = async (publictoken) => {
  console.log('getApplicationFormByToken');
  try {
    const applicationForm = await models.applicationform.findOne({
      where: {
        publictoken,
      },
      attributes: ['formContent'],
    });
    if (applicationForm) {
      // applicationForm.formContent = JSON.parse(applicationForm.formContent);

      return {
        statusCode: 200,
        status: 'success',
        message: applicationForm,
      };
    }
    return {
      statusCode: 404,
      status: 'failed',
      message: 'Application form not found',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while retreiving the Application form',
    };
  }
};

module.exports.applicationFormAddEntry = async (publictoken, payload) => {
  try {
    const applicationForm = await models.applicationform.findOne({
      where: {
        publictoken,
      },
    });
    if (applicationForm) {
      console.log(payload);

      try {
        const appFormEntry = await models.applicationformentry.create({
          applicationFormId: applicationForm.applicationFormId,
          entryData: payload,
        });
        return {
          statusCode: 200,
          status: 'success',
          message: appFormEntry,
        };
      } catch (err) {
        console.log(err);
        return {
          statusCode: 404,
          status: 'failed',
          message: 'Cannot save Application form Entry',
        };
      }
    }
    return {
      statusCode: 404,
      status: 'failed',
      message: 'Application form not found',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 400,
      status: 'failed',
      message: 'Error while retreiving the Application form',
    };
  }
};

module.exports.groupColumnOrderEdit = async (columnsOrder) => {
  try {
    columnsOrder.forEach(async (colId, idx) => {
      // models.filegroupcolumn.update()
      const column = await models.filegroupcolumn.findOne({
        where: {
          colId,
        },
      });
      if (column) {
        column.colorder = idx + 1;
        column.save();
      }
    });
    return {
      statusCode: 200,
      status: 'success',
      message: 'Column status Updated.',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error while saving columns order',
    };
  }
};

module.exports.groupRowOrderEdit = async (rowsOrder) => {
  try {
    rowsOrder.forEach(async (recordId, idx) => {
      const record = await models.filegrouprecord.findOne({
        where: {
          recordId,
        },
      });
      if (record) {
        record.order = idx + 1;
        record.save();
      }
    });
    return {
      statusCode: 200,
      status: 'success',
      message: 'Rows order updated.',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      status: 'failed',
      message: 'Error while saving rows order',
    };
  }
};
