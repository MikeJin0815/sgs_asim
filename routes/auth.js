const authController = require('../controllers/auth');
const { verifytoken } = require('../middlewares/authentication');
const { registrationBySocialLogin, socialLogin } = require('../services/auth');

const passport = require('passport');
const KakaoStrategy = require('passport-kakao').Strategy;
const NaverStrategy = require('passport-naver').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth2').Strategy;

const { default: axios } = require('axios');

module.exports = router => {
  /* Authentication related route */
  router.post(
    '/registration/open',
    authController.registrationOpen
  ); /* This EP will be used in first registration Page */
  router.get('/me', verifytoken, authController.getUserInfo);

  router.post(
    '/registration/byadmin',
    verifytoken,
    authController.registrationByAdmin
  ); /* This EP will be used by admin to create and sending invitaion to new user */
  router.post(
    '/registration/complete/byuser',
    authController.regCompleteByUser
  ); /* This EP will be used by admin to create and sending invitaion to new user */
  router.post('/login', authController.login);
  router.post('/forgetPassword', authController.forgetPassword);
  router.post('/forgetUsername', authController.forgetUserName);
  router.post('/verifyCode', authController.verifyUserCode);
  router.post('/resetPassword', authController.resetPassword);
  router.post('/sendOtp', authController.sendPhoneOtp);
  router.post('/verifyOtp', authController.verifyPhoneOtp);
  router.post('/check/email', authController.checkEmail);
  router.post('/check/username', authController.checkUserName);

  /* Kakao Login */
  passport.use(
    'kakao',
    new KakaoStrategy(
      {
        clientID: process.env.KAKAO_APP_ID,
        callbackURL: `${process.env.HOSTIP}/api/v1/auth/kakao/callback`,
      },
      async (accessToken, refreshToken, profile, done) => {
        const regBySocialLogin = await registrationBySocialLogin(
          profile._json.kakao_account.email,
          profile.displayName,
          null,
          'self',
          'kakao',
          profile.id,
          profile._json.properties.profile_image
        );
        if (regBySocialLogin == 1) {
          const response = await socialLogin(
            profile.id,
            profile._json.kakao_account.email
          );
          return done(null, response);
        }

        const response = await socialLogin(
          profile.id,
          profile._json.kakao_account.email
        );
        return done(null, response);

        /*
         * Data from Passport kakao
         *  return done(JSON.stringify({
         *      "id" : profile.id,
         *      "provider" : profile.provider,
         *      "name" : profile.displayName,
         *      "email" : profile._json.kakao_account.email,
         *      "image" : profile._json.properties.profile_image,
         *      "accesstoken" : accessToken,
         *      "refreshtoken" : refreshToken
         *  }));
         */
      }
    )
  );
  router.get('/kakao', passport.authenticate('kakao'));
  router.get(
    '/kakao/callback',
    passport.authenticate('kakao', {
      failureRedirect: `${process.env.CLIENT_URL}`,
    }),
    (req, res) => {
      const url = `social-login?token=${req.user.token}&request=${req.user.request}&approvedBy=${req.user.approvedBy}`;
      if (req.user.request == 'accepted') {
        res.redirect(`${process.env.CLIENT_URL}/${url}`);
      } else {
        res.redirect(`${process.env.CLIENT_URL}`);
      }

      /*
       *Passport authentication return us callback information inside {req.user} object
       *res.json(req.user);
       */
    }
  );

  /* Naver Login */
  passport.use(
    'naver',
    new NaverStrategy(
      {
        clientID: process.env.NAVER_APP_ID,
        clientSecret: process.env.NAVER_APP_SECRET,
        callbackURL: `${process.env.HOSTIP}/api/v1/auth/naver/callback`,
      },
      async (accessToken, refreshToken, profile, done) => {
        const regBySocialLogin = await registrationBySocialLogin(
          profile._json.email,
          profile.displayName,
          null,
          'self',
          'naver',
          profile.id,
          profile._json.profile_image
        );

        if (regBySocialLogin == 1) {
          const response = await socialLogin(profile.id, profile._json.email);
          return done(null, response);
        }

        const response = await socialLogin(profile.id, profile._json.email);
        return done(null, response);

        /*
         * Data from Passport Navar
         *  return done(JSON.stringify({
         *      "id" : profile.id,
         *      "provider" : profile.provider,
         *      "name" : profile.displayName,
         *      "email" : profile._json.email,
         *      "image" : profile._json.profile_image,
         *      "accesstoken" : accessToken,
         *      "refreshtoken" : refreshToken
         *  }));
         */
      }
    )
  );
  router.get('/naver', passport.authenticate('naver'));
  router.get(
    '/naver/callback',
    passport.authenticate('naver', {
      failureRedirect: `${process.env.CLIENT_URL}`,
    }),
    (req, res) => {
      const url = `social-login?token=${req.user.token}&request=${req.user.request}&approvedBy=${req.user.approvedBy}`;
      if (req.user.request == 'accepted') {
        res.redirect(`${process.env.CLIENT_URL}/${url}`);
      } else {
        res.redirect(`${process.env.CLIENT_URL}`);
      }

      /*
       *Passport authentication return us callback information inside {req.user} object
       *res.json(req.user);
       */
    }
  );

  /* Facebook Login */
  passport.use(
    'facebook',
    new FacebookStrategy(
      {
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        callbackURL: `${process.env.HOSTIP}/api/v1/auth/facebook/callback`,
      },
      async (accessToken, refreshToken, profile, done) => {
        /* For basic info */
        const url = `${
          'https://graph.facebook.com/v3.2/me?'
          + 'fields=id,name,email,first_name,last_name&access_token='
        }${accessToken}`;
        const resp = await axios.get(url);
        /* For profile photo icon */
        const picture = `https://graph.facebook.com/me/picture?access_token=${accessToken}&&redirect=false`;
        const picturedata = await axios.get(picture);
        const profilePhoto = picturedata.data.data.url;
        const regBySocialLogin = await registrationBySocialLogin(
          resp.data.email,
          resp.data.name,
          null,
          'self',
          'facebook',
          resp.data.id,
          profilePhoto,
        );

        if (regBySocialLogin == 1) {
          const response = await socialLogin(resp.data.id, resp.data.email);
          return done(null, response);
        }

        const response = await socialLogin(resp.data.id, resp.data.email);
        return done(null, response);

        /*
         * Data from Passport Facebook
         *  return done(JSON.stringify({
         *      "id" : resp.data.id,
         *      "provider" : "facebook",
         *      "name" : resp.data.name,
         *      "email" : resp.data.email,
         *      "image" : profilePhoto,
         *      "accesstoken" : accessToken,
         *      "refreshtoken" : refreshToken
         *  }));
         */
      }
    )
  );
  router.get(
    '/facebook',
    passport.authenticate('facebook', { scope: ['email'] })
  );
  router.get(
    '/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: `${process.env.CLIENT_URL}`,
    }),
    (req, res) => {
      const url = `social-login?token=${req.user.token}&request=${req.user.request}&approvedBy=${req.user.approvedBy}`;
      if (req.user.request == 'accepted') {
        res.redirect(`${process.env.CLIENT_URL}/${url}`);
      } else {
        res.redirect(`${process.env.CLIENT_URL}`);
      }

      /*
       *Passport authentication return us callback information inside {req.user} object
       *res.json(req.user);
       */
    }
  );

  /* Gmail Login */
  passport.use(
    'google',
    new GoogleStrategy(
      {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: `${process.env.HOSTIP}/api/v1/auth/google/callback`,
      },
      async (accessToken, refreshToken, profile, done) => {
        const regBySocialLogin = await registrationBySocialLogin(
          profile.email,
          profile.displayName,
          null,
          'self',
          'google',
          profile.id,
          profile.picture
        );

        if (regBySocialLogin == 1) {
          const response = await socialLogin(profile.id, profile.email);
          return done(null, response);
        }

        const response = await socialLogin(profile.id, profile.email);
        return done(null, response);

        /*
         * Data from Passport Google
         *  return done(JSON.stringify({
         *      "id" : profile.id,
         *      "provider" : "google",
         *      "name" : profile.displayName,
         *      "email" : profile.email,
         *      "image" : profile.picture,
         *      "accesstoken" : accessToken,
         *      "refreshtoken" : refreshToken
         *  }));
         */
      }
    )
  );
  router.get(
    '/google',
    passport.authenticate('google', { scope: ['email', 'profile'] })
  );
  router.get(
    '/google/callback',
    passport.authenticate('google', {
      failureRedirect: `${process.env.CLIENT_URL}`,
    }),
    (req, res) => {
      const url = `social-login?token=${req.user.token}&request=${req.user.request}&approvedBy=${req.user.approvedBy}`;
      if (req.user.request == 'accepted') {
        res.redirect(`${process.env.CLIENT_URL}/${url}`);
      } else {
        res.redirect(`${process.env.CLIENT_URL}`);
      }

      /*
       *Passport authentication return us callback information inside {req.user} object
       *res.json(req.user);
       */
    }
  );

  passport.serializeUser((user, cb) => {
    cb(null, user);
  });

  passport.deserializeUser((obj, cb) => {
    cb(null, obj);
  });

  return router;
};
