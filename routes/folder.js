const controllerFolder = require('../controllers/folder');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  router.post('/add', verifytoken, controllerFolder.addWorkspaceFolder);
  router.put('/:workspaceFolderId/rename', verifytoken, controllerFolder.updateWorkspaceFolderName);
  router.delete('/:workspaceFolderId/delete', verifytoken, controllerFolder.deleteWorkspaceFolder);
  router.put('/order', verifytoken, controllerFolder.updateFolderListOrder);
  /* Copy Folder */
  router.post('/:workspaceFolderId/add-by-copy-command', verifytoken, controllerFolder.addFolderByCopy);

  return router;
};
