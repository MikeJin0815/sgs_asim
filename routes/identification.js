const identificationController = require('../controllers/identification');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  router.post('/getAdmin', verifytoken, identificationController.getAdminUser);
  router.post('/updateType', verifytoken, identificationController.updateUserType);
  router.post('/create', verifytoken, identificationController.createIdentification);
  router.post('/getIdentification', verifytoken, identificationController.getIdentification);
  router.post('/getUserIdentification', verifytoken, identificationController.getUserIdentification);
  router.post('/approveType', verifytoken, identificationController.updateUserType);
  router.post('/remove', verifytoken, identificationController.removeIdenification);
  router.post('/deny', verifytoken, identificationController.updateIdentification);

  return router;
};