const controllerWorkspace = require('../controllers/workspace');
const { verifytoken } = require('../middlewares/authentication');

module.exports = router => {
  /* Repository-workspace Related EP */
  router.get(
    '/:workspaceId/get',
    verifytoken,
    controllerWorkspace.getWorkSpaceInfo
  );
  router.put(
    '/:workspaceId/rename',
    verifytoken,
    controllerWorkspace.renameWorkspace
  );
  router.post('/add', verifytoken, controllerWorkspace.addWorkspace);
  router.post(
    '/add-by-copy',
    verifytoken,
    controllerWorkspace.addWorkspaceByCopy
  );
  router.delete(
    '/:workspaceId/delete',
    verifytoken,
    controllerWorkspace.deleteWorkspace
  );

  return router;
};
