const organizationController = require('../controllers/organization');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  /* Organization related route */
  router.post('/check', verifytoken, organizationController.checkOrganization);
  router.post('/checkuser', verifytoken, organizationController.checkUser);
  router.post('/check/partial', verifytoken, organizationController.checkOrganizationByPartialSearch);
  router.post('/create', verifytoken, organizationController.createOrganization);
  router.post('/join/request', verifytoken, organizationController.joinRequestOrganization);
  router.post('/join/request/approve', verifytoken, organizationController.requestOrganizationApprove);
  router.get('/get/list', verifytoken, organizationController.list);
  router.get(
    '/get/user/:organizationId/list',
    verifytoken,
    organizationController.listOfUsersByOrg
  ); /* Limit and page */
  router.get('/details/:organizationId/get', verifytoken, organizationController.getOrgDetails);
  router.get('/:subdomain/get', organizationController.getBySubdomain);
  router.delete(
    '/:organizationId/users/:userId/delete',
    verifytoken,
    organizationController.removeUserFromOrganization
  );
  return router;
};
