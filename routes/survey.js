const surveyController = require('../controllers/survey');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  /* Organization related route */
  /* login이 필수적일 경우 verifytoken 포함 */
  router.post('/create', verifytoken, surveyController.createSurvey); 
  router.get('/get/list', verifytoken, surveyController.list);
  router.get('/get/doList', verifytoken, surveyController.doList);
  router.post('/get/questionList', verifytoken, surveyController.questionList);
  router.post('/answer', verifytoken, surveyController.responseSurveyInfo, surveyController.responseSurvey);
  router.delete('/:surveyId/delete', verifytoken, surveyController.removeSurvey);
  router.post('/get/questionAnswerList', verifytoken, surveyController.questionAnswerList);

  return router;
};