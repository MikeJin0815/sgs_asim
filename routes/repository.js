const controllerRepository = require('../controllers/repository');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  /* Repository Related EP */
  router.post('/create', verifytoken, controllerRepository.createRepository); /* No need to work on this route */
  router.get(
    '/:repositoryId/workspacelist/get',
    verifytoken,
    controllerRepository.getRepositoryInfo
  ); /* No need to work on this route */
  /* No need to work on this route */
  /*
  router.put(
    '/:repositoryId/update',
    verifytoken,
    controllerRepository.updateRepositoryName
  );
  */
  router.put('/:repositoryId/update', verifytoken, controllerRepository.updateRepository);
  router.put('/:repositoryId/photo/update', verifytoken, controllerRepository.updateRepositoryPhoto);
  router.delete('/:repositoryId/remove', verifytoken, controllerRepository.deleteRepository);
  router.get('/:repositoryId/user', verifytoken, controllerRepository.repositoryUser);
  router.put('/user/:repositoryUserId', verifytoken, controllerRepository.updateRepositoryUser);
  router.get('/:repositoryId/users', verifytoken, controllerRepository.repositoryUsers);

  /* Repository-User management Related EP */
  router.post('/:repositoryId/adduser', verifytoken, controllerRepository.adduser); /* No need to work on this route */
  router.delete('/:repositoryUserId/removeuser', verifytoken, controllerRepository.removeUser);
  router.post(
    '/workspace/folder/board/:workspaceFolderId/add',
    verifytoken,
    controllerRepository.addWorkspaceFolderBoard
  );
  router.put(
    '/workspace/folder/board/:workspaceFolderBoardId/rename',
    verifytoken,
    controllerRepository.updateWorkspaceFolderBoardName
  );
  router.delete(
    '/workspace/folder/board/:workspaceFolderBoardId/delete',
    verifytoken,
    controllerRepository.deleteBoard
  );
  router.put('/workspace/folder/board/order', verifytoken, controllerRepository.updateBoardListOrder);

  router.get('/workspace/folder/board/:boardId/details', verifytoken, controllerRepository.boardDetails);

  router.post(
    '/workspace/folder/board/:folderId/:boardId/add-by-copy-command',
    verifytoken,
    controllerRepository.addBoardByCopy
  );

  return router;
};
