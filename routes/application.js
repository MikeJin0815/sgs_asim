const applicationController = require('../controllers/application');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  router.post('/create', verifytoken, applicationController.createApplication); 
  router.post('/get', verifytoken, applicationController.getApplicationList);
  router.post('/getAll', verifytoken, applicationController.getAllApplicationList);
  router.post('/remove', verifytoken, applicationController.removeApplication);
  router.post('/update', verifytoken, applicationController.updateApplication); 

  return router;
};