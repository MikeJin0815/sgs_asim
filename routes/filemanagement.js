const fileManagement = require('../controllers/filemanagement');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  /* File management related route */
  router.post('/group/create', verifytoken, fileManagement.groupCreate);
  router.post('/group/:groupId/edit', verifytoken, fileManagement.groupUpdate);
  router.delete('/group/:groupId', verifytoken, fileManagement.groupDelete);
  router.post('/group/column/create', verifytoken, fileManagement.groupColumnCreate);
  router.post('/group/record/create', verifytoken, fileManagement.groupRecordCreate);
  router.post('/group/record/:recordId/edit', verifytoken, fileManagement.groupRecordUpdate);
  router.delete('/group/record/:recordId', verifytoken, fileManagement.groupRecordDelete);
  router.post('/group/record/file/create', verifytoken, fileManagement.groupRecordFileCreate);
  router.delete('/group/record/file/:recordFileId', verifytoken, fileManagement.groupRecordFileDelete);

  /* Get */
  router.get('/group/:boardId/get', verifytoken, fileManagement.groupList);
  router.get('/group/:fileGroupId/column/get', verifytoken, fileManagement.groupColumnSequence);
  router.get('/group/:boardId/getFiles', verifytoken, fileManagement.getFilesByGroups);

  router.get('/group/getFilesByFolderBoard', verifytoken, fileManagement.getFilesByFolderBoard);

  /* Update group order */
  router.put('/group/order', verifytoken, fileManagement.updateGroupListOrder);

  /* Edit column */
  router.put('/group/column/:colId/edit', verifytoken, fileManagement.groupColumnEdit);
  router.put('/group/columnorder/edit', verifytoken, fileManagement.groupColumnOrderEdit);
  router.put('/group/column/:colId/status', verifytoken, fileManagement.groupColumnStatus);
  router.delete('/group/column/:colId', verifytoken, fileManagement.groupColumnDelete);

  /* Edit record */
  router.put('/group/record/:recordFileId/edit', verifytoken, fileManagement.groupRecordFileEdit);
  router.put('/group/recordorder/edit', verifytoken, fileManagement.groupRowOrderEdit);

  /* Status related routes */
  router.post('/status/create', verifytoken, fileManagement.createStatus);
  router.put('/status/:statusId/edit', verifytoken, fileManagement.editStatus);
  router.delete('/status/:statusId/delete', verifytoken, fileManagement.deleteStatus);

  /* SubGroup */
  router.post('/group/subgroup/create', verifytoken, fileManagement.subGroupCreate);
  /* Application Form */

  router.get('/application/:boardId/get', verifytoken, fileManagement.getApplicationForm);

  router.post('/application/:boardId/create', verifytoken, fileManagement.applicationFormCreate);

  router.post('/application/:appformId/save', verifytoken, fileManagement.applicationFormSave);

  router.get('/application/:appformId/generatelink', verifytoken, fileManagement.applicationFormGenerateLink);

  router.post(
    '/application/:publictoken/saveentry',
    // verifytoken,
    fileManagement.applicationFormAddEntry
  );

  router.get(
    '/application/:publictoken/load',
    // verifytoken,
    fileManagement.getApplicationFormByToken
  );

  return router;
};
