const userManagement = require('../controllers/usermanagement');
const { verifytoken } = require('../middlewares/authentication');

module.exports = (router) => {
  /* Organization related route */
  router.get('/get', verifytoken, userManagement.getUsers); /* Limit & page */
  router.put('/approve/:id/update', verifytoken, userManagement.approveUser); /* By admin */
  router.put('/:id/update', verifytoken, userManagement.updateUser); /* By Admin */
  router.patch('/profile/update', verifytoken, userManagement.updateProfile);
  return router;
};
