const express = require('express');
const authRoutes = require('./auth');
const organizationRoutes = require('./organization');
const usermanagementRoutes = require('./usermanagement');
const repositoryRoutes = require('./repository');
const serviceRoutes = require('./service');
const filesRoutes = require('./filemanagement');
const workspaceRoutes = require('./workspace');
const folderRoutes = require('./folder');
const surveyRoutes = require('./survey')
const applicationRoutes = require('./application')
const indentificationRoutes = require('./identification')

module.exports = (app) => {
  const routes = [
    {
      basePath: `${process.env.BASE_PATH}/auth`,
      routes: authRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/organization`,
      routes: organizationRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/user-management`,
      routes: usermanagementRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/repository`,
      routes: repositoryRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/workspace`,
      routes: workspaceRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/folder`,
      routes: folderRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/service`,
      routes: serviceRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/file-management`,
      routes: filesRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/survey`,
      routes: surveyRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/application`,
      routes: applicationRoutes,
    },
    {
      basePath: `${process.env.BASE_PATH}/identification`,
      routes: indentificationRoutes,
    },
  ];

  for (const route of routes) {
    app.use(route.basePath, route.routes(express.Router()));
  }
};
