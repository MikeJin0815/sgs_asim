const s3Service = require('../helpers/service');
const { verifytoken } = require('../middlewares/authentication');
const controllerEmailTemplate = require('../controllers/emailTemplate');

module.exports = router => {
  /* Image file uploading route */
  router.post('/upload', verifytoken, s3Service.uploadImageToS3);
  router.post('/upload/any', verifytoken, s3Service.uploadFilesToS3);

  /* Merge all files and downloads  */
  router.post('/download', verifytoken, s3Service.download);
  router.post('/download-pdf', verifytoken, s3Service.downloadPdf);

  /* Email Template Related services */
  router.post(
    '/template/create',
    verifytoken,
    controllerEmailTemplate.createEmailTemplate
  );

  /* Delete Email Template service */
  router.delete(
    '/template/delete',
    verifytoken,
    controllerEmailTemplate.deleteEmailTemplateByType
  );

  // Get Email Template
  router.post(
    '/template/getTemplate',
    verifytoken,
    controllerEmailTemplate.getEmailTemplate
  );

  return router;
};
