import logging
import os

from docx import Document

from src.document_merge.document_merge import MergeWordDocuments, WordDocumentPersistance
from src.main import logger

DOC_FILES_PATH = [
    "tests/src/document_merge/doc_files/korean_001.docx",
    "tests/src/document_merge/doc_files/korean_002.docx",
    "tests/src/document_merge/doc_files/korean_003.docx",
    "tests/src/document_merge/doc_files/lorem_ipsum_003.docx",
]

DOCUMENTS = [Document(file_path) for file_path in DOC_FILES_PATH]


class TestMergeDocuments:
    def test_add_document(self):
        # Given
        md_instance = MergeWordDocuments(logger)
        # When
        for document_path in DOC_FILES_PATH:
            md_instance.add_document(document_path)

        # Then
        assert len(DOCUMENTS) == len(md_instance.get_documents)
        document_count = len(DOCUMENTS)
        counter_document = 0
        documents_result = md_instance.get_documents
        while counter_document < document_count:
            paragraph_count = len(DOCUMENTS[counter_document].paragraphs)
            counter = 0
            while paragraph_count > counter:
                assert (
                    documents_result[counter_document].paragraphs[counter].text
                    == DOCUMENTS[counter_document].paragraphs[counter].text
                )
                counter += 1
            counter_document += 1

    def test_delete_document_exception(self, caplog):
        # Given
        md_instance = MergeWordDocuments(logger)
        expected_docs = DOCUMENTS[0:1].copy()
        expected_docs.extend(DOCUMENTS[2:])
        # When
        for document_path in DOC_FILES_PATH:
            md_instance.add_document(document_path)

        # Then
        md_instance.delete_document(5)
        with caplog.at_level(logging.ERROR):
            assert "IndexError: list assignment index out of range" in caplog.text

    def test_delete_document(self):
        # Given
        md_instance = MergeWordDocuments(logger)
        expected_docs = DOCUMENTS[0:1].copy()
        expected_docs.extend(DOCUMENTS[2:])
        # When
        for document_path in DOC_FILES_PATH:
            md_instance.add_document(document_path)
        md_instance.delete_document(1)

        # Then
        assert len(md_instance.get_documents) == len(expected_docs)
        document_count = len(expected_docs)
        counter_document = 0
        documents_result = md_instance.get_documents
        while counter_document < document_count:
            paragraph_count = len(expected_docs[counter_document].paragraphs)
            counter = 0
            while paragraph_count > counter:
                assert (
                    documents_result[counter_document].paragraphs[counter].text
                    == expected_docs[counter_document].paragraphs[counter].text
                )
                counter += 1
            counter_document += 1

    def test_merge_documents(self):
        # Given
        expected_merged_docx = Document("tests/src/document_merge/doc_files/merged.docx")
        md_instance = MergeWordDocuments(logger)
        # When
        for document_path in DOC_FILES_PATH:
            md_instance.add_document(document_path)

        composer = md_instance.merge_documents()
        Document()
        composer.save("tests/src/document_merge/doc_files/merge_test.docx")
        with open("tests/src/document_merge/doc_files/merge_test.docx", "rb") as fh:
            paragraph_count = len(expected_merged_docx.paragraphs)
            counter = 0
            result_doc = Document(fh)
            while paragraph_count > counter:
                assert expected_merged_docx.paragraphs[counter].text == result_doc.paragraphs[counter].text
                counter += 1

        os.remove("tests/src/document_merge/doc_files/merge_test.docx")


class TestWordDocumentPersistance:
    def test_save(self):
        # Given
        file_path_to_save = "tests/src/document_merge/doc_files/test_save.docx"
        wdp_intsance = WordDocumentPersistance(file_path_to_save)
        md_instance = MergeWordDocuments(logger)
        expected_merged_docx = Document("tests/src/document_merge/doc_files/merged.docx")
        for document_path in DOC_FILES_PATH:
            md_instance.add_document(document_path)

        # When
        composer = md_instance.merge_documents()
        wdp_intsance.save_documents(composer)

        # Then
        assert os.path.exists("tests/src/document_merge/doc_files/test_save.docx")
        with open("tests/src/document_merge/doc_files/test_save.docx", "rb") as fh:
            paragraph_count = len(expected_merged_docx.paragraphs)
            counter = 0
            result_doc = Document(fh)
            while paragraph_count > counter:
                assert expected_merged_docx.paragraphs[counter].text == result_doc.paragraphs[counter].text
                counter += 1

        os.remove("tests/src/document_merge/doc_files/test_save.docx")
