import json
import logging
import secrets
import time
import uuid
from io import BytesIO
from logging.config import fileConfig
from typing import List

import requests
from fastapi import Depends, FastAPI, HTTPException, Request, Response, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from retry import retry

from src.document_merge.document_merge import MergeWordDocuments, WordDocumentPersistance
from src.utils.config import Config

from .models import InputModel, InputModelUrl

fileConfig("logger.conf", disable_existing_loggers=False)

logger = logging.getLogger("root")

app = FastAPI(title="Word DocX Merge", version="0.1.1")

security = HTTPBasic()


file = open("appconfig.json")
json_formatted = json.loads(file.read())
config = Config(**json_formatted)
file.close()

if not config:
    raise Exception("Config not found")


@app.middleware("http")
async def log_requests(request: Request, call_next):
    idem = str(uuid.uuid4())
    logger.info(f"rid={idem} start request path={request.url.path}")
    start_time = time.time()

    response = await call_next(request)

    process_time = (time.time() - start_time) * 1000
    formatted_process_time = "{0:.2f}".format(process_time)
    logger.info(f"rid={idem} completed_in={formatted_process_time}ms status_code={response.status_code}")

    return response


def has_access(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, config.username)
    correct_password = secrets.compare_digest(credentials.password, config.password)
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username


@retry(Exception, 3, 2)
def get_file_from_url(url: str) -> BytesIO:
    request = requests.get(url, stream=True)
    request.raw.decode_content = True  # Content-Encoding
    return BytesIO(request.raw.data)


@app.post("/mergedocument/word")
def create_merged_word_document(files_info: List[InputModel]):
    mwd_instance = MergeWordDocuments(logger=logger)
    sorted_files_info = sorted(files_info, key=lambda file_info: file_info.index)
    for file_info in sorted_files_info:
        try:
            mwd_instance.add_document(file_info.file_path)
        except Exception:
            raise HTTPException(status_code=404, detail=f"File not found {file_info.file_path}")

    buffer = BytesIO()
    composer = mwd_instance.merge_documents()
    buffer = WordDocumentPersistance("").save_documents_as_bytes(composer, buffer)
    file_name = str(uuid.uuid4()) + ".docx"
    return Response(
        content=buffer.getvalue(),
        media_type="application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        headers={"Content-Disposition": f"attachment;filename={file_name}"},
    )


@app.post("/mergedocument/word_from_urls")
def create_merged_word_document_from_urls(files_info: List[InputModelUrl]):
    mwd_instance = MergeWordDocuments(logger=logger)
    sorted_files_info = sorted(files_info, key=lambda file_info: file_info.index)
    for file_info in sorted_files_info:
        try:
            raw_file = get_file_from_url(file_info.url)
            mwd_instance.add_document_from_file_like_object(raw_file)
        except Exception:
            raise HTTPException(status_code=404, detail=f"File not found {file_info.url}")

    buffer = BytesIO()
    composer = mwd_instance.merge_documents()
    buffer = WordDocumentPersistance("").save_documents_as_bytes(composer, buffer)
    file_name = str(uuid.uuid4()) + ".docx"
    return Response(
        content=buffer.getvalue(),
        media_type="application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        headers={"Content-Disposition": f"attachment;filename={file_name}"},
    )
