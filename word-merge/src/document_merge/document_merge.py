import abc
from io import BytesIO
from logging import Logger
from typing import List

from docx import Document

from .composer import CustomComposer


class MergeDocuments(abc.ABC):
    @abc.abstractclassmethod
    def merge_documents(self):
        pass

    def add_document(self, document):
        pass

    def delete_document(self, document):
        pass


class MergeWordDocuments(
    MergeDocuments,
):
    def __init__(self, logger: Logger):
        self._documents = []
        self.logger = logger

    def merge_documents(self) -> CustomComposer:
        result = self._documents[0]
        result.add_page_break()
        composer = CustomComposer(result)
        last_document = self._documents[-1]
        for document in self._documents[1:]:
            if last_document != document:
                document.add_page_break()

            composer.append(document)

        return composer

    def add_document(self, document_path: str):
        self._documents.append(Document(document_path))

    def add_document_from_file_like_object(self, file_like_obj):
        self._documents.append(Document(file_like_obj))

    def delete_document(self, index: int):
        try:
            del self._documents[index]
        except Exception as e:
            self.logger.exception(f"Cannot delete document from the list. Exception occure {str(e)}")

    @property
    def get_documents(self) -> List[Document]:
        return self._documents


class DocumentPersistance(abc.ABC):
    @abc.abstractclassmethod
    def save_documents(self, documents):
        pass


class WordDocumentPersistance(DocumentPersistance):
    def __init__(self, file_path: str):
        self.file_path = file_path

    def save_documents(self, composer: CustomComposer):
        composer.save(self.file_path)

    def save_documents_as_bytes(self, composer, buffer: BytesIO):
        composer.save(buffer)
        return buffer
