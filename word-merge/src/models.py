from pydantic import BaseModel


class InputModel(BaseModel):
    index: int
    file_path: str


class InputModelUrl(BaseModel):
    index: int
    url: str
