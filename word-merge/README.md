<html>
<head><link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Pacifico&family=Righteous&display=swap" rel="stylesheet">
<!-- <style type="text/css">html, body{font-family:'Righteous', cursive;}</style> -->
</head>

# Word DOCX Merge General

This project is a single endpoint API which you can feed file paths and indexes and it will give merge of word files.
It can be easily converted you just upload multiple files. I am not fan of creating single endpoint for doing multiple stuffs.
In my opinion, there should be at least two endpoints. First one is posting files. Second one get end point retrieving merged files.

**<font color='#EB421E' style="font-family: 'Righteous', cursive;">Developer:</font>** <font style="font-family: 'Righteous', cursive;">Yunus Kursav</font>\
**<font color='#EB421E' style="font-family: 'Righteous', cursive;">E-mail:</font>** yunus.kursav@gmail.com \
**<font color='#EB421E' style="font-family: 'Righteous', cursive;">Upwork Profile:</font>** https://www.upwork.com/freelancers/~0138fca37125b83412

# Installation

(Recommended python version 3.10.1)

1. Next step is installing dependencies.

```bash
pip install -r requirements.txt
```

2. If you want to run tests please install below requirements as well.

```bash
pip install -r requirements-dev.txt
```

# How to run the code in local machine

1. You can just run below command in your terminal

```bash
uvicorn src.main:app
```

2. Navigate to link below - http://localhost:8000/

3. You should see the page already.
</html>
