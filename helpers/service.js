const AWS = require('aws-sdk');
const axios = require('axios');
const DocxMerger = require('docx-merger');
// const builder = require('docx-builder');
const fs = require('fs');
const PDFMerger = require('pdf-merger-js');

AWS.config.update({ region: 'ap-northeast-2' });

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ID,
  secretAccessKey: process.env.AWS_SECRET,
});

module.exports.uploadImageToS3 = async (req, res) => {
  const { data, mimetype, name, size } = req.files.file;
  const fileContent = Buffer.from(data, 'binary');
  const fileType = mimetype;
  const ext = name.split('.');
  const type = ext[ext.length - 1];
  if (fileType.includes('image')) {
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: `/${Date.now()}_${size}.${type}`,
      Body: fileContent,
      ContentType: mimetype,
    };
    s3.upload(params, async (err, data) => {
      if (err) {
        return res.status(400).send({ status: 'Failed', message: 'Something Went Wrong', err });
      }
      return res.status(200).send({ status: 'success', message: data.Location });
    });
  } else {
    return res.status(415).send({ status: 'Failed', message: 'Images Only' });
  }
};

const downloadObjectFromS3 = (key) =>
  s3
    .getObject({ Bucket: process.env.AWS_BUCKET, Key: key })
    .promise()
    .then(async (data) => {
      const path = `/tmp/${data.ETag}`;
      await fs.promises.writeFile(path, data.Body);
      return path;
    })
    .catch((error) => error);

module.exports.download = async (req, res) => {
  try {
    const { files } = req.body;
    // const fileKeys = files.map((file) => file.substring(file.lastIndexOf('/')));
    // const fileContents = await Promise.all(fileKeys.map((key) => downloadObjectFromS3(key)));
    const dataArr = files.map((path, index) => ({ index, url: path }));
    console.log(dataArr);
    const result = await axios({
      url: `${process.env.FILE_MERGE_API}/word_from_urls`,
      method: 'POST',
      responseType: 'arraybuffer',
      data: dataArr,
    });
    return res.send(result.data);
  } catch (error) {
    console.error(error);
    return res.status(400).send({ status: 'Failed', message: 'Something Went Wrong', error });
  }
};

module.exports.downloadPdf = async (req, res) => {
  try {
    const { files } = req.body;
    const fileKeys = files.map((file) => file.substring(file.lastIndexOf('/')));
    const fileContents = await Promise.all(fileKeys.map((key) => downloadObjectFromS3(key)));

    const merger = new PDFMerger();
    fileContents.forEach((path) => {
      merger.add(path);
    });

    const mergedFilePath = `/tmp/${Date.now()}_merged.pdf`;
    await merger.save(mergedFilePath);
    return res.sendFile(mergedFilePath);
  } catch (error) {
    console.error(error);
    return res.status(400).send({ status: 'Failed', message: 'Something Went Wrong', error });
  }
};

module.exports.uploadFilesToS3 = async (req, res) => {
  if (!req.files) {
    return res.status(400).send({ status: 'Failed', message: 'File is empty' });
  }

  try {
    const promises = [];
    Object.keys(req.files).forEach((index) => {
      const { data, mimetype, name, size } = req.files[index];

      const fileContent = Buffer.from(data, 'binary');
      const ext = name.split('.');
      const type = ext[ext.length - 1];
      const params = {
        Bucket: process.env.AWS_BUCKET,
        Key: `/${Date.now()}_${size}.${type}`,
        Body: fileContent,
        ContentType: mimetype,
      };
      const s3UploadPromise = () =>
        new Promise((resolve, reject) => {
          s3.upload(params, (err, data) => {
            if (err) {
              reject(err);
            } else {
              resolve({ fileName: name, fileLocation: data.Location });
            }
          });
        });

      promises.push(s3UploadPromise(params));
    });

    const results = await Promise.all(promises);
    return res.status(200).send({ status: 'success', results });
  } catch (error) {
    return res.status(400).send({ status: 'Failed', message: 'Something Went Wrong', error });
  }
};
