module.exports.organizationData = data => {
  if (data.length > 0) {
    let organization = null;
    let organizationId = null;
    const userList = [];
    const finalObject = {};

    for (let i = 0; i < data.length; i++) {
      if (data[i].childOf == null) {
        organization = data[i].organization;
        organizationId = data[i].organizationId;
      }

      const user = {};

      user.rowId = data[i].organizationId;
      user.role = data[i].role;
      user.requestAccepted = data[i].requestAccepted;
      user.childOf = data[i].childOf;
      user.uid = data[i].user.uid;
      user.first_name = data[i].user.first_name;
      user.last_name = data[i].user.last_name;
      user.full_name = data[i].user.full_name;
      user.username = data[i].user.username;
      user.email = data[i].user.email;
      user.phone = data[i].user.phone;
      user.profileImage = data[i].user.profileImage;
      userList.push(user);
    }
    finalObject.organization = organization;
    finalObject.organizationId = organizationId;
    finalObject.users = userList;

    return finalObject;
  }
  return {};
};

module.exports.organizationRepositoryData = data => {
  // Return data;
  const finalObject = [];
  if (data.length > 0) {
    for (let k = 0; k < data.length; k++) {
      const { repositoryId } = data[k];
      const repository = data[k].repositoryName;
      const { repositoryPhoto } = data[k];
      const { organizationId } = data[k];
      const users = [];
      const tempObject = {};
      tempObject.repositoryId = repositoryId;
      tempObject.repository = repository;
      tempObject.repositoryPhoto = repositoryPhoto;
      tempObject.organizationId = organizationId;

      if (data[k].repositoryusers.length > 0) {
        for (let i = 0; i < data[k].repositoryusers.length; i++) {
          const user = {};
          user.rowId = data[k].repositoryusers[i].id;
          user.uid = data[k].repositoryusers[i].uid;
          user.first_name = data[k].repositoryusers[i].user.first_name;
          user.last_name = data[k].repositoryusers[i].user.last_name;
          user.full_name = data[k].repositoryusers[i].user.full_name;
          user.username = data[k].repositoryusers[i].user.username;
          user.profileImage = data[k].repositoryusers[i].user.profileImage;
          users.push(user);
        }
      }
      tempObject.users = users;
      finalObject.push(tempObject);
    }
    return finalObject;
  }
  return {};
};
