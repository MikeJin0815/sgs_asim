module.exports.Registration = () => ({
  type: 'Registration',
  subject: 'Welcome to LuxERP',
  body: `
            <h3 class="emailHeader">Greeting {{FIRSTNAME}} {{LASTNAME}},</h3>
            <p class="paragraph">
            Thank you for your registration.
            </p>
            <p class="paragraph">
            Your username is - {{USERNAME}}<br>
            Click the link below : <a href="{{URL}}">Click here to login</a>
            </p>
        `,
});

module.exports.UserInvitation = () => ({
  type: 'UserInvitation',
  subject: 'Invitation To Join',
  body: `
            <h3 class="emailHeader">Hello {{FIRSTNAME}} {{LASTNAME}},</h3>

                 <p class="paragraph"> You have been invited from LuxPM ERP.
                 <br/>Click the link below : <a href="{{HOME_URL}}">Click here to join</a>
                 </p>
        `,
});

module.exports.userAcceptanceMail = () => ({
  type: 'userAcceptanceMail',
  subject: 'User Acceptance Confirmation',
  body: `
           <h3 class="emailHeader">Hello {{FIRSTNAME}} {{LASTNAME}},</h3>
           <p class="paragraph"> Your account has been verified. <br>
            Click the link below : <a href={{URL}}>Click here to login</a><br>
            </p>
        `,
});

module.exports.SocialLogin = () => ({
  type: 'SocialLogin',
  subject: 'Registration',
  body: `
            <h3 class="emailHeader">Hello {{FIRSTNAME}} {{LASTNAME}},</h3>
            <p class="paragraph">
            Your Username: {{USERNAME}}<br>
            Your registered Email : <b> {{EMAIL}} </b><br>
            Your platform : {{PLATFORM}} </b><br>
             Click the link below : <a href="{{URL}}">Click here to login</a>
            </p>
        `,
});

module.exports.ForgetUserName = () => ({
  type: 'ForgetUserName',
  subject: 'Forget User Name Helper!',
  body: `
            <h3 class="emailHeader">Hello {{FIRSTNAME}} {{LASTNAME}},</h3>
            <p class="paragraph"> We have received your Forget Username Request! We have found your email address in our system. <br>
            This is your Username : <b> {{USERNAME}} </b> <br>
            Click link url : <a href="{{URL}}">Click here to login</a> <br>
            </p>
        `,
});

module.exports.ForgetPassword = () => ({
  type: 'ForgetPassword',
  subject: 'Forget Password',
  body: `
            <h3 class="emailHeader">Hello {{FIRSTNAME}} {{LASTNAME}},</h3>
            <p class="paragraph">We have received your Forget password Request! We have found your email address in our system.<br>
            Click the link below :  <a href="{{EMAIL_URL}}"> Click here to reset your password</a> </p>
        `,
});
