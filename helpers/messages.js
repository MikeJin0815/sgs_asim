module.exports.defaultServerResponse = {
  status: 400,
  message: '',
  body: {},
};

module.exports.apiResponse = {
  NEED_AUTHENTICATION: 'Need authentication to use this API',
};

module.exports.user = {
  DUPLICATE_EMAIL: 'User already exist with given email',
  USER_ALREADY_EXISTS: 'User already exist with given email',
  INVALIDE_VERIFICATION_CODE: 'Provided code in invalide',
  SIGNUP_SUCCESS: 'Signup Success',
  SIGNIN_SUCCESS: 'Signin Success',
  USER_NOT_FOUND: 'User not found',
  INVALID_PASSWORD: 'Invalid password',
  DELETE_SUCCESS: 'User Deleted Successfully',
  PROFILE_UPDATE_SUCCESS: 'Profile Updated Successfully',
  FORGET_REQUEST_SUCCESS: 'Forget Request Successfully',
  USER_NOT_FOUND: 'There is no user with the provided credentials',
  INVALID_PASSWORD: 'The provided username or password are incorrect',
  AUTH_SUCCES: 'User authenticated successfully',
  AUTH_FAILED: 'User authentication failed',
  LOGIN_SUCCESS: 'Logged in successfully',
  LOGED_OUT_SUCCESSFULLY: 'Logged out successfully',
  EMAIL_CODE_SUCCESS: 'Code sent successfully',
  UPDATE_SUCCESS: 'user information updated successfully',
  DELETE_SUCCESS: 'user deleted successfully',
  EMAIL_UPDATE_SUCCESS: 'user e-mail updated successfully',
  PASSWORD_UPDATED_SUCCESS: 'user password updated successfully',
  PASSWORD_NOT_MATCHING: 'The provided passwords do not match',
  INVALID_VERIFICATION_CODE: 'The provided verification code is invalid',
  VERIFICATION_CODE_VERIFIED: 'Verification Code Verified',
  TOKEN_NOT_VERIFIED: 'Token is not verified for the user',
  SEND_PHONE_OTP_SUCCESS: 'Otp sent successfully',
  PHONE_OTP_VERIFIED: 'Otp Verified',
  PHONE_NOT_FOUND: 'There is no user with this phone number',
  INVALID_OTP_CODE: 'Provided otp is invalid',
  EXPIRED_OTP_CODE: 'Provided otp is expired, Please send the otp again!',
  MATCHED_EMAIL: 'Username will be sent if the email is matched!',
};
