const { Op, where } = require('sequelize');
const { models } = require('../database/index');

/* Check creator is admin */
module.exports.checkIsAdmin = async (uid, organizationId) => {
  try {
    const isAdmin = await models.organizationuser.findOne({
      where: {
        uid,
        organizationId,
      },
    });

    if (isAdmin && isAdmin.role === 'admin') {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

/* Check repository name is available */
module.exports.isRepositoryNameAvailable = async (
  organizationId,
  repositoryName
) => {
  try {
    const notAvilable = await models.repository.findOne({
      where: { organizationId, repositoryName },
    });
    if (notAvilable == null) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

/* Check repository name is available */
module.exports.isRepositoryAvailable = async repositoryId => {
  try {
    const isAvilable = await models.repository.findOne({
      where: { repositoryId },
    });
    if (isAvilable) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

/* Check user is avalable is oaganization */
module.exports.isUserAvailableInOrganization = async (
  userId,
  organizationId
) => {
  try {
    const isAvailable = await models.organizationuser.findOne({
      where: { organizationUserId: userId, organizationId },
    });

    return Boolean(isAvailable);
  } catch (error) {
    return false;
  }
};

/* Check user is avalable is oaganization */
module.exports.isUserAlreadyInRepository = async (userId, repositoryId) => {
  try {
    const isAlreadyInRepository = await models.repositoryuser.findOne({
      where: { repositoryId, organizationUserId: userId },
    });

    return Boolean(isAlreadyInRepository);
  } catch (error) {
    return false;
  }
};

/* Check workspace is available */
module.exports.isWorkspaceNotAvailable = async (
  repositoryId,
  workspaceName
) => {
  try {
    const notAddedYet = await models.repositoryworkspace.findOne({
      where: { repositoryId, workspaceName },
    });
    if (notAddedYet == null) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

/* Check Workspace is available */
module.exports.isWorkSpaceAvlble = async workspaceId => {
  try {
    const isAvailable = await models.repositoryworkspace.findOne({
      where: { workspaceId },
    });
    if (isAvailable) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

/* Check Workspace Folder Name is available or not */
module.exports.isWorkspaceFolderAvlble = async (
  workspaceId,
  workspaceFolderName
) => {
  try {
    const isAvailable = await models.repositoryworkspacefolder.findOne({
      where: { workspaceId, workspaceFolderName },
    });
    if (isAvailable == null) {
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

module.exports.validationCheck = async (recordId, colId) => {
  const isAvailable = await models.filegrouprecord.findOne({
    where: { recordId },
  });
  if (!isAvailable) {
    return false;
  }
  const { fileGroupId } = isAvailable;
  const checkIfColAvailable = await models.filegroupcolumn.findOne({
    where: { fileGroupId, colId },
  });
  if (checkIfColAvailable) {
    return true;
  }
  return false;
};
