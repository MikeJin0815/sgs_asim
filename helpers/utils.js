const jwt = require('jsonwebtoken');
const mailer = require('nodemailer');
const handlebars = require('handlebars');
const axios = require('axios');
const messages = require('./messages');
const { models } = require('../database/index');

const { errorLog } = require('./loggers');
const emailConstant = require('./emailConstants');

module.exports = async function controllerBuilder({
  controllerName,
  serviceCall,
  serviceData,
  succesMsg,
}) {
  const response = { ...messages.defaultServerResponse };
  try {
    const responseFromService = await serviceCall(serviceData);
    response.status = 200;
    response.message = succesMsg;
    response.body = responseFromService;
  } catch (error) {
    errorLog(
      'ERROR:',
      `Something went wrong: Controller: ${controllerName}\n${error.message}`
    );
    response.message = error.message;
  }

  return response;
};

module.exports.generateToken = dataObj => {
  const payload = { user: dataObj.email };
  return jwt.sign(payload, process.env.JWT_SECRET);
};

module.exports.validateEmail = email => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

module.exports.validateToken = (req, res, next) => {
  const authorizationHeaader = req.headers.authorization;
  let result;
  if (authorizationHeaader) {
    const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
    const options = {
      expiresIn: '2d',
    };
    try {
      // Verify makes sure that the token hasn't expired and has been issued by us
      result = jwt.verify(token, process.env.JWT_SECRET, options);

      // Let's pass back the decoded token to the request object
      req.decoded = result;
      // We call next to pass execution to the subsequent middleware
      next();
    } catch (err) {
      /*
       *  Throw an error just in case anything goes wrong with verification
       * throw new Error(err);
       */
      res.status(401).send(err);
    }
  } else {
    result = {
      error: 'Authentication error. Token required.',
      status: 401,
    };
    res.status(401).send(result);
  }
};

module.exports.decodeToken = async token => {
  const decoded = jwt.verify(
    token,
    process.env.SECRET_KEY || 'tokenSigningSecretKey'
  );
  return decoded.id;
};

module.exports.commonEmailSend = async (user, emailTemplate) => {
  try {
    if (user && emailTemplate) {
      const { type } = emailTemplate;
      const { subject } = emailTemplate;
      const tempBody = emailTemplate.body;
      let value = emailConstant.HTML +
        emailConstant.CSS +
        emailConstant.HEADER +
        tempBody +
        emailConstant.FOOTER;
      const templateData = handlebars.compile(value);
      let replacements = {};
      if (type === 'Registration') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name ? user.last_name : '',
          USERNAME: user.username,
          URL: process.env.CLIENT_URL,
        };
      } else if (type === 'UserInvitation') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name,
          HOME_URL: user.URL,
        };
      } else if (type === 'SocialLogin') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name,
          PLATFORM: user.platform,
          EMAIL: user.email,
          URL: process.env.CLIENT_URL,
        };
      } else if (type === 'userAcceptanceMail') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name,
          EMAIL: user.email,
          URL: process.env.CLIENT_URL,
        };
      } else if (type === 'ForgetUserName') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name,
          USERNAME: user.username,
          URL: process.env.CLIENT_URL,
        };
      } else if (type === 'ForgetPassword') {
        replacements = {
          FIRSTNAME: user.first_name,
          LASTNAME: user.last_name,
          EMAIL_URL: user.emailURL,
        };
      }

      value = templateData(replacements);
      const body = {
        from: process.env.EMAIL_USER,
        to: user.email,
        subject,
        html: value,
      };
      console.log("send")
      const transporter = mailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASS,
        },
      });

      console.log("value body")
      console.log(body)
      await transporter.sendMail(body);
      console.log("sent")

    } else {
      throw new Error('Unable to execute send email function..');
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports.sendPhoneOtpSMS = async (typeMessage, code, receiverArr) => {
  const data = {
    title: 'Otp Code',
    message: `${code} is the one time password from LuxPM ERP System : ${typeMessage}`,
    sender: process.env.DIRECT_SMS_SENDER_PHONE,
    username: process.env.DIRECT_SMS_USERNAME,
    key: process.env.DIRECT_SMS_KEY,
    receiver: receiverArr,
  };
  const url = process.env.DIRECT_SEND_SMS_API_URL;
  return new Promise((resolve, reject) => {
    axios
      .post(url, data)
      .then(result => {
        console.log(result.data);
        resolve(result.data);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
};

module.exports.formatDateTime = date => [
  `${date.getFullYear()}-${date.getMonth() + 1 < 10 ? '0' : ''}${date.getMonth() + 1
  }-${date.getDate() < 10 ? '0' : ''}${date.getDate()}`,

  `${(date.getHours() < 10 ? '0' : '') + date.getHours()}:${date.getMinutes() < 10 ? '0' : ''
  }${date.getMinutes()}`,
];

module.exports.addColumnIfNotExist = (
  queryInterface,
  tableName,
  columnName,
  type
) =>
  queryInterface.describeTable(tableName).then(tableDefinition => {
    if (!tableDefinition[columnName]) {
      return queryInterface.addColumn(tableName, columnName, type);
    }

    return Promise.resolve(true);
  });

module.exports.removeColumnIfExist = (queryInterface, tableName, columnName) =>
  queryInterface.describeTable(tableName).then(tableDefinition => {
    if (tableDefinition[columnName]) {
      return queryInterface.removeColumn(tableName, columnName);
    }

    return Promise.resolve(true);
  });
