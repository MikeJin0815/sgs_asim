module.exports.HTML = `<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
`;

module.exports.CSS = `<style type="text/css">

        .container {
            position: relative;
            max-width: 590px;
            background-color: whitesmoke;
        }
        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0,0,0,.125);
            border-radius: .25rem;
            width: 85%;
            /*margin-top: -10%;*/
            /*margin-left: 0%;*/
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        .paragraph {
            padding-top: 1rem;
            font-size: large;
            text-align: left;
            font-weight: 400;
        }

        .paragraph2rem {
            padding-top: 2rem;
        }

        .emailHeader {
            text-align: left;
            color: #0A071B;
        }

        .containerCustom {
            padding-top: 2rem;
            height: 30px;
            width: 590px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-weight: 300;
        }

        .leftbox {
            float:left;
            width: 468px;
            height: inherit;
            background-color:white;
            border:solid black;
        }

        .rightbox{
            float:right;
            width: 25px;
            height: inherit;
            background-color:white;
            border:solid black;
        }

        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        img {
            width: 100%;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }
        span.preheader {
            display: none;
            font-size: 1px;
        }
        html {
            width: 100%;
        }
        table {
            font-size: 14px;
            border: 0;
        }
        .paragraph {
            font-size: medium;
            line-height: normal;
            letter-spacing: 0px
        }
        .emailHeader {
            text-align: left;
            letter-spacing: 0px;
            color: #0A071B;
        }
        .headingEmail {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: x-large;
            font-weight: 700;
            padding: 1rem;
        }

        .innerContaint {
            padding: 1rem;
            font-size: medium;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        }

        .placeInCenter {
            text-align: -webkit-center;
        }

        .placeInStart {
            text-align: start;
        }

        .bg-white {
            background: #ffffff !important;

        }
        @media only screen and (max-width: 640px) {
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            .divider img {
                width: 440px !important;
            }
            .container590 {
                width: 440px !important;
            }
            .container580 {
                width: 400px !important;
            }
            .main-button {
                width: 220px !important;
            }
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }
            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }
        @media only screen and (max-width: 479px) {
            .divider img {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container590 {
                width: 280px !important;
            }
            .container580 {
                width: 260px !important;
            }
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }
    </style>

</head>`;

module.exports.HEADER = `<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">
    <tbody>
    <tr>
        <td align="center">
            <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590"
                   style="background:#f7f8fc">
                <tbody class="placeInCenter">
                <tr>
                    <div class="container" >
                        <!-- img src="https://erpdevbucket.s3.us-east-2.amazonaws.com//1635586845565_110189.jpeg" -->
                        <div class="paragraph2rem"></div>
                        <div class="content">
                            <div class="card">
                                <div class="container bg-white">
                                    <div style="border-radius:4px">
                                        <div>
                                        <div class="headingEmail" style="text-align:left;letter-spacing:0px;color:#003cf5">
                                        Welcome to ${process.env.EMAILIDENTIFIER}
                                        </div>
                                            <div class="innerContaint">`;

module.exports.FOOTER = `</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </tr>
                <tr>
                    <td class="innerContaint" >
                    </td>
                </tr>
                <tr>
                    <td class="innerContaint placeInStart" style="font-weight: 600;">
                        <p style="margin-left: 2rem">
                            Thank You, <br/>${process.env.EMAILIDENTIFIER} Operation Team
                        </p>
                    </td>
                </tr>
                <tr style="width: 590px;" class="placeInStart">
                    <td style="width: inherit">
                        <div class="containerCustom">
                            <div class="leftbox" style="border: 12px solid #0A193F;background: #0A193F no-repeat padding-box" >
                                <p style="font-size: 14px; color: black; ">® All Rights Reserved by ${process.env.EMAILIDENTIFIER} <br/>© 2022 ${process.env.EMAILIDENTIFIER}</p>
                            </div>
<!--
                            <div class="rightbox" style="border: 12px solid #0A193F;background: #0A193F no-repeat padding-box;" >
                                    <a href="https://www.linkedin.com/company/luxspm/">
                                        <img src="https://erpdevbucket.s3.us-east-2.amazonaws.com//1634638482925_599.png">
                                    </a>
                            </div>

                            <div class="rightbox" style="border: 12px solid #0A193F;background: #0A193F no-repeat padding-box;" >
                                <a href="https://www.facebook.com/luxpmsoft">
                                    <img src="https://erpdevbucket.s3.us-east-2.amazonaws.com//1634709427405_544.png">
                                </a>
                            </div>
-->
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    </tbody>
</table>
</body>

</html>`;
