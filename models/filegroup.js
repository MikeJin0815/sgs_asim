const { Sequelize, Model } = require('sequelize');

const FileGroup = (sequelize) => {
  class FileGroupModel extends Model {}

  FileGroupModel.init(
    {
      fileGroupId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      boardId: Sequelize.INTEGER,
      fileSystemGroupName: {
        type: Sequelize.STRING,
      },
      uid: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      colOrder: {
        type: Sequelize.JSON,
        defaultValue: [],
      },
      order: Sequelize.INTEGER,
      subGroupId: {
        type: Sequelize.INTEGER,
        unique: true,
      },
    },
    { sequelize, modelName: 'filegroup' },
  );
};

module.exports = FileGroup;
