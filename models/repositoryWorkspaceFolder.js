const { Sequelize, Model } = require('sequelize');

const repositoryWorkSpaceFolder = (sequelize) => {
  class RepositoryWorkSpaceFolder extends Model {}

  RepositoryWorkSpaceFolder.init(
    {
      workspaceFolderId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      workspaceId: {
        type: Sequelize.INTEGER,
        required: true,
      },
      workspaceFolderName: {
        type: Sequelize.STRING,
        required: true,
      },
      order: Sequelize.INTEGER,
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'repositoryworkspacefolder',
    },
  );

  RepositoryWorkSpaceFolder.afterCreate(async (folder) => {
    const homeGroup = await sequelize.models.filegroup.findOne({
      where: { workspaceId: folder.workspaceId },
    });
    await sequelize.models.filegrouprecord.create({
      fileGroupId: homeGroup.fileGroupId,
      title: folder.workspaceFolderName,
      type: 'folder',
      linkId: folder.workspaceFolderId,
    });
    await folder.createFilegroup({
      fileSystemGroupName: folder.workspaceFolderName,
    });
  });
};

module.exports = repositoryWorkSpaceFolder;
