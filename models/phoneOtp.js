const { Sequelize, Model } = require('sequelize');

const PhoneOtp = (sequelize) => {
  /* Extending the PhoneOtp model from the sequilize Model class */

  class phoneOtpModel extends Model {}

  /* Initiating the PhoneOtp model schema */

  phoneOtpModel.init(
    {
      phone: Sequelize.STRING,
      otp: Sequelize.INTEGER,
      expiresAt: Sequelize.DATE,
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    },
    { sequelize, modelName: 'phoneOtp', timestamps: true },
  );
};

module.exports = PhoneOtp;
