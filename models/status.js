const { Sequelize, Model } = require('sequelize');

const Status = (sequelize) => {
  class StatusModel extends Model {}

  StatusModel.init(
    {
      statusId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      colId: {
        type: Sequelize.INTEGER,
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      color: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    { sequelize, modelName: 'status' },
  );
};

module.exports = Status;
