const { Sequelize, Model } = require('sequelize');

const ApplicationForm = (sequelize) => {
  class ApplicationFormModel extends Model {}

  ApplicationFormModel.init(
    {
      applicationFormId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      boardId:{
        type: Sequelize.INTEGER,
        allowNull: false,
      },      
      formContent:{
        type: Sequelize.JSON,
        allowNull: true,
      },
      isActive:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      publictoken:{
        type: Sequelize.TEXT,
        allowNull:true,
        defaultValue:''
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    { sequelize, modelName: 'applicationform' },
  );
};

module.exports = ApplicationForm;
