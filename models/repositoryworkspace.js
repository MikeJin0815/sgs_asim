const { Sequelize, Model } = require('sequelize');

const repositoryWorkSpace = (sequelize) => {
  class RepositoryWorkSpace extends Model {}

  RepositoryWorkSpace.init(
    {
      workspaceId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      repositoryId: {
        type: Sequelize.INTEGER,
        required: true,
      },
      workspaceName: {
        type: Sequelize.STRING,
        required: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'repositoryworkspace',
    },
  );

  RepositoryWorkSpace.afterCreate(async (workspace) => {
    await workspace.createFilegroup({ fileSystemGroupName: 'Home' });
  });
};

module.exports = repositoryWorkSpace;
