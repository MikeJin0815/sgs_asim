const { Sequelize, Model } = require('sequelize');

const OrganizationUser = (sequelize) => {
  class organizationUserModel extends Model {}

  organizationUserModel.init(
    {
      organizationUserId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      role: Sequelize.ENUM('admin', 'user'),
      requestAccepted: Sequelize.ENUM('true', 'false', 'deactivate', 'requested'),
      department: {
        type: Sequelize.STRING,
        defaultValue: '',
      },
      approvedBy: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
    },
    { sequelize, modelName: 'organizationuser', timestamps: true }
  );
};

module.exports = OrganizationUser;
