const { Sequelize, Model } = require('sequelize');

const repositoryUser = (sequelize) => {
  class RepositoryUser extends Model {}

  RepositoryUser.init(
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      organizationUserId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      repositoryId: {
        type: Sequelize.INTEGER,
        required: true,
      },
      assignedBy: {
        type: Sequelize.INTEGER,
        required: true,
      },
      accessLevel: {
        type: Sequelize.INTEGER,
        validate: { min: 1, max: 2 },
        defaultValue: 1, // 1 = READ, WRITE, DELETE - 2 = READ ONLY
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'repositoryuser',
    }
  );
};

module.exports = repositoryUser;
