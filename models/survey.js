const { Sequelize, Model } = require('sequelize');

const survey = (sequelize) => {
  class surveyModel extends Model {}

  surveyModel.init(
    {
      surveyId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      writer: {
        type: Sequelize.STRING,
      },
      rType: {
        type: Sequelize.STRING,
      },
      start_date: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      end_date: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'survey',
      timestamps: true,
    }
  );

};

module.exports = survey