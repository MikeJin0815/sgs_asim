const { Sequelize, Model } = require('sequelize');

const FileGroupRecord = (sequelize) => {
  class FileGroupRecordModel extends Model {}

  FileGroupRecordModel.init(
    {
      recordId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      fileGroupId: Sequelize.INTEGER,
      title: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.ENUM('folder', 'board', 'record'),
        defaultValue: 'record',
      },
      linkId: Sequelize.INTEGER,
      order: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
      },
    },
    { sequelize, modelName: 'filegrouprecord' }
  );

  FileGroupRecordModel.afterCreate(async (record) => {
    /* Get all the column from the group to create an empty fileGroupRecord */
    const columnsFromGroup = await sequelize.models.filegroupcolumn.findAll({
      where: {
        fileGroupId: record.fileGroupId,
      },
    });

    /*
     * Create array of promises to create all the records simultaneously for better optimization
     * but still wait for them to be all created before sending success request to avoid possible
     * problem front-side if the user try to update a recordFile before it has been created
     */
    const promises = [];
    columnsFromGroup.forEach((column) => {
      promises.push(
        sequelize.models.filegrouprecordfile.create({
          recordId: record.recordId,
          colId: column.colId,
        })
      );
    });
    await Promise.all(promises);
  });
};

module.exports = FileGroupRecord;
