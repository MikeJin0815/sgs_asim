const { Sequelize, Model } = require('sequelize');

const Subgroup = sequelize => {
  class SubgroupModel extends Model {}

  SubgroupModel.init(
    {
      subgroupId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'subgroup',
      timestamps: true,
      defaultScope: {
        include: [{ model: sequelize.models.filegroup, as: 'filegroup' }],
      },
    },
  );
};

module.exports = Subgroup;
