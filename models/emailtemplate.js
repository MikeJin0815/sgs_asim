const { Sequelize, Model } = require('sequelize');
const emailConstant = require('../helpers/emailConstants');
const { errorLog } = require('../helpers/loggers');
const {
  SocialLogin,
  Registration,
  userAcceptanceMail,
  UserInvitation,
  ForgetUserName,
  ForgetPassword,
} = require('../helpers/static/emailtemplate');

const { Op } = Sequelize;
const template = (sequelize) => {
  class emailTemplate extends Model {
    static async createRegistrationEmail() {
      /* For registration template */
      try {
        var type = await Registration().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });
        if (!checkTemplate) {
          const emailBody = await Registration().body;
          const subject = await Registration().subject;
          await this.create({ type, subject, body: emailBody });
          console.log('Registration template created!');
        }
        console.log('Registration template is already created!');
      } catch (error) {
        console.log('failed to create Registration template!');
      }

      /* User invitation template */
      try {
        var type = await UserInvitation().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });

        if (!checkTemplate) {
          const emailBody = await UserInvitation().body;
          const subject = await UserInvitation().subject;
          await this.create({ type, subject, body: emailBody });
          console.log('User invitation template template created!');
        }
        console.log('User invitation template template is already created!');
      } catch (error) {
        console.log('failed to create User invitation template template!');
      }

      /* User acceptance template */
      try {
        var type = await userAcceptanceMail().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });

        if (!checkTemplate) {
          const emailBody = await userAcceptanceMail().body;
          const subject = await userAcceptanceMail().subject;
          await this.create({
            type,
            subject,
            body: emailBody,
          });
          console.log('User acceptance template created!');
        }
        console.log('User acceptance template is already created!');
      } catch (error) {
        console.log('failed to create template!');
      }

      /* Social registration template */
      try {
        var type = await SocialLogin().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });

        if (!checkTemplate) {
          const emailBody = await SocialLogin().body;
          const subject = await SocialLogin().subject;
          await this.create({
            type,
            subject,
            body: emailBody,
          });
          console.log('Social registration template created!');
        }
        console.log('Social registration template is already created!');
      } catch (error) {
        console.log('failed to create Social registration template!');
      }

      /* Forget User Name template */
      try {
        var type = await ForgetUserName().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });

        if (!checkTemplate) {
          const emailBody = await ForgetUserName().body;
          const subject = await ForgetUserName().subject;
          await this.create({
            type,
            subject,
            body: emailBody,
          });
          console.log('ForgetUserName  template created!');
        }
        console.log('ForgetUserName template is already created!');
      } catch (error) {
        console.log('failed to create ForgetUserName template!');
      }

      /* Forget Password template */
      try {
        var type = await ForgetPassword().type;
        const checkTemplate = await this.findOne({
          where: {
            type: {
              [Op.like]: type,
            },
          },
        });

        if (!checkTemplate) {
          const emailBody = await ForgetPassword().body;
          const subject = await ForgetPassword().subject;
          await this.create({
            type,
            subject,
            body: emailBody,
          });
          console.log('Forget Password  template created!');
        }
        console.log('Forget Password template is already created!');
      } catch (error) {
        console.log('failed to create Forget Password template!');
      }
    }
  }

  emailTemplate.init(
    {
      templateId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      body: {
        type: Sequelize.TEXT('long'),
      },
      uid: {
        type: Sequelize.INTEGER,
      },
      subject: {
        type: Sequelize.STRING,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'emailtemplate',
    },
  );
};

module.exports = template;
