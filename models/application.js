const { Sequelize, Model } = require('sequelize');

const application = (sequelize) => {
  class applicationModel extends Model {}

  applicationModel.init(
    {
      applicationId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      company: {
        type: Sequelize.STRING,
      },
      manager: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      address: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      managementNum: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      express: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      fileName: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      fileLocation: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      size: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      sizeUnit: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      count: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      density: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      date: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      range: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      sample: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      extra: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      agree: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'application',
      timestamps: true,
    }
  );

};

module.exports = application