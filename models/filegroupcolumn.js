const { Sequelize, Model } = require('sequelize');

const FileGroupColumn = (sequelize) => {
  class FileGroupColumnModel extends Model {}

  FileGroupColumnModel.init(
    {
      colId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      fileGroupId: Sequelize.INTEGER,
      headerName: {
        type: Sequelize.STRING,
      },
      headerType: {
        type: Sequelize.ENUM('status', 'file', 'text', 'people', 'date', 'daterange'),
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      rootreference: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      width: {
        type: Sequelize.INTEGER,
        defaultValue: 150,
      },
      colorder: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
      },
    },
    { sequelize, modelName: 'filegroupcolumn' }
  );

  FileGroupColumnModel.afterCreate(async (column) => {
    if (column.headerType === 'status') {
      const statusArr = [
        { title: 'Working on it', color: 'bg-orange-0' },
        { title: 'Stuck', color: 'bg-red-0' },
        { title: 'Done', color: 'bg-green-0' },
        { title: '', color: 'bg-gray-6' },
      ];
      const promises = [];
      statusArr.forEach((currStatus) => {
        promises.push(
          sequelize.models.status.create({
            colId: column.colId,
            title: currStatus.title,
            color: currStatus.color,
          })
        );
      });
      await Promise.all(promises);
    }
  });

  FileGroupColumnModel.beforeDestroy(async (column) => {
    if (column.headerType === 'status') {
      await sequelize.models.status.destroy({ where: { colId: column.colId } });
    }
  });
};

module.exports = FileGroupColumn;
