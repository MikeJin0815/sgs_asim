const { Sequelize, Model } = require('sequelize');

const ApplicationFormEntry = (sequelize) => {
  class ApplicationFormEntryModel extends Model {}

  ApplicationFormEntryModel.init(
    {
      applicationFormEntryId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      applicationFormId: {
        type: Sequelize.INTEGER,
      },    
      entryData:{
        type: Sequelize.JSON,
        allowNull: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    { sequelize, modelName: 'applicationformentry' },
  );
};

module.exports = ApplicationFormEntry;
