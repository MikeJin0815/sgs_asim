const { Sequelize, Model } = require('sequelize');

const repository = (sequelize) => {
  class repositoryModel extends Model {}

  repositoryModel.init(
    {
      repositoryId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      uid: {
        type: Sequelize.INTEGER,
      },
      organizationId: {
        type: Sequelize.INTEGER,
      },
      repositoryName: {
        type: Sequelize.STRING,
      },
      repositoryPhoto: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      isPublic: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: 'repository',
      timestamps: true,
    }
  );

  repositoryModel.afterCreate(async (repo) => {
    await sequelize.models.repositoryworkspace.create({
      repositoryId: repo.repositoryId,
      uid: repo.uid,
      workspaceName: 'Main workplace',
    });
  });
};

module.exports = repository;
