const { Sequelize, Model } = require('sequelize');

const FileGroupRecordFiles = (sequelize) => {
  class FileGroupRecordfilesModel extends Model {}

  FileGroupRecordfilesModel.init(
    {
      recordFileId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      recordId: Sequelize.INTEGER,
      colId: {
        type: Sequelize.STRING,
      },
      fileName: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      fileLocation: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status_old: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      statusId: {
        type: Sequelize.INTEGER,
      },
      text: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      organizationUserId: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      people: {
        type: Sequelize.STRING /* Pass id values as comma separated example. 12,32,5 */,
        allowNull: true,
      },
      date: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      daterange_start: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: new Date(),
      },
      daterange_end: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: new Date(),
      },
      uid: {
        type: Sequelize.INTEGER,
      },
    },
    { sequelize, modelName: 'filegrouprecordfile' },
  );
};

module.exports = FileGroupRecordFiles;
