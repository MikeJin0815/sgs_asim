const { Sequelize, Model } = require('sequelize');
const bcrypt = require('bcryptjs');
const { slugify } = require('transliteration');
const messages = require('../helpers/messages');
const { generateToken } = require('../helpers/utils');
const { errorLog } = require('../helpers/loggers');

const Organization = sequelize => {
  class OrganizationModel extends Model {}

  OrganizationModel.init(
    {
      organizationId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      uid: Sequelize.INTEGER,
      name: {
        type: Sequelize.STRING,
        unique: true,
      },
      subdomain: {
        type: Sequelize.STRING,
        unique: true,
      },
      logo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    },
    { sequelize, modelName: 'organization', timestamps: true },
  );

  OrganizationModel.beforeCreate(async (organization) => {
    organization.subdomain = slugify(organization.name);
  });
};

module.exports = Organization;
