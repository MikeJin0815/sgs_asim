const { Sequelize, Model } = require('sequelize');

const identification = (sequelize) => {
  class identificationModel extends Model {}

  identificationModel.init(
    {
      identificationId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      uid: {
        type: Sequelize.INTEGER,
      },
      uName: {
        type: Sequelize.STRING,
      },
      adminId: {
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.STRING,
      }
    },
    {
      sequelize,
      modelName: 'identification',
      timestamps: true,
    }
  );

};

module.exports = identification