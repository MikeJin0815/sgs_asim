const { Sequelize, Model } = require('sequelize');

const question = (sequelize) => {
  class questionModel extends Model {}

  questionModel.init(
    {
      questionId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.STRING,
      },
    },
    {
      sequelize,
      modelName: 'question',
      timestamps: true,
    }
  );
};

module.exports = question