const { Sequelize, Model } = require('sequelize');
const bcrypt = require('bcryptjs');
const { Op } = require('sequelize');
const messages = require('../helpers/messages');
const { generateToken } = require('../helpers/utils');
const { errorLog } = require('../helpers/loggers');
const emailDesign = require('../helpers/emailConstants');

const { commonEmailSend } = require('../helpers/utils');

const User = (sequelize) => {
  /* Extending the User model from the sequilize Model class */
  class UserModel extends Model {
    /**
     * Create user
     *
     * @params {User}
     * @result {User}
     *
     */

    /* static async createSuperUser() {
      try {
        const email = 'superadmin@luxpmsoft.com';
        const checkSuper = await this.findOne({ where: { email } });
        if (!checkSuper) {
          const newPassword = await bcrypt.hash('lux@123', 12);
          await this.create({
            first_name: 'Super',
            last_name: 'Admin',
            username: 'superadmin',
            email,
            type: 'super',
            phone: '+8201037577880',
            password: newPassword,
            phone_verification_code: '1234567890',
            user_created_by: 'system',
            approvedBy: 1,
            request: 'accepted',
          });
          return console.log('Super user created!');
        }

        return console.log('Super user already available!');
      } catch (error) {
        console.log('Super user already available!');
      }
    } */

    static async createSuperUser({
      first_name,
      last_name,
      username,
      email,
      phone,
      password,
      phone_verification_code,
    }) {
      try {
        await this.checkIfExist(email);
        const newPassword = await bcrypt.hash(password, 12);
        const user = await this.create({
          first_name,
          last_name,
          username: email.split('@')[0],
          email,
          type: 'super',
          phone,
          password: newPassword,
          phone_verification_code,
          user_created_by: 'self',
        });
        return {
          status: 200,
          mesage:
            'Super user created! If your email is correct, we will send your username to your email account.',
          user: user.toJSON(),
        };
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the createUser service ${error}`,
        );
        throw new Error(error);
      }
    }
    
    static async createCompanyUser({
      first_name,
      last_name,
      username,
      email,
      phone,
      password,
      phone_verification_code,
    }) {
      try {
        await this.checkIfExist(email);
        const newPassword = await bcrypt.hash(password, 12);
        const user = await this.create({
          first_name,
          last_name,
          username: email.split('@')[0],
          email,
          type: 'user',
          phone,
          password: newPassword,
          phone_verification_code,
          user_created_by: 'self',
        });
        return {
          status: 200,
          mesage:
            'If your email is correct, we will send your username to your email account.',
          user: user.toJSON(),
        };
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the createUser service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async createUser({
      first_name,
      last_name,
      username,
      email,
      phone,
      password,
      phone_verification_code,
    }) {
      try {
        await this.checkIfExist(email);
        const newPassword = await bcrypt.hash(password, 12);
        const user = await this.create({
          first_name,
          last_name,
          username: email.split('@')[0],
          email,
          type: 'guest',
          phone,
          password: newPassword,
          phone_verification_code,
          user_created_by: 'self',
        });
        return {
          status: 200,
          mesage:
            'If your email is correct, we will send your username to your email account.',
          user: user.toJSON(),
        };
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the createUser service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async createUserByAdmin({
      first_name,
      last_name,
      email,
      user_created_by,
      email_verification_code,
    }) {
      console.log("verificationCode", email_verification_code)
      try {
        const user = await this.create({
          first_name,
          last_name,
          username: email.split('@')[0],
          email,
          phone: '',
          password: 'update-your-password',
          type: 'user',
          phone_verification_code: '',
          email_verification_code: email_verification_code,
          request: 'invitation-sent',
          user_created_by,
        });
        return user.toJSON();
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the createUser service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async createUserBySocialLogin({
      first_name,
      last_name,
      email,
      user_created_by,
      verificationCode,
      platform,
      socialUid,
      profileImage,
    }) {
      try {
        const user = await this.create({
          first_name,
          last_name,
          username: email.split('@')[0],
          email,
          phone: '000-0000-0000',
          password: '',
          type: 'user',
          phone_verification_code: '',
          email_verification_code: '',
          user_created_by,
          platform,
          socialUid,
          profileImage,
        });
        const emailTemplate = await sequelize.models.emailtemplate.findOne({
          where: {
            type: {
              [Op.like]: 'SocialLogin',
            },
          },
        });

        commonEmailSend(user, emailTemplate);
        return user.toJSON();
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the createUser service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async checkIfExist(email) {
      try {
        const userAlreadyCreated = await this.findOne({
          where: {
            email,
          },
        });

        if (userAlreadyCreated) {
          throw new Error(messages.user.DUPLICATE_EMAIL);
        }
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the Check User Exists service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async checkIfExistTrueFalse(email) {
      const userAlreadyCreated = await this.findOne({
        where: {
          email,
        },
      });

      if (userAlreadyCreated) {
        return true;
      }
      false;
    }

    static async checkIfExistEmailVerificationCode(
      email,
      email_verification_code,
    ) {
      try {
        const userAvailability = await this.findOne({
          where: {
            email,
            email_verification_code,
          },
        });

        if (userAvailability) {
          return true;
        }
      } catch (error) {
        errorLog(
          'USER MODEL ERROR: ',
          `Something went wrong with the Check User Exists service ${error}`,
        );
        throw new Error(error);
      }
    }

    static async login(userData) {
      try {
        const userAlreadyCreated = await this.findOne({
          where: {
            email: userData.email,
            platform: {
              [Op.eq]: null,
            },
          },
        });

        if (!userAlreadyCreated) {
          throw new Error(messages.user.USER_NOT_FOUND);
        }
        const passwordField = userAlreadyCreated.dataValues.password;
        const userDataJson = userAlreadyCreated.toJSON();
        const matchPassword = await bcrypt.compare(
          userData.password,
          passwordField,
        );
        if (!matchPassword) {
          throw new Error(messages.user.INVALID_PASSWORD);
        }

        // JWT token
        const token = await generateToken(userDataJson);
        return { token };
      } catch (error) {
        console.log('Something went wrong: Service: signup', error);
        throw new Error(error);
      }
    }
  }

  /* Override to JSON to exclude the password */
  UserModel.prototype.toJSON = function () {
    const values = { ...this.get() };

    delete values.password;
    delete values.verification_code;
    return values;
  };

  /* Initiating the User model schema */

  UserModel.init(
    {
      uid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      first_name: Sequelize.STRING,
      last_name: Sequelize.STRING,
      full_name: {
        type: Sequelize.VIRTUAL,
        get() {
          return `${this.first_name} ${this.last_name}`.trim();
        },
        set() {
          throw new Error('Do not try to set the `full_name` value!');
        },
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        validate: {
          isEmail: true,
        },
      },
      type: {
        type: Sequelize.ENUM('admin', 'user', 'super', 'guest'), 
        defaultValue: 'guest',
      } /* Super > admin > user > guest */,
      password: Sequelize.STRING,
      phone: Sequelize.STRING,
      phone_verification_code: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      email_verification_code: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      device_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      device_token: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      forget_password_code: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      platform: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      socialUid: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      profileImage: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      request: {
        type: Sequelize.ENUM(
          'invitation-sent',
          'requested-by-user',
          'accepted',
        ),
        defaultValue: 'accepted',
      },
      user_created_by: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      approvedBy: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'user',
      defaultScope: {
        attributes: { exclude: ['password', 'verification_code'] },
      },
      scopes: { withPassword: { attributes: {} } },
    },
  );
};

module.exports = User;
