const { Sequelize, Model } = require('sequelize');

const questionAnswer = (sequelize) => {
  class questionAnswerModel extends Model {}

  questionAnswerModel.init(
    {
      questionAnswerId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      answer: {
        type: Sequelize.STRING,
      },
      respondent: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'questionAnswer',
      timestamps: true,
    }
  );
};

module.exports = questionAnswer