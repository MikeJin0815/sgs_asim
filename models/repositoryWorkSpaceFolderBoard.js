const { Sequelize, Model } = require('sequelize');

const repositoryWorkSpaceFolderBoard = (sequelize) => {
  class RepositoryWorkSpaceFolderBoard extends Model {}

  RepositoryWorkSpaceFolderBoard.init(
    {
      workspaceFolderBoardId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      workspaceFolderId: {
        type: Sequelize.INTEGER,
        required: true,
      },
      workspaceFolderBoardName: {
        type: Sequelize.STRING,
        required: true,
      },
      workspaceFolderBoardType: {
        type: Sequelize.STRING,
        required: true,
      },
      order: Sequelize.INTEGER,
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {
      sequelize,
      modelName: 'repositoryworkspacefolderboard',
    },
  );

  RepositoryWorkSpaceFolderBoard.afterCreate(async (board) => {
    const folderGroup = await sequelize.models.filegroup.findOne({
      where: { workspaceFolderId: board.workspaceFolderId },
    });
    await sequelize.models.filegrouprecord.create({
      fileGroupId: folderGroup.fileGroupId,
      title: board.workspaceFolderBoardName,
      type: 'board',
      linkId: board.workspaceFolderBoardId,
    });
  });
};

module.exports = repositoryWorkSpaceFolderBoard;
