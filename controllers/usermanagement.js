const { models } = require('../database/index');
const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');

const {
  getUsers,
  getoTotalUsers,
  updateUser,
  updateUserRole,
  updateProfile,
} = require('../services/userManagementService');

module.exports.getUsers = async (req, res) => {
  const { limit, page } = req.query;
  let offset = 0;
  if (Number(page) != 1) {
    offset = Number(limit) * (Number(page) - 1);
  }

  /* Getting users */
  const response = await getUsers(Number(limit), Number(offset));
  const count = await getoTotalUsers();
  return res.status(200).send({
    totalRecord: count,
    response,
  });
};
module.exports.approveUser = async (req, res) => {
  const orgRowId = req.params.id;
  const authId = req.authorizationId;
  const orgData = await models.organization.findOne({
    where: { organizationId: orgRowId },
  });
  const belongsTo = orgData.childOf;
  const whoIsAdmin = await models.organization.findOne({
    where: { organizationId: belongsTo },
  });

  console.log(whoIsAdmin.uid, authId);
  if (whoIsAdmin.uid == authId) {
    const response = await updateUser(orgRowId, authId);
    if (response.status == 'success') {
      return res.status(200).send(response);
    }

    return res.status(403).send(response);
  }

  return res.status(400).send({
    sttaus: 'failed',
    message: 'User does not have authority',
  });
};

module.exports.updateUser = async (req, res) => {
  const { id } = req.params;
  const { body } = req;
  const adminId = req.authorizationId;
  /* Approving users */
  const response = await updateUser(id, body, adminId);
  return res.status(response.statusCode).send(response);
};

module.exports.updateProfile = async (req, res) => {
  const userId = req.authorizationId;
  const { body } = req;
  const response = await updateProfile(userId, body);
  return res.status(response.statusCode).send(response);
};
