const { STATUS_CODES } = require('http');
const { models } = require('../database/index');
const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');

const {
  groupCreate,
  groupUpdate,
  groupDelete,
  groupColumnCreate,
  groupRecordCreate,
  groupRecordUpdate,
  groupRecordDelete,
  groupRecordFileCreate,
  groupRecordFileDelete,
  groupListByboardId,
  groupColumnSequence,
  groupColumnEdit,
  groupColumnStatus,
  groupRecordFileEdit,
  createStatus,
  editStatus,
  deleteStatus,
  updateGroupListOrder,
  subGroupCreate,
  getDownloadFilesByGroups,
  getApplicationForm,
  applicationFormCreate,
  applicationFormSave,
  applicationFormGenerateLink,
  getApplicationFormByToken,
  applicationFormAddEntry,
  getDownloadFilesByFolderBoard,
  groupColumnOrderEdit,
  groupRowOrderEdit,
  groupColumnDelete,
} = require('../services/fileManagementService');

module.exports.groupCreate = async (req, res) => {
  const userId = req.authorizationId;
  const { boardId, groupName } = req.body;
  const response = await groupCreate(userId, boardId, groupName);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.updateGroupListOrder = async (req, res) => {
  const { groupOrders } = req.body;
  const response = await updateGroupListOrder(groupOrders);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupUpdate = async (req, res) => {
  const { groupId } = req.params;
  const { body } = req;
  const response = await groupUpdate(groupId, body);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupDelete = async (req, res) => {
  const { groupId } = req.params;
  const response = await groupDelete(groupId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupColumnCreate = async (req, res) => {
  const userId = req.authorizationId;
  const { fileGroupId, headerName, headerType, colorder } = req.body;
  const response = await groupColumnCreate(userId, fileGroupId, headerName, headerType, colorder);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordCreate = async (req, res) => {
  const userId = req.authorizationId;
  const { fileGroupId, title } = req.body;
  const response = await groupRecordCreate(userId, fileGroupId, title);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordDelete = async (req, res) => {
  const { recordId } = req.params;
  const response = await groupRecordDelete(recordId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordUpdate = async (req, res) => {
  const { recordId } = req.params;
  const { body } = req;
  const response = await groupRecordUpdate(recordId, body);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordFileCreate = async (req, res) => {
  const userId = req.authorizationId;
  const payload = req.body;
  payload.uid = userId;
  const response = await groupRecordFileCreate(payload);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordFileDelete = async (req, res) => {
  const { recordFileId } = req.params;
  const response = await groupRecordFileDelete(recordFileId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupList = async (req, res) => {
  const { boardId } = req.params;
  const response = await groupListByboardId(boardId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.getFilesByGroups = async (req, res) => {
  const { fileGroups, type } = req.query;
  const downloadFiles = await getDownloadFilesByGroups(fileGroups.split(','), type);
  return res.status(200).send({
    status: 'success',
    response: downloadFiles,
  });
};

module.exports.getFilesByFolderBoard = async (req, res) => {
  const { workspaceId, folderId, boardId, type } = req.query;
  const response = await getDownloadFilesByFolderBoard(workspaceId, folderId, boardId, type);
  return res.status(200).send({
    status: 'success',
    response,
  });
};

module.exports.groupColumnSequence = async (req, res) => {
  const { fileGroupId } = req.params;
  const response = await groupColumnSequence(fileGroupId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupColumnEdit = async (req, res) => {
  const { colId } = req.params;
  const { body } = req;
  const response = await groupColumnEdit(colId, body);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupColumnStatus = async (req, res) => {
  const { colId } = req.params;
  const statusValue = req.body.status;
  const response = await groupColumnStatus(colId, statusValue);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupColumnDelete = async (req, res) => {
  const { colId } = req.params;
  const response = await groupColumnDelete(colId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRecordFileEdit = async (req, res) => {
  const { recordFileId } = req.params;
  const payload = req.body;
  const response = await groupRecordFileEdit(recordFileId, payload);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.createStatus = async (req, res) => {
  const { colId, title, color } = req.body;
  const response = await createStatus(colId, title, color);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.editStatus = async (req, res) => {
  const { statusId } = req.params;
  const { title, color } = req.body;
  const response = await editStatus(statusId, title, color);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.deleteStatus = async (req, res) => {
  const { statusId } = req.params;
  const response = await deleteStatus(statusId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.subGroupCreate = async (req, res) => {
  const { recordId } = req.body;
  const response = await subGroupCreate(recordId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

/* Application Form management */
module.exports.getApplicationForm = async (req, res) => {
  // const userId = req.authorizationId;
  const { boardId } = req.params;
  const response = await getApplicationForm(boardId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.applicationFormCreate = async (req, res) => {
  // const userId = req.authorizationId;
  const { formContent } = req.body;
  const { boardId } = req.params;
  const response = await applicationFormCreate(boardId, formContent);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.applicationFormSave = async (req, res) => {
  // const userId = req.authorizationId;
  // const { formContent, isActive } = req.body;
  const { appformId } = req.params;

  const response = await applicationFormSave(appformId, req.body);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};
module.exports.applicationFormGenerateLink = async (req, res) => {
  // const userId = req.authorizationId;
  // const { formContent, isActive } = req.body;
  const { appformId } = req.params;

  const response = await applicationFormGenerateLink(appformId);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.getApplicationFormByToken = async (req, res) => {
  // const userId = req.authorizationId;
  // const { formContent, isActive } = req.body;
  const { publictoken } = req.params;

  const response = await getApplicationFormByToken(publictoken);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.applicationFormAddEntry = async (req, res) => {
  const { publictoken } = req.params;

  const response = await applicationFormAddEntry(publictoken, req.body);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};
module.exports.groupColumnOrderEdit = async (req, res) => {
  const { columnsOrder } = req.body;
  const response = await groupColumnOrderEdit(columnsOrder);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.groupRowOrderEdit = async (req, res) => {
  const { rowsOrder } = req.body;
  const response = await groupRowOrderEdit(rowsOrder);
  const { statusCode, status, message } = response;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};
