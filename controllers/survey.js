const { nextDay } = require('date-fns');
const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');
const survey = require('../routes/survey');

const {
    createSurvey,
    getDoSurveyList,
    getSurveyList,
    getQeustionList,
    responseSurvey,
    removeSurvey,
    getQeustionAnswerList
} = require('../services/survey');

module.exports.createSurvey = async (req, res) => {
  const { name, writer, startDateYear, startDateMonth, startDateDay, endDateYear, endDateMonth, endDateDay, question, type, rType } = req.body;
  const end_date = endDateYear + '-' + endDateMonth + '-' + endDateDay
  const start_date = startDateYear + '-' + startDateMonth + '-' + startDateDay
  const response = await createSurvey(name, writer, start_date, end_date, question, type, rType)
  return res.status(200).send(response); 
};

module.exports.list = async (req, res) => {
    const response = await getSurveyList();
    return res.status(response.statusCode).send(response);
};

module.exports.doList = async (req, res) => {
  const response = await getDoSurveyList();
  return res.status(response.statusCode).send(response);
};

module.exports.questionList = async (req, res) => {
  const response = await getQeustionList(req.body.surveyId);
  return res.status(response.statusCode).send(response);
};

module.exports.responseSurveyInfo = async (req, res, next) => {
  const { answer } = req.body;
  let array = new Array
  array = answer
  for(var i = 0; i<array.length; i++) {
    if(answer[i] == null) continue
    if( typeof(answer[i]) == 'object') {
      console.log('실행')
      answer[i] = answer[i][0]
    }
  }
  next();
};

module.exports.responseSurvey = async (req, res) => {
  const { respondent, answer } = req.body;
  
  const response = await responseSurvey(respondent, answer)
  return res.status(200).send(response); 
};

module.exports.removeSurvey = async (req, res) => {
  const { surveyId } = req.params;
  const response = await removeSurvey(surveyId);
  return res.status(response.code).send({ status: response.status, message: response.response });
};

module.exports.questionAnswerList = async (req, res) => {
  const response = await getQeustionAnswerList(req.body.questionId);
  return res.status(response.statusCode).send(response);
};

