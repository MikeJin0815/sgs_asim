const {
  addWorkspaceToRepository,
  getWorkSpaceInfo,
  deleteWorkspace,
  addWorkspaceToRepositoryByCopy,
  renameWorkspace,
} = require('../services/workspace');
const {
  isRepositoryAvailable,
  isWorkspaceNotAvailable,
} = require('../helpers/isCheck');

module.exports.addWorkspace = async (req, res) => {
  const uid = req.authorizationId;
  const { workspaceName, repositoryId } = req.body;
  const isRepoAvlble = await isRepositoryAvailable(repositoryId);
  const isWorkspaceNotAvlble = await isWorkspaceNotAvailable(
    Number(repositoryId),
    workspaceName,
  );
  if (isRepoAvlble && isWorkspaceNotAvlble) {
    const response = await addWorkspaceToRepository(
      Number(uid),
      Number(repositoryId),
      workspaceName
    );
    return res.status(200).send(response);
  }
  return res
    .status(400)
    .send({ status: 'failed', message: 'Failed to create Workspace' });
};

module.exports.addWorkspaceByCopy = async (req, res) => {
  const uid = req.authorizationId;
  const { workspaceId, repositoryId } = req.body;
  const isRepoAvailable = await isRepositoryAvailable(repositoryId);
  if (isRepoAvailable) {
    const response = await addWorkspaceToRepositoryByCopy(
      Number(uid),
      Number(repositoryId),
      Number(workspaceId)
    );
    return res.status(200).send(response);
  }
  return res
    .status(400)
    .send({ status: 'failed', message: 'Failed to copy Workspace' });
};

module.exports.getWorkSpaceInfo = async (req, res) => {
  const { workspaceId } = req.params;
  const { filters } = req.query;
  const response = await getWorkSpaceInfo(Number(workspaceId), filters);
  return res.status(response.statusCode).send(response);
};

module.exports.deleteWorkspace = async (req, res) => {
  const { workspaceId } = req.params;
  const response = await deleteWorkspace(Number(workspaceId));
  return res.status(200).send(response);
};

module.exports.renameWorkspace = async (req, res)=>{
  const { workspaceId } = req.params;
  const {workspaceName} = req.body;

  const response = await renameWorkspace(Number(workspaceId), workspaceName);
  return res.status(200).send(response);
}
