const { nextDay } = require('date-fns');
const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');

const {
    createApplication,
    getApplicationList,
    removeApplication,
    updateApplication,
    getAllApplicationList
} = require('../services/application');

module.exports.createApplication = async (req, res) => {
  const { company, manager, phone, address, number, category, managementNum, 
    express, size, sizeUnit, count, density, date, range, sample, extra, agree, fileName, fileLocation } = req.body; 
  const response = await createApplication(company, manager, phone, address, number, category, managementNum, 
                                            express, size, sizeUnit, count, density, date, range, sample, extra, agree, fileName, fileLocation)
  return res.status(200).send(response); 
};

module.exports.getApplicationList = async (req, res) => {
  const { manager } = req.body; 
  const response = await getApplicationList(manager)
  return res.status(200).send(response); 
};

module.exports.getAllApplicationList = async (req, res) => {
  const response = await getAllApplicationList()
  return res.status(200).send(response); 
};

module.exports.removeApplication = async (req, res) => {
  const applicationId = req.body.applicationId; 
  const response = await removeApplication(applicationId)
  return res.status(200).send(response); 
};

module.exports.updateApplication = async (req, res) => {
  const { applicationId, category, express, size, sizeUnit, count, density, date, range, sample, extra} = req.body; //MSDSattach
  const response = await updateApplication(applicationId, category, express, size, sizeUnit, count, density, date, range, sample, extra)
  return res.status(200).send(response); 
};