const {
  createRepository,
  addUserToRepository,
  getRepositoryInfo,
  updateRepositoryName,
  updateRepository,
  updateRepositoryPhoto,
  deleteRepository,
  removeUser,
  getBoardDetails,
  getRepositoryUsers,
  createBoardByCopy,
  clearBoard,
  updateBoardListOrder,
  addWorkspaceBoard,
  updateBoardName,
  getRepositoryUser,
  updateRepositoryUser,
} = require('../services/repository');
const { checkIsAdmin, isRepositoryNameAvailable, isRepositoryAvailable } = require('../helpers/isCheck');
const { models } = require('../database/index');

module.exports.createRepository = async (req, res) => {
  const { repositoryName, organizationId, isPublic } = req.body;
  const repositoryPhoto = req.body.repositoryPhoto || null;
  const uid = req.authorizationId;
  const isAdmin = await checkIsAdmin(uid, organizationId);
  const isRepoAvailable = await isRepositoryNameAvailable(organizationId, repositoryName);
  if (isAdmin === true && isRepoAvailable === true) {
    const response = await createRepository(uid, organizationId, repositoryName, null, isPublic);
    return res.status(200).send(response);
  }
  return res.status(403).send({ status: 'failed', message: 'Failed to create Repository' });
};

module.exports.getRepositoryInfo = async (req, res) => {
  const { repositoryId } = req.params;
  /* You dont need to work on this functionality */
  const response = await getRepositoryInfo(repositoryId);
  return res.status(200).send(response);
};

// Updating repository name
module.exports.updateRepositoryName = async (req, res) => {
  const { repositoryName } = req.body;
  const { repositoryId } = req.params;
  const response = await updateRepositoryName(repositoryId, repositoryName);
  if (response.status == 'success') {
    return res.status(200).send(response);
  }

  return res.status(403).send(response);
};

module.exports.updateRepository = async (req, res) => {
  const adminId = req.authorizationId;
  const { repositoryId } = req.params;
  const response = await updateRepository(repositoryId, req.body, adminId);
  if (response.status === 'success') {
    return res.status(200).send(response);
  }

  return res.status(403).send(response);
};

// Update repository photo
module.exports.updateRepositoryPhoto = async (req, res) => {
  const adminId = req.authorizationId;
  const { repositoryPhoto } = req.body;
  const { repositoryId } = req.params;
  const response = await updateRepositoryPhoto(repositoryId, repositoryPhoto, adminId);
  if (response.status === 'success') {
    return res.status(200).send(response);
  }

  return res.status(403).send(response);
};

module.exports.deleteRepository = async (req, res) => {
  const adminId = req.authorizationId;
  const { repositoryId } = req.params;
  const isRepoAvailable = await isRepositoryAvailable(repositoryId);
  if (isRepoAvailable) {
    const response = await deleteRepository(repositoryId, adminId);
    return res.status(200).send(response);
  }
  return res.status(403).send({ status: 'failed', message: 'Failed to delete repository' });
};

module.exports.adduser = async (req, res) => {
  const { repositoryId } = req.params;
  const { organizationId, usertoadd } = req.body;
  const adminId = req.authorizationId;
  const response = await addUserToRepository(
    Number(adminId),
    Number(repositoryId),
    Number(usertoadd),
    Number(organizationId)
  );
  return res.status(response.statusCode).send(response);
};

module.exports.removeUser = async (req, res) => {
  const { repositoryUserId } = req.params;
  const adminId = req.authorizationId;
  const response = await removeUser(repositoryUserId, adminId);
  return res.status(response.code).send({ status: response.status, message: response.response });
};

module.exports.addWorkspaceFolderBoard = async (req, res) => {
  const uid = req.authorizationId;
  const { workspaceFolderId } = req.params;
  const { boardType } = req.body;
  const { boardName } = req.body;
  const response = await addWorkspaceBoard(Number(uid), Number(workspaceFolderId), boardName, boardType);
  return res.status(200).send(response);
};

module.exports.updateWorkspaceFolderBoardName = async (req, res) => {
  const { workspaceFolderBoardId } = req.params;
  const { boardName } = req.body;
  const data = await updateBoardName(Number(workspaceFolderBoardId), boardName);
  return res.status(data.statusCode).send({ status: data.status, updated: data.updated });
};

module.exports.updateBoardListOrder = async (req, res) => {
  const { boardOrders } = req.body;
  const data = await updateBoardListOrder(boardOrders);
  const { statusCode, status, message } = data;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

module.exports.deleteBoard = async (req, res) => {
  const boardId = req.params.workspaceFolderBoardId;
  await clearBoard([boardId]);
  return res.status(204).send({ status: 'success', updated: 'record cleared' });
};

module.exports.boardDetails = async (req, res) => {
  const { boardId } = req.params;
  const data = await getBoardDetails(boardId);
  return res.status(data.code).send({ status: data.status, message: data.response });
};

module.exports.repositoryUser = async (req, res) => {
  const userId = req.authorizationId;
  const { repositoryId } = req.params;
  const data = await getRepositoryUser(repositoryId, userId);
  return res.status(data.code).send({ status: data.status, message: data.response });
};

module.exports.updateRepositoryUser = async (req, res) => {
  const { repositoryUserId } = req.params;
  const data = await updateRepositoryUser(repositoryUserId, req.body);
  return res.status(data.code).send({ status: data.status, message: data.response });
};

module.exports.repositoryUsers = async (req, res) => {
  const { repositoryId } = req.params;
  const data = await getRepositoryUsers(repositoryId);
  return res.status(data.code).send({ status: data.status, message: data.response });
};

module.exports.addBoardByCopy = async (req, res) => {
  const uid = req.authorizationId;
  const { folderId } = req.params;
  const { boardId } = req.params;
  const { isWithData } = req.body;
  const data = await createBoardByCopy(uid, folderId, boardId, isWithData);
  return res.status(data.code).send({ status: data.status, message: data.response });
};
