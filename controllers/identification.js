const { nextDay } = require('date-fns');
const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');
const {
    getAdminUser,
    updateUserType,
    createIdentification,
    getIdentification,
    getUserIdentification,
    deleteIdentification,
    updateIdentification
} = require('../services/identification');

module.exports.getAdminUser = async (req, res) => {
    const response = await getAdminUser()
    return res.status(200).send(response); 
  };

module.exports.updateUserType = async (req, res) => {
    const { type, uid } = req.body; 
    const response = await updateUserType(type, uid)
    return res.status(200).send(response); 
  };

module.exports.createIdentification = async (req, res) => {
    const { uid, uName, adminId, status, type } = req.body; 
    const response = await createIdentification(uid, uName, adminId, status, type)
    return res.status(200).send(response); 
  };

module.exports.getIdentification = async (req, res) => {
  const { uid } = req.body;
    const response = await getIdentification(uid)
    return res.status(200).send(response); 
  };

module.exports.getUserIdentification = async (req, res) => {
    const { uid } = req.body;
      const response = await getUserIdentification(uid)
      return res.status(200).send(response); 
    };

module.exports.updateUserType = async (req, res) => {
  const { type, uid, identificationId } = req.body;
  let response = await updateUserType(type, uid)
  
  console.log(response)
  response = await deleteIdentification(identificationId)
  return res.status(200).send(response); 
};

module.exports.removeIdenification = async (req, res) => {
  const { identificationId } = req.body;
  const response = await deleteIdentification(identificationId)
  return res.status(200).send(response); 
};

module.exports.updateIdentification = async (req, res) => {
  const { identificationId } = req.body;
  const response = await updateIdentification(identificationId)
  return res.status(200).send(response); 
};