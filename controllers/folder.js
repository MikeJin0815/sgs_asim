const { isWorkSpaceAvlble, isWorkspaceFolderAvlble } = require('../helpers/isCheck');
const {
  addWorkspaceFolderName,
  updateFolderName,
  deleteFolder,
  updateFolderListOrder,
  createFolderByCopy,
} = require('../services/folder');

module.exports.addWorkspaceFolder = async (req, res) => {
  const uid = req.authorizationId;
  const { workspaceFolderName, workspaceId } = req.body;
  const isWorkSpaceAvailable = await isWorkSpaceAvlble(workspaceId);
  const isWorkspaceFolderAvailable = await isWorkspaceFolderAvlble(Number(workspaceId), workspaceFolderName);

  console.log(isWorkSpaceAvailable, isWorkspaceFolderAvailable);
  if (isWorkSpaceAvailable && isWorkspaceFolderAvailable) {
    const response = await addWorkspaceFolderName(Number(uid), Number(workspaceId), workspaceFolderName);
    return res.status(200).send(response);
  }
  return res.status(400).send({ status: 'failed', message: 'Failed to create Workspace' });
};

module.exports.updateWorkspaceFolderName = async (req, res) => {
  const { workspaceFolderId } = req.params;
  const { folderName } = req.body;
  const data = await updateFolderName(Number(workspaceFolderId), folderName);
  return res.status(data.statusCode).send({ status: data.status, updated: data.updated });
};

module.exports.deleteWorkspaceFolder = async (req, res) => {
  const { workspaceFolderId } = req.params;
  const data = await deleteFolder([Number(workspaceFolderId)]);
  return res.status(data.statusCode).send({ status: data.status, message: data.message });
};

module.exports.updateFolderListOrder = async (req, res) => {
  const { folderOrders } = req.body;
  const data = await updateFolderListOrder(folderOrders);
  const { statusCode, status, message } = data;
  return res.status(statusCode).send({
    status,
    response: message,
  });
};

/* Copy functionality */
module.exports.addFolderByCopy = async (req, res) => {
  const uid = req.authorizationId;
  const folderId = req.params.workspaceFolderId;
  const { isWithData } = req.body;
  const data = await createFolderByCopy(uid, folderId, isWithData);
  return res.status(data.code).send({ status: data.status, message: data.response });
};
