const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');

const {
  registrationOpen,
  getInformation,
  registrationByAdmin,
  registrationCompleteByUser,
  login,
  forgetPassword,
  forgetUserNameFunc,
  verifiyUserCode,
  resetPassword,
  sendPhoneOtp,
  verifyPhoneOtp,
  checkEmailService,
  checkUserNameService,
} = require('../services/auth');

module.exports.registrationOpen = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'User registration',
    serviceCall: registrationOpen,
    serviceData: req,
    succesMsg: messages.user.SIGNUP_SUCCESS,
  });

  return res.status(response.status).send(response);
};
module.exports.getUserInfo = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'User information by token',
    serviceCall: getInformation,
    serviceData: req,
  });

  return res.status(response.status).send(response);
};

module.exports.registrationByAdmin = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Partial User registration',
    serviceCall: registrationByAdmin,
    serviceData: req,
    succesMsg: messages.user.SIGNUP_SUCCESS,
  });

  return res.status(response.status).send(response);
};

module.exports.regCompleteByUser = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Partial User registration',
    serviceCall: registrationCompleteByUser,
    serviceData: req,
    succesMsg: messages.user.PROFILE_UPDATE_SUCCESS,
  });

  return res.status(response.status).send(response);
};

module.exports.login = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Auth Login',
    serviceCall: login,
    serviceData: req,
    succesMsg: messages.user.SIGNIN_SUCCESS,
  });

  return res.status(response.status).send(response);
};

module.exports.forgetPassword = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Auth Forget Password',
    serviceCall: forgetPassword,
    serviceData: req.body,
    succesMsg: messages.user.FORGET_REQUEST_SUCCESS,
  });

  return res.status(response.status).send(response);
};

module.exports.forgetUserName = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Auth Forget Username',
    serviceCall: forgetUserNameFunc,
    serviceData: req.body,
    succesMsg: messages.user.MATCHED_EMAIL,
  });

  return res.status(response.status).send(response);
};

module.exports.verifyUserCode = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Verify User code',
    serviceCall: verifiyUserCode,
    serviceData: req.body,
    succesMsg: messages.user.VERIFICATION_CODE_VERIFIED,
  });

  return res.status(response.status).send(response);
};

module.exports.resetPassword = async (req, res) => {
  const { email, newPassword, confirmPassword } = req.body;
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Reset Password',
    serviceCall: resetPassword,
    serviceData: { email, newPassword, confirmPassword },
    succesMsg: messages.user.PASSWORD_UPDATED_SUCCESS,
  });
  return res.status(response.status).send(response);
};

module.exports.sendPhoneOtp = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Send Phone Otp',
    serviceCall: sendPhoneOtp,
    serviceData: req.body,
    succesMsg: messages.user.SEND_PHONE_OTP_SUCCESS,
  });

  return res.status(response.status).send(response);
};

module.exports.verifyPhoneOtp = async (req, res) => {
  /* Call the controller builder */
  const response = await controllerBuilder({
    controllerName: 'Verify Phone Otp',
    serviceCall: verifyPhoneOtp,
    serviceData: req.body,
    succesMsg: messages.user.PHONE_OTP_VERIFIED,
  });

  return res.status(response.status).send(response);
};

module.exports.checkEmail = async (req, res) => {
  const { email } = req.body;
  const response = await checkEmailService(email);
  return res.status(response.status).send({
    proceed: response.proceed,
  });
};

module.exports.checkUserName = async (req, res) => {
  const { username } = req.body;
  const response = await checkUserNameService(username);
  return res.status(response.status).send({
    proceed: response.proceed,
  });
};
