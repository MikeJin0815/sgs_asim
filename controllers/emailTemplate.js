const {
  createEmailTemplate,
  getEmailTemplate,
  deleteEmailTemplate,
  updateEmailTemplate,
  getEmailTemplateBySubject,
} = require('../services/repository');
const { checkIsAdmin } = require('../helpers/isCheck');

// Create Email Template
module.exports.createEmailTemplate = async (req, res) => {
  const type = req.body.templateType;
  const body = req.body.templateBody;
  const uid = req.authorizationId;
  const { subject } = req.body;
  const { organizationId } = req.body;
  const isAdmin = await checkIsAdmin(uid, organizationId);
  if (isAdmin) {
    let response;
    const getOneData = await getEmailTemplate(type);

    if (getOneData.response) {
      const tempId = getOneData.response.templateId;
      let uBody;
      let uSubject;

      if (!body) {
        uBody = getOneData.response.body;
      } else {
        uBody = body;
      }

      if (!subject) {
        uSubject = getOneData.response.subject;
      } else {
        uSubject = subject;
      }

      response = await updateEmailTemplate(tempId, uBody, uid, uSubject);
    } else {
      response = await createEmailTemplate(type, body, uid, subject);
    }
    return res.status(403).send(response);
  }
  return res.status(403).send('Admin Access Only');
};

// Delete Email Template
module.exports.deleteEmailTemplateByType = async (req, res) => {
  const type = req.body.templateType;
  const uid = req.authorizationId;
  const { organizationId } = req.body;
  const isAdmin = await checkIsAdmin(uid, organizationId);
  if (isAdmin) {
    let response;
    const getOneData = await getEmailTemplate(type);

    if (getOneData.response) {
      response = await deleteEmailTemplate(type);
    } else {
      return res.status(401).send('Email Template Not Found!');
    }
    return res.status(200).send(response);
  }
  return res.status(403).send('Admin Access Only');
};

// Get Email Template
module.exports.getEmailTemplate = async (req, res) => {
  const { templateType } = req.body;
  const getOneData = await getEmailTemplate(templateType);

  return res.status(200).send(getOneData);
};
