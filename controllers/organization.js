const messages = require('../helpers/messages');
const controllerBuilder = require('../helpers/utils');

const {
  getOrgList,
  getOrgUsersListByOrgId,
  orgDetailsByID,
  orgDetailsWithRepoInfoByID,
  checkOrganization,
  createOrganization,
  joinRequestOrganization,
  requestOrganizationApprove,
  checkOrganizationByPartialSearch,
  getBySubdomain,
  removeUserFromOrganization,
  checkUser,
} = require('../services/organization');

module.exports.checkOrganization = async (req, res) => {
  const { organization } = req.body;
  const userId = req.authorizationId;
  const response = await checkOrganization(organization, userId);
  return res.status(response.statusCode).send(response);
};

module.exports.checkUser = async (req, res) => {
  const { organizationId, email } = req.body;

  const response = await checkUser(organizationId, email);
  return res.status(response.statusCode).send(response);
};

module.exports.checkOrganizationByPartialSearch = async (req, res) => {
  const { organization } = req.body;
  const response = await checkOrganizationByPartialSearch(organization);
  return res.status(response.statusCode).send(response);
};

/* Not checking any condition as before proceeding this, user need to check {/organization/check} api */
module.exports.createOrganization = async (req, res) => {
  const { organization } = req.body;
  const userId = req.authorizationId;
  const response = await createOrganization(organization, userId);
  return res.status(response.statusCode).send(response);
};

module.exports.getOrganizationInfo = async (req, res) => {
  const { organizationId } = req.params;
  const response = await getOrganizationInfo(organizationId);
  return res.status(response.statusCode).send(response);
};

module.exports.joinRequestOrganization = async (req, res) => {
  const { organizationId } = req.body;
  const userId = req.authorizationId;
  const response = await joinRequestOrganization(organizationId, userId);
  return res.status(response.statusCode).send(response);
};

module.exports.list = async (req, res) => {
  const response = await getOrgList();
  return res.status(response.statusCode).send(response);
};

module.exports.requestOrganizationApprove = async (req, res) => {
  const { userId, organizationId } = req.body;
  const adminId = req.authorizationId;
  const response = await requestOrganizationApprove(userId, organizationId, adminId);
  return res.status(response.statusCode).send(response);
};

module.exports.listOfUsersByOrg = async (req, res) => {
  const { limit, page } = req.query;
  let offset = 0;
  if (Number(page) !== 1) {
    offset = Number(limit) * (Number(page) - 1);
  }
  const { organizationId } = req.params;
  const response = await getOrgUsersListByOrgId(organizationId, limit, offset);
  return res.status(response.statusCode).send(response);
};

module.exports.getOrgDetails = async (req, res) => {
  const userId = req.authorizationId;
  const { organizationId } = req.params;
  const response = await orgDetailsWithRepoInfoByID(organizationId, userId);
  return res.status(response.statusCode).send(response);
};

module.exports.getBySubdomain = async (req, res) => {
  const { subdomain } = req.params;
  const response = await getBySubdomain(subdomain);
  return res.status(response.statusCode).send(response);
};

module.exports.removeUserFromOrganization = async (req, res) => {
  const { userId, organizationId } = req.params;
  const adminId = req.authorizationId;
  const response = await removeUserFromOrganization(userId, organizationId, adminId);
  return res.status(response.statusCode).send(response);
};
