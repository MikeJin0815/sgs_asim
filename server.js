const dotEnv = require('dotenv');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const http = require('http');

const app = require('./app');
const sequelize = require('./database/index');
const { successLog } = require('./helpers/loggers');

dotEnv.config();
global.__basedir = __dirname;
const { PORT } = process.env;
const { models } = require('./database/index');

/*
 * Const fs = require('fs');
 * const key = fs.readFileSync('./key.pem');
 * const cert = fs.readFileSync('./cert.pem');
 */
const server = http.createServer(app);

if (process.env.NODE_ENV !== 'production') {
  const swaggerDocument = YAML.load('./swagger.yaml');
  swaggerDocument.host = process.env.HOSTIP.split('//')[1];
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}
app.listen(process.env.PORT, async () => {
  successLog('PORT OPENED : ', `Server listening on port ${PORT}`);
});
