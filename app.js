const express = require('express');
const session = require('express-session');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const SessionStore = require('express-session-sequelize')(session.Store);
const passport = require('passport');
const fileUpload = require('express-fileupload');
const sequelize = require('./database/index');
const initiateRouter = require('./routes/index');
const { errorLog } = require('./helpers/loggers');

const sequelizeSessionStore = new SessionStore({
  db: sequelize,
});

function initiateApp() {
  const app = express();
  app.use(fileUpload());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(cors());
  app.use(cookieParser());
  app.use('/uploads', express.static('./uploads'));

  app.use(express.json({ limit: '10mb' }));
  app.use(express.urlencoded({ extended: true }));
  app.use(
    session({
      secret: process.env.SECRET_KEY,
      resave: true,
      saveUninitialized: false,
      store: sequelizeSessionStore,
      cookie: { maxAge: 60 * 60 * 1000 },
    })
  );

  app.use((err, req, res, next) => {
    errorLog('SERVER ERROR: ', err.stack);
    res.status(500).send({
      status: 500,
      message: err.message,
      body: {},
    });
  });

  initiateRouter(app, express.Router());
  return app;
}

module.exports = initiateApp();
