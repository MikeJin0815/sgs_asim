const { Sequelize } = require('sequelize');
const { addColumnIfNotExist, removeColumnIfExist } = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'filegrouprecords', 'order', {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    });
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'filegrouprecords', 'order');
  },
};
