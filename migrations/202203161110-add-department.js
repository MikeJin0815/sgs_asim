const { Sequelize } = require('sequelize');
const { addColumnIfNotExist, removeColumnIfExist } = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'organizationusers', 'department', {
      type: Sequelize.STRING,
      defaultValue: '',
    });
    await removeColumnIfExist(queryInterface, 'users', 'department');
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'organizationusers', 'department');
  },
};
