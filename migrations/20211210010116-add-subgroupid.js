const { Sequelize } = require('sequelize');
const {
  addColumnIfNotExist,
  removeColumnIfExist,
} = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'filegroups', 'subGroupId', {
      type: Sequelize.INTEGER,
    });
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'filegroups', 'subGroupId');
  },
};
