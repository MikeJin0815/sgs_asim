const { Sequelize } = require('sequelize');
const { addColumnIfNotExist, removeColumnIfExist } = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'repositoryusers', 'accessLevel', {
      type: Sequelize.INTEGER,
      defaultValue: 1,
    });
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'repositoryusers', 'accessLevel');
  },
};
