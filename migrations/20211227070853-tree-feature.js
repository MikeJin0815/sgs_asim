const { Sequelize } = require('sequelize');
const {
  addColumnIfNotExist,
  removeColumnIfExist,
} = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'filegroups', 'workspaceId', {
      type: Sequelize.INTEGER,
    });

    await addColumnIfNotExist(
      queryInterface,
      'filegroups',
      'workspaceFolderId',
      {
        type: Sequelize.INTEGER,
      },
    );

    await addColumnIfNotExist(queryInterface, 'filegrouprecords', 'type', {
      type: Sequelize.ENUM('folder', 'board', 'record'),
      defaultValue: 'record',
    });

    await addColumnIfNotExist(queryInterface, 'filegrouprecords', 'linkId', {
      type: Sequelize.INTEGER,
    });
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'filegroups', 'workspaceId');
    await removeColumnIfExist(
      queryInterface,
      'filegroups',
      'workspaceFolderId',
    );

    await removeColumnIfExist(queryInterface, 'filegrouprecords', 'type');
    await removeColumnIfExist(queryInterface, 'filegrouprecords', 'linkId');
  },
};
