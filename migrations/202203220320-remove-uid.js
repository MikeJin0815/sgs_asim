const { Sequelize } = require('sequelize');
const { addColumnIfNotExist, removeColumnIfExist } = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'filegroupcolumns', 'uid');
    await removeColumnIfExist(queryInterface, 'filegrouprecords', 'uid');
    await removeColumnIfExist(queryInterface, 'repositoryworkspaces', 'uid');
    await removeColumnIfExist(queryInterface, 'repositoryworkspacefolders', 'uid');
    await removeColumnIfExist(queryInterface, 'repositoryworkspacefolderboards', 'uid');
  },

  down: async ({ context: queryInterface }) => {
  },
};
