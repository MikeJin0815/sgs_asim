const { Sequelize } = require('sequelize');
const { addColumnIfNotExist, removeColumnIfExist } = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(queryInterface, 'filegroups', 'order', {
      type: Sequelize.INTEGER,
    });
    await addColumnIfNotExist(queryInterface, 'filegroups', 'colOrder', {
      type: Sequelize.JSON,
    });
    await addColumnIfNotExist(queryInterface, 'systems', 'order', {
      type: Sequelize.INTEGER,
    });
    await addColumnIfNotExist(queryInterface, 'repositoryworkspacefolders', 'order', {
      type: Sequelize.INTEGER,
    });
    await addColumnIfNotExist(queryInterface, 'repositoryworkspacefolderboards', 'order', {
      type: Sequelize.INTEGER,
    });
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(queryInterface, 'filegroups', 'order');
    await removeColumnIfExist(queryInterface, 'filegroups', 'colOrder');
    await removeColumnIfExist(queryInterface, 'systems', 'order');
    await removeColumnIfExist(queryInterface, 'repositoryworkspacefolders', 'order');
    await removeColumnIfExist(queryInterface, 'repositoryworkspacefolderboards', 'order');
  },
};
