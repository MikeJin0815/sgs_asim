const { Sequelize } = require('sequelize');
const {
  addColumnIfNotExist,
  removeColumnIfExist,
} = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.bulkDelete(
      'filegroups',
      { subGroupId: { [Sequelize.Op.ne]: null } },
      {},
    );
  },

  down: async ({ context: queryInterface }) => {},
};
