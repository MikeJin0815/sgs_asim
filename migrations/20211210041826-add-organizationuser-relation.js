const { Sequelize } = require('sequelize');
const {
  addColumnIfNotExist,
  removeColumnIfExist,
} = require('../helpers/utils');

module.exports = {
  up: async ({ context: queryInterface }) => {
    await addColumnIfNotExist(
      queryInterface,
      'filegrouprecordfiles',
      'organizationUserId',
      {
        type: Sequelize.INTEGER,
      }
    );
  },

  down: async ({ context: queryInterface }) => {
    await removeColumnIfExist(
      queryInterface,
      'filegrouprecordfiles',
      'organizationUserId'
    );
  },
};
