const { models } = require('../database/index');
const { decodeToken } = require('../helpers/utils');

module.exports.verifytoken = async (req, res, next) => {
  req._isAuthenticated = false;
  try {
    const token = req.headers['x-access-token'];
    if (!token) {
      return res.status(403).send({
        status: 'Forbidden',
      });
    }
    const userId = await decodeToken(token);
    const foundUser = await models.user.findOne({ where: { uid: userId } });
    if (foundUser) {
      const user = foundUser.toJSON();
      delete user.password;
      req._isAuthenticated = true;
      req.authorizationType = user.type;
      req.authorizationId = userId;
    }
    next();
  } catch (error) {
    return res.status(403).send({
      status: 'Forbidden',
    });
  }
};
