const { check, validationResult } = require('express-validator');

exports.validateProductRequest = [
  check('name').trim().escape().not().isEmpty(),
  check('email').isEmail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
  },
];

module.exports.limitPageChecker = async (req, res, next) => {
  const { limit, page } = req.query;
  if (limit <= 25 && page) {
    let offset = 0;
    if (Number(page) != 1) {
      offset = Number(limit) * (Number(page) - 1);
    }
    req.limit = limit;
    req.offset = offset;
    next();
  } else {
    return res.status(400).send({
      status: 'Bad Request',
      limit: 'Maximum value is 25',
      message: 'limit and page query parameters are mandatory.',
    });
  }
};
