module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('statuses', [
      {
        statusId: 1,
        colId: 2,
        title: 'Working on it',
        color: 'bg-orange-0',
      },
      {
        statusId: 2,
        colId: 2,
        title: 'Stuck',
        color: 'bg-red-0',
      },
      {
        statusId: 3,
        colId: 2,
        title: 'Done',
        color: 'bg-green-0',
      },
      {
        statusId: 4,
        colId: 2,
        title: '',
        color: 'bg-gray-6',
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('statuses', null, {}),
};
