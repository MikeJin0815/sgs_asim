const sampleDate = (days = 0) => {
  const date = new Date();
  date.setDate(date.getDate() + days);
  return date;
};

module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('filegrouprecordfiles', [
      {
        recordFileId: 1,
        recordId: 3,
        colId: 1,
        text: 'Sample A',
      },
      {
        recordFileId: 2,
        recordId: 3,
        colId: 2,
        statusId: 1,
      },
      {
        recordFileId: 3,
        recordId: 3,
        colId: 3,
        people: '1,2',
      },
      {
        recordFileId: 4,
        recordId: 3,
        colId: 4,
        daterange_start: sampleDate(-1),
        daterange_end: sampleDate(2),
      },
      {
        recordFileId: 5,
        recordId: 4,
        colId: 1,
        text: 'Sample B',
      },
      {
        recordFileId: 6,
        recordId: 4,
        colId: 2,
        statusId: 3,
      },
      {
        recordFileId: 7,
        recordId: 4,
        colId: 3,
        people: '3',
      },
      {
        recordFileId: 8,
        recordId: 4,
        colId: 4,
        daterange_start: sampleDate(-1),
        daterange_end: sampleDate(8),
      },
      {
        recordFileId: 9,
        recordId: 5,
        colId: 1,
        text: 'Sample C',
      },
      {
        recordFileId: 10,
        recordId: 5,
        colId: 2,
        statusId: 4,
      },
      {
        recordFileId: 11,
        recordId: 5,
        colId: 3,
        people: '',
      },
      {
        recordFileId: 12,
        recordId: 5,
        colId: 4,
        daterange_start: sampleDate(-4),
        daterange_end: sampleDate(5),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('filegrouprecordfiles', null, {}),
};
