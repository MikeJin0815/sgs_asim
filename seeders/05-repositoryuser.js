module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('repositoryusers', [
      {
        id: 1,
        organizationUserId: 1,
        repositoryId: 1,
        assignedBy: 1,
        accessLevel: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 2,
        organizationUserId: 2,
        repositoryId: 1,
        assignedBy: 1,
        accessLevel: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 3,
        organizationUserId: 3,
        repositoryId: 1,
        assignedBy: 1,
        accessLevel: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('repositoryusers', null, {}),
};
