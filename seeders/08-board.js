module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('repositoryworkspacefolderboards', [
      {
        workspaceFolderBoardId: 1,
        workspaceFolderBoardName: 'Board A',
        workspaceFolderId: 1,
        workspaceFolderBoardType: 'Project',
        order: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('repositoryworkspacefolderboards', null, {}),
};
