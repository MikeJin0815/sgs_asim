module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('repositoryworkspacefolders', [
      {
        workspaceFolderId: 1,
        workspaceFolderName: 'Folder A',
        workspaceId: 1,
        order: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('repositoryworkspacefolders', null, {}),
};
