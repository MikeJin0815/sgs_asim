module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('organizations', [
      {
        organizationId: 1,
        name: 'Luxolis',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('organizations', null, {}),
};
