module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('repositories', [
      {
        repositoryId: 1,
        repositoryName: 'Main repository',
        organizationId: 1,
        isPublic: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('repositories', null, {}),
};
