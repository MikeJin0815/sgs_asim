module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('repositoryworkspaces', [
      {
        workspaceId: 1,
        workspaceName: 'Main workspace',
        repositoryId: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('repositoryworkspaces', null, {}),
};
