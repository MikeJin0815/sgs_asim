module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('filegroupcolumns', [
      {
        colId: 1,
        fileGroupId: 3,
        headerName: 'Text',
        headerType: 'text',
        active: true,
        width: 150,
        colorder: 1,
      },
      {
        colId: 2,
        fileGroupId: 3,
        headerName: 'Status',
        headerType: 'status',
        active: true,
        width: 150,
        colorder: 2,
      },
      {
        colId: 3,
        fileGroupId: 3,
        headerName: 'People',
        headerType: 'people',
        active: true,
        width: 150,
        colorder: 3,
      },
      {
        colId: 4,
        fileGroupId: 3,
        headerName: 'Timeline',
        headerType: 'daterange',
        active: true,
        width: 150,
        colorder: 4,
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('filegroupcolumns', null, {}),
};
