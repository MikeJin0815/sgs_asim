module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('filegrouprecords', [
      {
        recordId: 1,
        fileGroupId: 1,
        title: 'Folder A',
        type: 'folder',
        linkId: 1,
        order: 1,
      },
      {
        recordId: 2,
        fileGroupId: 2,
        title: 'Board A',
        type: 'board',
        linkId: 1,
        order: 1,
      },
      {
        recordId: 3,
        fileGroupId: 3,
        title: 'Record A',
        type: 'record',
        order: 1,
      },
      {
        recordId: 4,
        fileGroupId: 3,
        title: 'Record B',
        type: 'record',
        order: 2,
      },
      {
        recordId: 5,
        fileGroupId: 3,
        title: 'Record C',
        type: 'record',
        order: 3,
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('filegrouprecords', null, {}),
};
