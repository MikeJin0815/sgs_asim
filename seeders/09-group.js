module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('filegroups', [
      {
        fileGroupId: 1,
        fileSystemGroupName: 'Home',
        colOrder: '[]',
        order: 1,
        workspaceId: 1,
      },
      {
        fileGroupId: 2,
        fileSystemGroupName: 'Folder A',
        colOrder: '[]',
        order: 1,
        workspaceFolderId: 1,
      },
      {
        fileGroupId: 3,
        boardId: 1,
        fileSystemGroupName: 'Group A',
        colOrder: '[]',
        order: 1,
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('filegroups', null, {}),
};
