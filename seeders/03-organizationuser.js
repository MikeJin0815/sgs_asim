module.exports = {
  up: async (queryInterface) =>
    queryInterface.bulkInsert('organizationusers', [
      {
        organizationUserId: 1,
        role: 'admin',
        requestAccepted: true,
        approvedBy: 104,
        uid: 1,
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        organizationUserId: 2,
        role: 'admin',
        requestAccepted: true,
        approvedBy: 1,
        uid: 2,
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        organizationUserId: 3,
        role: 'user',
        requestAccepted: true,
        approvedBy: 1,
        uid: 3,
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]),
  down: (queryInterface) => queryInterface.bulkDelete('organizationusers', null, {}),
};
