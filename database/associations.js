module.exports = (sequelize) => {
  const {
    user,
    organization,
    organizationuser,
    repository,
    repositoryuser,
    repositoryworkspace,
    repositoryworkspacefolder,
    repositoryworkspacefolderboard,
    filegroup,
    filegroupcolumn,
    filegrouprecord,
    filegrouprecordfile,
    status,
    subgroup,
    applicationform,
    applicationformentry,
    survey,
    question,
    questionAnswer,
    identification
  } = sequelize.models;
  
  user.belongsToMany(organization, {
    through: organizationuser,
    foreignKey: 'uid',
  });
  organization.belongsToMany(user, {
    through: organizationuser,
    foreignKey: 'organizationId',
  });
  organizationuser.belongsTo(user, { foreignKey: 'uid', onDelete: 'CASCADE' });
  organizationuser.belongsTo(organization, {
    foreignKey: 'organizationId',
    onDelete: 'CASCADE',
  });
  user.hasMany(organizationuser, { foreignKey: 'uid', onDelete: 'CASCADE' });
  organization.hasMany(organizationuser, {
    foreignKey: 'organizationId',
    onDelete: 'CASCADE',
  });
  user.hasMany(repository, { foreignKey: 'uid' });

  organization.hasMany(repository, { foreignKey: 'organizationId' });
  repository.belongsTo(organization, { foreignKey: 'organizationId' });

  repository.hasMany(repositoryuser, { foreignKey: 'repositoryId' });
  repositoryuser.belongsTo(repository, { foreignKey: 'repositoryId' });

  organizationuser.hasMany(repositoryuser, {
    foreignKey: 'organizationUserId',
    onDelete: 'CASCADE',
  });
  repositoryuser.belongsTo(organizationuser, {
    foreignKey: 'organizationUserId',
    onDelete: 'CASCADE',
  });

  repository.hasMany(repositoryworkspace, { foreignKey: 'repositoryId' });
  repositoryworkspace.belongsTo(repository, { foreignKey: 'repositoryId' });

  repositoryworkspace.hasOne(filegroup, { foreignKey: 'workspaceId', onDelete: 'CASCADE' });
  filegroup.belongsTo(repositoryworkspace, { foreignKey: 'workspaceId', onDelete: 'CASCADE' });

  repositoryworkspace.hasMany(repositoryworkspacefolder, {
    foreignKey: 'workspaceId',
    onDelete: 'CASCADE',
  });
  repositoryworkspacefolder.belongsTo(repositoryworkspace, {
    foreignKey: 'workspaceId',
  });

  repositoryworkspacefolder.hasOne(filegroup, {
    foreignKey: 'workspaceFolderId',
    onDelete: 'CASCADE',
  });
  filegroup.belongsTo(repositoryworkspacefolder, {
    foreignKey: 'workspaceFolderId',
    onDelete: 'CASCADE',
  });

  repositoryworkspacefolder.hasMany(repositoryworkspacefolderboard, {
    foreignKey: 'workspaceFolderId',
    onDelete: 'CASCADE',
  });
  repositoryworkspacefolderboard.belongsTo(repositoryworkspacefolder, {
    foreignKey: 'workspaceFolderId',
  });

  repositoryworkspacefolderboard.hasMany(filegroup, { foreignKey: 'boardId' });
  filegroup.belongsTo(repositoryworkspacefolderboard, {
    foreignKey: 'boardId',
    onDelete: 'CASCADE',
  });

  status.belongsTo(filegroupcolumn, { foreignKey: 'colId', onDelete: 'CASCADE' });
  filegroupcolumn.hasMany(status, { foreignKey: 'colId', onDelete: 'CASCADE' });
  status.hasMany(filegrouprecordfile, { foreignKey: 'statusId', onDelete: 'CASCADE' });
  filegrouprecordfile.belongsTo(status, { foreignKey: 'statusId', onDelete: 'CASCADE' });

  filegroup.hasMany(filegrouprecord, {
    foreignKey: 'fileGroupId',
    onDelete: 'CASCADE',
  });
  filegrouprecord.belongsTo(filegroup, {
    foreignKey: 'fileGroupId',
    onDelete: 'CASCADE',
  });

  subgroup.belongsTo(filegrouprecord, {
    foreignKey: 'recordId',
    onDelete: 'CASCADE',
  });
  subgroup.belongsTo(filegroup, {
    as: 'filegroup',
    foreignKey: 'fileGroupId',
    onDelete: 'CASCADE',
  });
  filegrouprecord.hasOne(subgroup, {
    as: 'subgroup',
    foreignKey: 'recordId',
    onDelete: 'CASCADE',
  });

  applicationform.belongsTo(repositoryworkspacefolderboard, {
    as: 'repositoryworkspacefolderboard',
    foreignKey: 'boardId',
    onDelete: 'CASCADE',
  });
  repositoryworkspacefolderboard.hasOne(applicationform, {
    as: 'applicationform',
    foreignKey: 'boardId',
  });
  applicationform.hasMany(applicationformentry, {
    as: 'applicationformentry',
    foreignKey: 'applicationFormId',
  });
  applicationformentry.belongsTo(applicationform, {
    as: 'applicationform',
    foreignKey: 'applicationFormId',
  });

  survey.hasMany(question, {
    foreignKey: 'surveyId',
    onDelete: "cascade",
  })
  question.belongsTo(survey, {
    foreignKey: 'surveyId',
    onDelete: "cascade",
  });

  question.hasMany(questionAnswer, {
    foreignKey: 'questionId',
    onDelete: "cascade",
  })
  questionAnswer.belongsTo(question, {
    foreignKey: 'questionId',
    onDelete: "cascade",
  });

  user.hasMany(identification, {
    foreignKey: 'uid',
    onDelete: "cascade",
  })
  identification.belongsTo(user, {
    foreignKey: 'uid',
    onDelete: "cascade",
  });
  
};
