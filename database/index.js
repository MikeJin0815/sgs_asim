const Sequelize = require('sequelize');
const { Umzug, SequelizeStorage } = require('umzug');
const cls = require('cls-hooked');
const { DataTypes } = require('sequelize');

const namespace = cls.createNamespace('transaction-namespace');

Sequelize.useCLS(namespace);

const dotEnv = require('dotenv');

const { successLog, noticeLog, errorLog } = require('../helpers/loggers');

dotEnv.config();

function inititeDatabase() {
  const sequelize = new Sequelize(
    process.env.DATABASE_NAME,
    process.env.DATABASE_USER_NAME,
    process.env.DATABASE_PASSWORD,
    {
      host: process.env.DATABASE_HOST,
      dialect: 'mysql',
      logging: noticeLog,
      define: {
        timestamps: false,
      },
    }
  );
  const umzug = new Umzug({
    migrations: { glob: 'migrations/*.js' },
    context: sequelize.getQueryInterface(),
    storage: new SequelizeStorage({ sequelize }),
    logger: console,
  });

  (async function () {
    try {
      await umzug.up();
      await sequelize.authenticate();
      successLog('CONNECTED TO DB :', 'Connection has been established successfully.');
    } catch (error) {
      errorLog('DB CONNECTION ERROR :', `Unable to connect to the database:${error}`);
    }
  }());

  require('./models')(sequelize);
  require('./associations')(sequelize);

  sequelize.sync();

  return sequelize;
}

module.exports = inititeDatabase();
