module.exports = sequelize => {
  const modelDefiners = [
    require('../models/user'),
    require('../models/phoneOtp'),
    require('../models/organization'),
    require('../models/organizationuser'),
    require('../models/repository'),
    require('../models/repositoryuser'),
    require('../models/repositoryworkspace'),
    require('../models/repositoryWorkspaceFolder'),
    require('../models/repositoryWorkSpaceFolderBoard'),
    require('../models/emailtemplate'),
    require('../models/filegroup'),
    require('../models/applicationform'),
    require('../models/applicationformentry'),
    require('../models/filegroupcolumn'),
    require('../models/filegrouprecord'),
    require('../models/filegrouprecordfile'),
    require('../models/status'),
    require('../models/subgroup'),
    require('../models/survey'),
    require('../models/question'),
    require('../models/qusetionAnswer'),
    require('../models/application'),
    require('../models/identification')
  ];

  for (const modelDefiner of modelDefiners) {
    modelDefiner(sequelize);
  }
};
